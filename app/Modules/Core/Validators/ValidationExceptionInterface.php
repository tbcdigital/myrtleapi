<?php namespace CommonCore\Core\Validators;

use JsonSerializable;
use Illuminate\Support\Contracts\ArrayableInterface;
use Illuminate\Support\Contracts\JsonableInterface;
use Illuminate\Support\Contracts\MessageProviderInterface;

interface ValidationExceptionInterface extends
	ArrayableInterface,
	JsonableInterface,
	JsonSerializable,
	MessageProviderInterface
{
}