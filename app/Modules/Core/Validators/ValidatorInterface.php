<?php namespace CommonCore\Core\Validators;

/**
 * This interface defines the basic methods required by all entity validators used in modules
 *
 * @author Lee Sherwood <lee.sherwood@tbc-digital.com>
 * @package CommonCore
 * @version 1.0.0
 */

interface ValidatorInterface
{

	/**
	 * Toggle whether to throw exceptions on validation errors.
	 *
	 * @param  boolean $toggle
	 *
	 * @return static
	 */
	public function toggleExceptions($toggle = true);


	/**
	 * Tell the validator to replace a certain value in the rules.
	 *
	 * @param  string $key
	 * @param  string $value
	 *
	 * @return void
	 */
	public function replace($key, $value);


	/**
	 * Validate a set of attributes against an action.
	 *
	 * @param  string  $operation
	 * @param  array   $attributes
	 * @param  boolean $merge      Whether or not to merge with common rules.
	 * Leave the parameter out to use whatever is the default.
	 *
	 * @return boolean
	 *
	 * @throws \anlutro\Validation\ValidationException if exceptions are toggled
	 * on and validation fails
	 */
	public function validate($operation, array $attributes, $merge = null);


	/**
	 * Get the validation errors. Alternative to ->errors()
	 *
	 * @return \Illuminate\Support\MessageBag
	 */
	public function getErrors();

}