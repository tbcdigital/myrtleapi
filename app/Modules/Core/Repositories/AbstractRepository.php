<?php namespace CommonCore\Core\Repositories;

use Illuminate\Support\MessageBag;

/**
 * Base repository
 *
 * @package CommonCore
 * @version 1.0.0
 * @author Lee Sherwood <lee.sherwood@tbcdigital.com>
 * @abstract
 */

abstract class AbstractRepository
{

    /**
     * Whether or not to throw exceptions or return null when "find" methods do
     * not yield any results.
     *
     * @var boolean
     */
    protected $throwExceptions = false;


    /**
     * The validator instance used by the repository to validate the data being stored
     *
     * @var \CommonCore\Core\Validators\ValidatorInterface
     */
    protected $validator = null;


    /**
     * Whether to validate the model or the input attributes.
     *
     * @var boolean
     */
    protected $validateEntity = false;


    /**
     * Error message container
     *
     * @var \Illuminate\Support\MessageBag
     */
    protected $errors;


    /**
     * Whether or not to paginate results.
     *
     * @var boolean
     */
    protected $paginate = false;


    /**
     * Constructor
     *
     * Creates an instance of the repository with an optional validation service
     *
     * @param \CommonCore\Core\Validators\ValidatorInterface $validator
     * @return void
     */
    public function __construct(\CommonCore\Core\Validators\ValidatorInterface $validator = null)
    {

        $this->resetErrors();

        if(!is_null($validator)) {
            $this->setValidator($validator);
        }

    }


    /**
     * Perform an action
     *
     * Acts as a middleman method between an operation and the performing of that operation
     * so that we have a centralised location for hooking in additional filters and checks.
     * You should call a public method describing the operation such as "create" or "createAsAdmin"
     * and also define a protected method prefixed with "perform" such as "performCreate" or "performCreateAsAdmin"
     *
     * @param string $operation The name of the operation being performed (later StudlyCaps against "perform" such as "performUpdate")
     * @param array|object $object the entity being persisted, or the query builder instance in the case of a lookup
     * @param array $attributes The attributes being saved for the entity (should be false for lookups)
     * @param bool $validate Whether to validate before persisting (should be false for lookups)
     * @return mixed The result of the private perform method, or false if validation fails
     */
    protected function perform($operation, $object, $attributes = [], $validate = true)
    {

        $perform = 'perform' . ucfirst($operation);
        if (!method_exists($this, $perform)) {
            throw new \BadMethodCallException("Method $perform does not exist on this repository");
        }

        if (true === $validate) {
            if ($this->validateEntity) {
                if (!$this->isValid($operation, $this->getEntityAttributes($object))) return false;
            } else {
                if (!$this->isValid($operation, $attributes)) return false;
            }
        }

        $beforeResult = $this->doBefore($operation, $object, $attributes);
        if ($beforeResult === false) return $beforeResult;

        $result = call_user_func_array([$this, $perform], [$object, $attributes]);
        if ($result === false) return $result;

        $this->doAfter($operation, $result, $attributes);

        return $result;

    }


    /**
     * Empty/Reset the error bag to empty
     *
     * @return void
     */
    public function resetErrors()
    {
        $this->errors = new MessageBag;
    }


    /**
     * Return the error bag
     *
     * @return \Illuminate\Support\MessageBag
     */
    public function getErrors()
    {
        return $this->errors;
    }


    /**
     * Check if there are any errors set
     *
     * @return boolean
     */
    public function hasErrors()
    {
        return $this->getErrorCount() > 0;
    }


    /**
     * Get number of errors
     *
     * @return int
     */
    public function getErrorCount()
    {
        return $this->errors->count();
    }


    /**
     * Set the repository's validator.
     *
     * @param \CommonCore\Core\Validators\ValidatorInterface $validator
     * @return static
     */
    public function setValidator(\CommonCore\Core\Validators\ValidatorInterface $validator)
    {
        $this->validator = $validator;

        $this->validator->replace('table', $this->model->getTable());

        return $this;
    }


    /**
     * Get the repository's validator.
     *
     * @return \CommonCore\Core\Validators\ValidationInterface
     */
    public function getValidator()
    {
        return $this->validator;
    }


    /**
     * Run validation on the entity, for a specific operation
     * @param string $operation Operation type: create|update
     * @param array $attributes The attributes for the entity to validate
     * @return boolean
     */
    public function isValid($operation, $attributes)
    {

        if(is_null($this->validator)) {
            return true;
        }


        $result = $this->getValidator()->validate($operation, $attributes);

        if(false === $result) {
            $this->errors->merge($this->getValidator()->getErrors());
        }

        return $result;

    }


    /**
     * Perform a before or after action.
     *
     * @param  string $which  before or after
     * @param  string $action
     * @param  array  $args
     *
     * @return false|null
     */
    protected function doBeforeOrAfter($which, $action, array $args)
    {
        $method = $which.ucfirst($action);

        if (method_exists($this, $method)) {
            $result = call_user_func_array([$this, $method], $args);
            if ($result === false) return $result;
        }

        return null;
    }


    /**
     * Do a before action.
     *
     * @see   doBeforeOrAfter
     *
     * @param  string $action
     * @param  object $object
     * @param  mixed  $attributes
     *
     * @return mixed
     */
    protected function doBefore($action, $object, $attributes)
    {
        return $this->doBeforeOrAfter('before', $action, [$object, $attributes]);
    }


    /**
     * Do an after action.
     *
     * @see   doBeforeOrAfter
     *
     * @param  string $action
     * @param  mixed  $result
     * @param  mixed  $attributes
     *
     * @return mixed
     */
    protected function doAfter($action, $result, $attributes)
    {
        return $this->doBeforeOrAfter('after', $action, [$result, $attributes]);
    }


    /**
     * Toggle throwing of exceptions.
     *
     * @param  boolean $toggle
     * @param  boolean $toggleValidator Whether or not to toggle exceptions on the validator as well as the repository. Defaults to true
     *
     * @return static
     */
    public function toggleExceptions($toggle, $toggleValidator = true)
    {
        $this->throwExceptions = (bool) $toggle;

        if ($this->validator && $toggleValidator) {
            $this->validator->toggleExceptions((bool) $toggle);
        }

        return $this;
    }


    /**
     * Create and persist a new entity with the given attributes.
     *
     * @param  array  $attributes
     *
     * @return object|false
     */
    public function create(array $attributes)
    {
        return $this->perform('create', $this->getNew(), $attributes, true);
    }


    /**
     * Update an entity with the given attributes and persist it.
     *
     * @param  object  $entity
     * @param  array   $attributes
     *
     * @return boolean
     */
    public function update($entity, array $attributes)
    {
        if ($this->validator) {
            $this->validator->replace('key', $this->getEntityKey($entity));
        }

        return $this->perform('update', $entity, $attributes, true) ? true : false;
    }


    /**
     * Delete an entity.
     *
     * @param  object  $entity
     *
     * @return boolean
     */
    public function delete($entity)
    {
        return $this->perform('delete', $entity, [], false);
    }


    /**
     * Toggle pagination.
     *
     * @param  false|int $toggle
     *
     * @return static
     */
    public function paginate($toggle)
    {
        $this->paginate = $toggle === false ? false : (int) $toggle;

        return $this;
    }


    /**
     * Get the connection the repository uses.
     *
     * @return mixed
     */
    protected abstract function getConnection();


    /**
     * Get a new entity instance.
     *
     * @param  array  $attributes
     *
     * @return object
     */
    abstract public function getNew(array $attributes = array());


    /**
	 * Get the name of the primary key to query for.
	 *
	 * @return string
	 */
	abstract protected function getKeyName();


	/**
	 * Get the primary key of an entity.
	 *
	 * @param  object  $entity
	 *
	 * @return mixed
	 */
	protected abstract function getEntityKey($entity);


	/**
	 * Get the name of an entity.
	 *
	 * @param  object  $entity
	 *
	 * @return mixed
	 */
	protected abstract function getEntityName($entity);


    /**
     * Get the attibutes of an entity/model
     *
     * @param object $entity
     * @return array
     */
    abstract protected function getEntityAttributes($entity);


    /**
     * Simple checker to see if the DB driver in use supports storing unserialized arrays
     *
     * This is fairly dumb, and should not be required. This is an implementation level thing so the controller
     * should not have to check for what is supported
     *
     * @TODO: Remove this and find a convenient way of serializing arrays on non-document DB's
     *
     * @return bool
     */
    abstract public function supportsArrays();

}