<?php namespace CommonCore\Core\Repositories;

/**
 * Base Eloquent repository
 *
 * All repositories that use an Eloquent model as a data accessor should extend this repository
 * in order to provide a common set of default methods.
 *
 * @package CommonCore
 * @version 1.0.0
 * @author Lee Sherwood <lee.sherwood@tbcdigital.com>
 */

class EloquentRepository extends AbstractRepository
{

    /**
     * The model which the repository uses in order to do active record lookups
     * @var \CommonCore\Core\Models\Model
     */
    protected $model = null;


    /**
     * Whether to call push() or just save() when creating/updating a model.
     *
     * @var boolean
     */
    protected $push = false;


    /**
     * Constructor
     *
     * @param \CommonCore\Core\Models\Model $model The model used for data access on this repo
     * @param \CommonCore\Core\Validators\ValidatorInterface $validator Optional validation service
     */
    public function __construct(\CommonCore\Core\Models\Model $model, \CommonCore\Core\Validators\ValidatorInterface $validator = null)
    {

        $this->setModel($model);

        parent::__construct($validator);

    }


    /**
     * Lookup a single entity via it's primary key
     *
     * @param string|int $key
     * @return \CommonCore\Core\Models\Model
     */
    public function findByKey($key)
    {

        $query = $this->newQuery()->where($this->getKeyName(), '=', $key);

        return $this->fetchSingle($query);

    }


    /**
     * Lookup a single entity via it's attributes
     *
     * @param array $attributes format array like so: ['column name' => 'column value']
     * @return \CommonCore\Core\Models\Model
     */
    public function findByAttributes($attributes)
    {

        if(empty($attributes)) {

            if($this->throwExceptions) {
                throw new Exception('No attributes for lookup');
            }

            return null;

        }

        $query = $this->buildQueryWithAttributes($this->newQuery(), $attributes, '=');

        return $this->fetchSingle($query);

    }


    /**
     * Return all entities managed by this repository
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getAll()
    {
        return $this->fetchMany($this->newQuery());
    }


    /**
     * Return all entities that match an array of attributes
     *
     * @param array $attributes format array like so: ['column name' => 'column value']
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getByAttributes($attributes)
    {

        if(empty($attributes)) {

            if($this->throwExceptions) {
                throw new Exception('No attributes for lookup');
            }

            return null;

        }

        $query = $this->buildQueryWithAttributes($this->newQuery(), $attributes, '=');

        return $this->fetchMany($query);

    }


    /**
     * Return a count of all entities managed by this repository
     *
     * @return int
     */
    public function getCount()
    {
        return $this->perform('count', $this->newQuery(), false, false);
    }


    /**
     * Return a count of all entities, filtered by the provided attributes
     *
     * @param array $attributes Column = Value pairs to filter the entities by
     * @return int
     */
    public function getCountByAttributes($attributes)
    {

        if(empty($attributes)) {

            if($this->throwExceptions) {
                throw new Exception('No attributes for counting');
            }

            return null;

        }

        $query = $this->buildQueryWithAttributes($this->newQuery(), $attributes, '=');

        return $this->perform('count', $query, false, false);

    }


    /**
     * The count query performer
     *
     * @param Illuminate\Database\Eloquent\Builder $query
     * @return int
     */
    protected function performCount($query)
    {
        return $query->count();
    }


    /**
     * Return a new copy of the managed entity
     *
     * @param array $attributes
     * @return \CommonCore\Core\Models\Model
     */
    public function getNew(array $attributes = [])
    {
    	return $this->model->newInstance($attributes);
    }


    /**
     * Perform a query (execute the query builder basically)
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param bool $many Wether we're fetching one or many entities
     * @return mixed
     */
    protected function performQuery($query, $many)
    {

        if ($many === false) {
            $result = $this->getRegularQueryResults($query, false);

            if (!$result && $this->throwExceptions === true) {
                throw $this->getNotFoundException($query);
            }

            return $result;
        }

        return $this->paginate === false ?
            $this->getRegularQueryResults($query, true) :
            $this->getPaginatedQueryResults($query);

    }


    /**
     * Get regular results from a query builder.
     *
     * @param  mixed   $query
     * @param  boolean $many
     * @return mixed
     */
    protected function getRegularQueryResults($query, $many)
    {
        return $many ? $query->get() : $query->first();
    }


    /**
     * Get paginated results from a query.
     *
     * @param  mixed $query
     * @return mixed
     */
    protected function getPaginatedQueryResults($query)
    {
        return $query->paginate($this->paginate);
    }


    /**
     * {@inheritdoc}
     */
    public function update($model, array $attributes)
    {
        if (!$model->exists) {
            throw new \RuntimeException('Cannot update non-existant model');
        }

        return parent::update($model, $attributes);
    }


    /**
	 * Perform a create action.
	 *
	 * @param  \CommonCore\Core\Models\Model  $model
	 * @param  array   $attributes
	 *
	 * @return \CommonCore\Core\Models\Model|false
	 */
    protected function performCreate($model, array $attributes)
    {
        $model->fill($attributes);

        return $this->perform('save', $model, $attributes, false);
    }


    /**
     * Perform an update action.
     *
     * @param  \CommonCore\Core\Models\Model  $model
     * @param  array   $attributes
     *
     * @return \CommonCore\Core\Models\Model|false
     */
    protected function performUpdate($model, array $attributes)
    {
        $model->fill($attributes);

        return $this->perform('save', $model, $attributes, false);
    }


    /**
     * Perform a save operation on an entity
     *
     * @param  \CommonCore\Core\Models\Model  $model
     * @param  array   $attributes
     *
     * @return \CommonCore\Core\Models\Model|false
     */
    protected function performSave($model, array $attributes)
    {
        $method = $this->push ? 'push' : 'save';

        return $model->$method() ? $model : false;
    }


    /**
     * {@inheritdoc}
     */
    protected function performDelete($model)
    {
        return $model->delete();
    }


    /**
     * Perform a query, fetching multiple rows.
     *
     * @param  \Illuminate\Database\Query\Builder  $query
     * @return mixed
     */
    protected function fetchMany($query)
    {
        return $this->perform('query', $query, true, false);
    }


    /**
     * Perform a query, fetching a single row.
     *
     * @param  \Illuminate\Database\Query\Builder  $query
     * @return mixed
     */
    protected function fetchSingle($query)
    {
        return $this->perform('query', $query, false, false);
    }


    /**
     * Helper method to return an instance of the query builder for the repo model
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function newQuery()
    {
    	return $this->model->newQuery();
    }


    /**
     * Helper method for building an attributed query (one or many WHERE's)
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param array $attributes
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function buildQueryWithAttributes($query, $attributes, $operator = '=')
    {

        foreach($attributes as $column_name => $column_value) {
            $query->where($column_name, $operator, $column_value);
        }

        return $query;
    }


    /**
     * Set the repository's model.
     *
     * @param \CommonCore\Core\Models\Model  $model
     * @return self
     */
    public function setModel($model)
    {
        $this->model = $model;

        return $this;
    }


    /**
     * Get the repository's model.
     *
     * @return \CommonCore\Core\Models\Model
     */
    public function getModel()
    {
        return $this->model;
    }


    /**
     * {@inheritdoc}
     */
    protected function getConnection()
    {
        return $this->model->getConnection();
    }


    /**
     * {@inheritdoc}
     */
    protected function getKeyName()
    {
        return $this->model->getKeyName();
    }

    /**
     * {@inheritdoc}
     */
    protected function getEntityKey($model)
    {
        return $model->getKey();
    }


    /**
     * {@inheritdoc}
     */
    protected function getEntityName($model)
    {
        return str_replace('\\', '', snake_case(strtolower(class_basename($this))));
    }


    /**
     * {@inheritdoc}
     */
    protected function getEntityAttributes($model)
    {
        return $model->getAttributes();
    }

    /**
     * {@inheritdoc}
     */
    public function supportsArrays()
    {
        return in_array(strtolower($this->model->getConnection()->getDriverName()), ['mongo', 'mongodb']);
    }

}