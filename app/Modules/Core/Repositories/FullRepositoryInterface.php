<?php namespace CommonCore\Core\Repositories;

/**
 * This interface enforces a full persistency contract
 *
 * By implementing this interface, you can ensure that the final repository
 * supports all generic persistancy and lookup methods.
 *
 * @author Lee Sherwood <lee.sherwood@tbc-digital.com>
 * @package CommonCore
 * @version 1.0.0
 */

interface FullRepositoryInterface
{

    /**
     * Create and persist a new entity with the given attributes.
     *
     * @param  array  $attributes
     *
     * @return object|false
     */
    public function create(array $attributes);


    /**
     * Update an entity with the given attributes and persist it.
     *
     * @param  object  $entity
     * @param  array   $attributes
     *
     * @return boolean
     */
    public function update($entity, array $attributes);


    /**
     * Delete an entity.
     *
     * @param  object  $entity
     *
     * @return boolean
     */
    public function delete($entity);


    /**
     * Lookup a single entity via it's primary key
     *
     * @param string|int $key
     * @return mixed
     */
    public function findByKey($key);


    /**
     * Lookup a single entity via it's attributes
     *
     * @param array $attributes format array like so: ['column name' => 'column value']
     * @return mixed
     */
    public function findByAttributes($attributes);


    /**
     * Return all entities managed by this repository
     *
     * @return array
     */
    public function getAll();


    /**
     * Return all entities that match an array of attributes
     *
     * @param array $attributes format array like so: ['column name' => 'column value']
     * @return array
     */
    public function getByAttributes($attributes);

    /**
     * Return a count of all entities managed by this repository
     *
     * @return int
     */
    public function getCount();


    /**
     * Return a count of all entities, filtered by the provided attributes
     *
     * @param array $attributes Column = Value pairs to filter the entities by
     * @return int
     */
    public function getCountByAttributes($attributes);

}