<?php namespace CommonCore\Core\Controllers;

use Illuminate\Routing\Controller;


/**
 * Inheritable base controller
 *
 * This controller implements a lot of helper methods. Only methods that would work across all
 * routes and view types (i.e. html view as well as json) should be implemented here.
 *
 * @author Lee Sherwood <lee.sherwood@tbc-digital.com>
 * @package CommonCore
 * @version 1.0.0
 */

class BaseController extends Controller
{

    /**
     * Get the logged in user account
     *
     * This method gets the logged in user account by using the Auth facade
     * ideally we would inject the authentication driver as a dependency
     * but that doesn't seem to work as we really need an instance of Guard which can only be
     * resolved from the facade
     *
     * @return null || \CommonCore\Users\User
     */
    public function getAuthenticatedUser()
    {
        return \Auth::user();
    }


    /**
     * General response method
     * @param mixed $responseData The data to send back to the client
     * @param int $statusCode The HTTP status code for the response
     */
    public function respond($responseData, $statusCode=200)
    {
        return \Response::json($responseData, $statusCode);
    }


    /**
     * Return a successful response to the client
     * @param mixed $responseData The data to send back to the client
     * @see self::respond()
     */
    public function respondWithSuccess($responseData)
    {

        if(is_string($responseData)) {
            $responseData = ['message' => $responseData];
        }

        return $this->respond($responseData, 200);

    }


    /**
     * Return a forbidden response
     * @param string $message Optional error message
     * @see self::respond()
     */
    public function respondWithForbidden($message = "You are not permitted to carry out the requested action")
    {
        return $this->respond(['message' => $message], 403);
    }


    /**
     * Return a not-found response
     * @param string $message Optional error message
     * @see self::respond()
     */
    public function respondWithNotFound($message = "The requested resource could not be found")
    {
        return $this->respond(['message' => $message], 404);
    }


    /**
     * Return a general error response
     * @param string $responseData Optional error message or complex response data
     * @see self::respond()
     */
    public function respondWithError($responseData = "An error was detected in your request")
    {

        if(is_string($responseData)) {
            $responseData = ['message' => $responseData];
        }

        return $this->respond($responseData, 400);

    }


    /**
     * Return an error for a form submission (i.e. Validation error on register)
     * @param array $errors An array of errors to return (usually the messagebag from the validator)
     * @see self::respond()
     */
    public function respondWithFormErrors($errors = [])
    {
        return $this->respond(['message' => 'There were errors in your submission', 'issues' => $errors], 400);
    }

    /**
     * Return a server failure that was not the clients fault (i.e. failed to save entity to database)
     * @param string $message Optional error message
     * @see self::respond()
     */
    public function respondWithInternalError($message = "Service encountered an unrecoverable error")
    {
        return $this->respond(['message' => $message], 500);
    }

}