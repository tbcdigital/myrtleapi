<?php namespace CommonCore\Core\Models;

use Jenssegers\Mongodb\Model as Eloquent;

/**
 * Default model class which all module models should extend
 *
 * The most important thing about this model class is that you can easily change the extended class in order
 * to support other persistency drivers, such as the mongodb eloquent driver
 *
 * @author Lee Sherwood <lee.sherwood@tbc-digital.com>
 * @package CommonCore
 * @version 1.0.0
 */

class Model extends Eloquent
{

}