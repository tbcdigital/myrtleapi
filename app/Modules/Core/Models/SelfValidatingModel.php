<?php namespace CommonCore\Core\Models;

use Closure;
use \Illuminate\Support\MessageBag;
use \Validator;
use \Str;

abstract class SelfValidatingModel extends Model
{

    /**
     * The message bag instance containing validation error messages
     *
     * @var \Illuminate\Support\MessageBag
     */
    public $validationErrors;


    /**
     * If set to true, the object will automatically remove redundant model
     * attributes (i.e. confirmation fields).
     *
     * @var bool
     */
    public $autoPurgeRedundantAttributes = true;


    /**
     * Attributes to purge before saving the model
     *
     * @var array
     */
    protected static $purgeable = [];


    /**
     * @var bool Designates if the model is valid after validation
     */
    protected $valid = null;


    /**
     * The rules for the validator
     *
     * @var array
     */
    protected static $rules = [
    	'save'   => [],
    	'create' => [],
    	'update' => []
    ];


    /**
     * Set up the mesasge bag
     */
    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        $this->validationErrors = new MessageBag;
        $this->valid = null;
    }


    /**
     * Validate the model attributes
     *
     * @return boolean
     */
    public function validate()
    {

        $passed = true;

        $rules = $this->getRuleset();

        if(!empty($rules)) {

            $validator = Validator::make($this->getAttributes(), $rules);

            if($validator->fails()) {

                if ($this->validationErrors->count() > 0) {
                    $this->validationErrors = new MessageBag;
                }

                $this->validationErrors = $this->cleanupValidationMessages($validator->messages());

                $passed = false;

            }

        }

        $this->valid = $passed;

        return $passed;

    }


    /**
     * Get the rules for the validator
     *
     * @return array
     */
    protected function getRuleset()
    {

        // Make the rules array
        $rulesDef = static::$rules;
        $mergeWithKey = $this->exists ? 'update' : 'create';
        $mergeWith = isset($rulesDef[$mergeWithKey]) ? $rulesDef[$mergeWithKey] : [];
        $rules = array_merge_recursive($rulesDef['save'], $mergeWith );

        //build unique exclusions
        $rules = $this->buildUniqueExclusionRules($rules);

        return $rules;

    }


    /**
     * When given an ID and a Laravel validation rules array, this function
     * appends the ID to the 'unique' rules given. The resulting array can
     * then be fed to a Ardent save so that unchanged values
     * don't flag a validation issue. Rules can be in either strings
     * with pipes or arrays, but the returned rules are in arrays.
     *
     * @param int   $id
     * @param array $rules
     *
     * @return array Rules with exclusions applied
     */
    protected function buildUniqueExclusionRules(array $rules = array()) {

        foreach ($rules as $field => &$ruleset) {
            // If $ruleset is a pipe-separated string, switch it to array
            $ruleset = is_string($ruleset) ? explode('|', $ruleset) : $ruleset;

            foreach ($ruleset as &$rule) {

                if (strpos($rule, 'unique') === 0) {
                    // Stop splitting at 4 so final param will hold optional where clause
                    $params = explode(',', $rule, 4);

                    $uniqueRules = array();

                    // Append table name if needed
                    $table = explode(':', $params[0]);
                    if (count($table) == 1)
                        $uniqueRules[1] = $this->table;
                    else
                        $uniqueRules[1] = $table[1];

                    // Append field name if needed
                    if (count($params) == 1)
                        $uniqueRules[2] = $field;
                    else
                        $uniqueRules[2] = $params[1];

                    $uniqueRules[3] = $this->getKey();
                    $uniqueRules[4] = $this->getKeyName();


                    $rule = 'unique:' . implode(',', $uniqueRules);
                } // end if strpos unique

            } // end foreach ruleset
        }

        return $rules;

    }


    /**
     * Save the model
     *
     * Override eloquents save to implement model validation
     *
     * @param array $options
     * @return boolean
     */
    public function save(array $options = array())
    {

        if( (!isset($options['force']) || $options['force'] === false) && false === $this->validate()) {
            return false;
        }

        if ($this->autoPurgeRedundantAttributes) {
            $this->purgeAttributes();
        }

        return parent::save($options);

    }


    /**
     * Purges unneeded fields by getting rid of all attributes
     * ending in '_confirmation' or starting with '_'
     *
     * @return array
     */
    protected function purgeAttributes()
    {

        $clean = array();
        foreach ($this->attributes as $key => $value)
        {
            if (! Str::endsWith($key, '_confirmation') && ! in_array($key, static::$purgeable))
                $clean[$key] = $value;
        }

        $this->attributes = $clean;

    }


    /**
     * Returns validationErrors MessageBag
     *
     * @return MessageBag
     */
    public function errors()
    {
        return $this->validationErrors;
    }


    /**
     * Returns if the model has passed validation
     *
     * @return bool
     */
    public function isValid()
    {

        if(null === $this->valid) {
            $this->validate();
        }

        return $this->valid;

    }


    /**
     * Set a given attribute on the model.
     *
     * Overriden to nullify validation
     *
     * @param  string $key
     * @param  mixed  $value
     *
     * @return void
     */
    public function setAttribute($key, $value)
    {
        if ($key != $this->getKeyName() && !in_array($key, $this->getDates())) {
            $this->valid = null;
        }

        return parent::setAttribute($key, $value);
    }


    /**
     * Cleans up the message bag after validation
     *
     * @param MessageBag $messages
     * @return \Illuminate\Support\MessageBag
     */
    protected function cleanupValidationMessages(MessageBag $messages)
    {

        $newMessageBag = new MessageBag;

        foreach($messages->getMessages() as $msgKey => $msgs) {

            foreach($msgs as $msg) {

                if( ($originalKey = stristr($msgKey, '_tolower', true)) !== false) {

                    $newMessageBag->add($originalKey, $msg);

                } else {

                    $newMessageBag->add($msgKey, $msg);

                }

            }

        }

        return $newMessageBag;
    }

}