<?php

//MOVED FROM NOTIFICATIONSCONTROLLER

use CommonCore\Users\User;
use \Carbon\Carbon;


class Notifications extends \BaseController {


	/**
	 * User repository
	 *
	 * @var CommonCore\Users\UserRepositoryInterface
	 */
	protected $userRepo;

	
	/**
	 * Passed-in Story
	 *
	 * @var Story obj
	 */
	protected $story=null;

	/**
	 * Passed in or discovered notif
	 *
	 * @var  obj
	 */
	protected $notif=null;
	
	/*
	public function __construct(CommonCore\Users\UserRepositoryInterface $userRepo){
		$this->userRepo = $userRepo;
	}	
	*/
	
	public function add_notification($user, $notif) {
		
		$this->notif = $notif;	
		
		$notif['read'] = 0;
		$notif['_id'] = (string) new MongoId;
	
		$redis = RedisL4::connection();
	
		$redis->zAdd('user:'.$user->getKey().':notifications', Carbon::now()->timestamp, json_encode($notif));
	
		$this->crop_notifications_for_user($user);
	
		/**
		 * This is the push notification bit.
		 * This should be moved into a proper library/class but we are pressed for time
		*/
		if(!is_null($user->notification_tokens)) {
	
			switch($notif['event']) {
	
				case 'ownsticker.liked':
					$msg = User::find($notif['user'])->username.' just liked your sticker';
					$target = 'stickerzap://stickers/'.$notif['sticker'];
					break;
				case 'ownphoto.liked':
					$msg = User::find($notif['user'])->username.' just liked your pic';
					$target = 'stickerzap://photos/'.$notif['photo'];
					break;
				case 'ownsticker.comment':
					$msg = User::find($notif['user'])->username.' just commented on your sticker';
					$target = 'stickerzap://stickers/'.$notif['sticker'];
					break;
				case 'ownphoto.comment':
					$msg = User::find($notif['user'])->username.' just commented on your pic';
					$target = 'stickerzap://photos/'.$notif['photo'];
					break;
				case 'ownsticker.purchased':
					$msg = User::find($notif['user'])->username.' just added your sticker';
					$target = 'stickerzap://stickers/'.$notif['sticker'];
					break;
				case 'friendrequest.incoming':
					$msg = User::find($notif['user'])->username.' wants to be friends';
					$target = 'stickerzap://users/'.$notif['user'];
					break;
				case 'friendrequest.accepted':
					$msg = User::find($notif['user'])->username.' accepted your friend request';
					$target = 'stickerzap://users/'.$notif['user'];
					break;
				case 'mentioned.sticker':
					$msg = User::find($notif['user'])->username.' just mentioned you on their sticker';
					$target = 'stickerzap://stickers/'.$notif['sticker'];
					break;
				case 'mentioned.photo':
					$msg = User::find($notif['user'])->username.' just mentioned you on their pic';
					$target = 'stickerzap://photos/'.$notif['photo'];
					break;
				case 'mentioned.stickercomment':
					$msg = User::find($notif['user'])->username.' just mentioned you in their sticker comment';
					$target = 'stickerzap://stickers/'.$notif['sticker'];
					break;
				case 'mentioned.photocomment':
					$msg = User::find($notif['user'])->username.' just mentioned you in their pic comment';
					$target = 'stickerzap://photos/'.$notif['photo'];
					break;
	
				default:
					continue 2;
			}
	
	
			foreach($user->notification_tokens as $td) {
	
				if($td['deviceType'] == 'ios') {
	
					$is_dev_token = isset($td['development']) && $td['development'] == 1 ? true : false;
	
	
					$stream_context = stream_context_create();
					stream_context_set_option($stream_context, 'ssl', 'local_cert', storage_path('certificates/apns/StickerZapAPNS'.($is_dev_token ? 'Dev' : 'Prod').'.pem'));
					stream_context_set_option($stream_context, 'ssl', 'passphrase', '567&nice&elements&272');
					$stream_client = @stream_socket_client("ssl://gateway".($is_dev_token ? '.sandbox' : '').".push.apple.com:2195", $err,	$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_ASYNC_CONNECT|STREAM_CLIENT_PERSISTENT, $stream_context);
					$body = array('aps' => $payload = array(
							"sound" => "default",
							"alert" => $msg,
							"target" => $target
					));
					$payload = json_encode($body);
					$message = chr(0) . pack('n', 32) . pack('H*', strtr($td['token'], array('>' => '', '<' => '', ' '=>''))) . pack('n', strlen($payload)) . $payload;
	
					// Send it to the server
					$result = fwrite($stream_client, $message, strlen($message));
	
					// Close the connection to the server
					fclose($stream_client);
	
				}
	
			}
	
		}
	
	}

	
	private function crop_notifications_for_user($user) {
	
		$maxcount = 10;
		$maxcount += 1;
		$maxcount = $maxcount * -1;
	
		$redis = RedisL4::connection();
	
		$redis->zRemRangeByRank('user:'.$user->getKey().':notifications', 0, $maxcount);
	
	}
	
	
}