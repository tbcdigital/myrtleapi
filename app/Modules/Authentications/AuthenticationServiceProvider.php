<?php namespace CommonCore\Authentications;

use Illuminate\Support\ServiceProvider;

/**
 * Service provider for loading the Authentication module
 *
 * @author Lee Sherwood <lee.sherwood@tbc-digital.com>
 * @package CommonCore
 * @version 1.0.0
 */

class AuthenticationServiceProvider extends ServiceProvider
{

    /**
     * Whether to defer loading until it's actually required (should be false if registering routes)
     *
     * @var bool
     */
    protected $defer = false;


    /**
     * Register bindings
     */
    public function register()
    {

        $this->app->bind('CommonCore\\Authentications\\AuthenticationRepositoryInterface', 'CommonCore\\Authentications\\AuthenticationRepository');
        $this->app->bind('Tbcdigital\Tbcauth\Repositories\TbcauthAuthRepositoryInterface', 'CommonCore\\Authentications\\AuthenticationRepository');

    }


    /**
     * Boot is run after all SP's are registered
     *
     * This method should be used when the actions being taken require other service providers to be loaded (such as the Router)
     *
     * @return void
     */
    public function boot()
    {
        $this->registerRoutes();
    }


    /**
     * If there is a routes file, then load it
     */
    public function registerRoutes()
    {

        $routes_file = dirname(__FILE__).'/routes.php';
        if(file_exists($routes_file)) {
            require_once $routes_file;
        }

    }

}