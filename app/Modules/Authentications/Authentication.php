<?php namespace CommonCore\Authentications;

/**
 * The authentication model
 *
 * This model/entity is used by authentication providers to save their login information against an account.
 * The only exception to this, is authentication providers whom use personal user attributes such as email/password
 * in order to perform their respective login operations.
 *
 * @author Lee Sherwood <lee.sherwood@tbc-digital.com>
 * @package CommonCore
 * @version 1.0.0
 *
 * @property \CommonCore\Users\User $user The associated user account
 */

class Authentication extends \CommonCore\Core\Models\Model
{

    /**
     * Attributes which can be mass-assigned
     *
     * @var array
     */
    protected $fillable = ['provider', 'provider_id', 'credentials'];


    /**
     * Attributes which are hidden from output
     *
     * @var array
     */
    protected $hidden = ['credentials', 'user_id'];


    /**
     * All authentication entities must belong to a user
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('\CommonCore\Users\User');
    }

}