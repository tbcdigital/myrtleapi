<?php namespace CommonCore\Authentications;

use \CommonCore\Core\Validators\AbstractValidator;

/**
 * Validation service for the Authentication entity
 *
 * @author Lee Sherwood <lee.sherwood@tbc-digital.com>
 * @package CommonCore
 * @version 1.0.0
 */

class AuthenticationValidator extends AbstractValidator {

    protected $rules = [
	   'common' => [
            'provider' => ['required'],
            'provider_id' => ['required']
        ]
    ];

}