<?php

/**
 * Routes file for authentication
 *
 * @author Lee Sherwood <lee.sherwood@tbc-digital.com>
 * @package CommonCore
 * @version 1.0.0
 */

Route::group(['prefix' => 'v1'], function(){

    // The controller prefix
    $namespace = 'CommonCore\\Authentications\\';

    // Routes requiring you be already authenticated
    Route::group(['before' => 'auth'], function() use ($namespace) {

        // Deauthenticate
        Route::delete('auth/{via}', $namespace.'AuthenticationController@deauthenticate');
        Route::delete('auth', $namespace.'AuthenticationController@logout');

        // Get authentications
        Route::get('auth', $namespace.'AuthenticationController@getAuthentications');
        Route::get('auth/{provider}', $namespace.'AuthenticationController@getAuthentications');

    });

    // Authenticate
    Route::post('auth/{via}', $namespace.'AuthenticationController@authenticate');

});