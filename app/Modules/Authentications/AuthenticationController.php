<?php namespace CommonCore\Authentications;

use \Tbcauth;
use \Auth;
use \Input;
use CommonCore\Users\UserRepositoryInterface;

/**
 * The authentication controller
 *
 * This controller manages:
 * - Attaching Social user accounts to an existing account
 * - Creating a new user from social login (if enabled in config)
 * - Creating guest accounts for UDID's (if enabled in config)
 * - Logging in via UDID, social network and form attributes (email/username/password)
 * - Removing authentication routes
 *
 * @author Lee Sherwood <lee.sherwood@tbc-digital.com>
 * @package CommonCore
 * @version 1.0.0
 */

class AuthenticationController extends \CommonCore\Core\Controllers\BaseController
{

    /**
     * The authentication repository
     *
     * @var AuthenticationRepositoryInterface
     */
    protected $authRepo;


    /**
     * The user repository
     *
     * @var UserRepositoryInterface
     */
    protected $userRepo;


    /**
     * Construct the controller injecting the required persistance repositories
     *
     * @param AuthenticationRepositoryInterface $authRepo
     * @param UserRepositoryInterface $userRepo
     */
    public function __construct(AuthenticationRepositoryInterface $authRepo, UserRepositoryInterface $userRepo)
    {
        $this->authRepo = $authRepo;
        $this->userRepo = $userRepo;
    }


    /**
     * Get all active authentications for the currently logged in user
     *
     * @param string $provider optional provider filter
     */
    public function getAuthentications($provider = null)
    {

        if(null === $provider) {
            $authentications = $this->authRepo->getByUserId(Auth::user()->getKey());
        } else {
            $authentications = $this->authRepo->getByUserIdAndProvider(Auth::user()->getKey(), $provider);
        }

        return $this->respondWithSuccess(['authentications' => $authentications->toArray()]);

    }


    /**
     * All authentication requests are handled by this method
     *
     * @param string $provider
     * @throws \Exception
     * @return mixed
     */
    public function authenticate($provider)
    {

        // Check the provider exists
        if(false === Tbcauth::isProviderLoaded($provider)) {
            return $this->respondWithNotFound('Authentication route not available');
        }

        // Get a reference to the provider
        $providerDriver = Tbcauth::getProvider($provider);

        // Attempt to resolve the provider credentials to a local user account
        $user = $providerDriver->resolveUser($provider, \Input::all());

        // If there are errors on the provider, then it failed not because it doesnt exist, but because the credentials dont work with the provider
        if($providerDriver->hasErrors()) {
            $providerErrors = $providerDriver->getErrors();
            return $this->respondWithError(array_shift($providerErrors));
        }

        // If theres no errors on the provider, then the credentials work
        $mergeableUser = $providerDriver->getMergeableUser(); // null or socialuser
        $providerCredentials = $providerDriver->getAuthenticationCredentials($provider);

        // Failed to resolve local user account
        if(null === $user) {

            if(false === Auth::check()) {

                if(null === $mergeableUser) {
                    // No authenticated user, so see if we can create an account (provider must have a mergeable user)

                    return $this->respondWithNotFound('User not found');

                } else if (false === $providerDriver->shouldCreateForNotFound()) {
                    // There's a mergeable user, but config dictates we shouldnt create accounts automatically (i.e. you may only link)

                    return $this->respond([ 'message' => 'User not found', 'provider_user' => is_array($mergeableUser) ? $mergeableUser : $mergeableUser->toArray() ], 404);

                } else {

                    // Mergeable account found, so try to create the account
                    $user = $this->userRepo->createFromProvider( is_array($mergeableUser) ? $mergeableUser : $mergeableUser->toArray() );
                    if(false === $user) {

                       return $this->respondWithFormErrors( $this->userRepo->getErrors() );

                    }

                    $this->authRepo->createForUserId($user->getKey(), [
	                    'provider' => $provider,
                        'provider_id' => $providerDriver->getLinkId(),
                        'credentials' => $this->authRepo->supportsArrays() ? $providerCredentials : json_encode($providerCredentials)
                    ]);

                    Auth::login($user);

                    return $this->respondWithSuccess(['user' => $user->toArray()]);

                }

            } else /* if is authenticated */ {

                if(false == $providerDriver->isLinkable()) {
                    return $this->respondWithError('User not found');
                }

                $this->authRepo->createForUserId(Auth::user()->getKey(), [
                    'provider' => $provider,
                    'provider_id' => $providerDriver->getLinkId(),
                    'credentials' => $this->authRepo->supportsArrays() ? $providerCredentials : json_encode($providerCredentials)
                ]);

                return $this->respondWithSuccess(['message' => $provider.' connected']);


            }

        } else /* If a local user account was found */ {

            // If not logged in yet, then log them in
            if( ! Auth::check() ) {

                // If it's linkable then we need to keep our creds in auth up to date
                if($providerDriver->isLinkable()) {

                    $authEntity = $providerDriver->getLookupEntity();

                    $this->authRepo->update($authEntity, ['credentials' => $this->authRepo->supportsArrays() ? $providerCredentials : json_encode($providerCredentials) ]);

                }

                Auth::login($user);

                return $this->respondWithSuccess(['user' => $user->toArray()]);

            } else if (Auth::user()->getKey() === $user->getKey()) /* Logged in and already connected */ {

                // If its linkable and we're already logged in, as above we just want to ensure we keep the latest creds
                if($providerDriver->isLinkable()) {

                    $authEntity = $providerDriver->getLookupEntity();

                    $this->authRepo->update($authEntity, ['credentials' => $this->authRepo->supportsArrays() ? $providerCredentials : json_encode($providerCredentials) ]);

                    $this->respondWithSuccess(['message' => $provider.' connected']);

                }

                return $this->respondWithSuccess(['user' => $user->toArray()]);

            } else {

                // If your logged in AND you attempt to authenticate as someone else then we merge
                die('merge em');

            }


        }

        // If you get to this point of script execution, something went wrong
        throw new \Exception('Oops');

    }


    /**
     * De-authenticate a specific provider for the authenticated account
     *
     * This controller method will remove an authenticate provider for the authenticated user
     * by either the authentication database id (this is the most robust method), or by a given provider and provider_id
     * or simply rmeove all authentication routes for a specific provider alias.
     *
     * @param string $provider
     */
    public function deauthenticate($provider)
    {

        // Get the logged in user
        $user = $this->getAuthenticatedUser();

        // Before allowing deauthentication, we need to check they will still have an authentication route available afterwards
        $auth_counter = $this->authRepo->getCountForUserId($user->getKey());

        if($auth_counter <= 1 && empty($user->password)) {

            return $this->respondWithError('Cannot remove final authentication route');

        }

        $singular = true;

        // If provider is id, then we are deauthenticating based on database id
        if(strtolower($provider) == 'id') {

            $entity = $this->authRepo->findByKey(Input::get('id'));

        } else {

            // Provider must exist!
            if(false === Tbcauth::isProviderLoaded($provider)) {
                return $this->respondWithNotFound('Authentication route not available');
            }


            // If theres a ?provider_id then we are getting a single authentication entry
            if(Input::has('provider_id')) {

                $entity = $this->authRepo->findByUserIdAndProvider($user->getKey(), $provider, Input::get('provider_id'));

            } else {

                // If all we have is a provider, then we only get for the auth'd user
                $singular = false;
                $entities = $this->authRepo->getByUserIdAndProvider($user->getKey(), $provider);
            }

        }

        // If its a singular removal (i.e. not "remove all for provider alias") then we are a little more
        // specific over the response sent back to the client.
        if($singular) {

            if(null === $entity) {

                return $this->respondWithNotFound();

            } else if($entity->user_id != $user->getKey()) {

                return $this->respondWithForbidden();

            } else {

                $this->authRepo->delete($entity);

                return $this->respondWithSuccess('Deauthentication successful');

            }

        } else {

            foreach($entities as $entity) {

                if(null !== $entity && $entity->user_id == $user->getKey()) {

                    $this->authRepo->delete($entity);

                }

            }

            return $this->respondWithSuccess('Deauthentication successful');

        }

    }
    
    
    /**
     * A very basic logout method used when removal of the authentication route is not required
     */
    public function logout()
    {
    	
    	Auth::logout();
    	return $this->respondWithSuccess('Logout successful');
    
    }

}