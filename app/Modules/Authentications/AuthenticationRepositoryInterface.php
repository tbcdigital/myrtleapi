<?php namespace CommonCore\Authentications;

/**
 * The interface class defining what methods must exist on our authentication repository
 *
 * @author Lee Sherwood <lee.sherwood@tbc-digital.com>
 * @package CommonCore
 * @version 1.0.0
 */

interface AuthenticationRepositoryInterface extends \CommonCore\Core\Repositories\FullRepositoryInterface
{

    /**
     * Create an Authentication entry for a user
     *
     * @param int|string $userId
     * @param array $attributes
     */
    public function createForUserId($userId, $attributes);


    /**
     * Find a single authentication entry for a specific provider
     *
     * @param string $provider
     * @param string|int $provider_id
     */
    public function findByProvider($provider, $provider_id);


    /**
     * Find a single authentication entry for a specific provider and user
     *
     * @param int|string $userId
     * @param string $provider
     * @param int|string $provider_id
     */
    public function findByUserIdAndProvider($userId, $provider, $provider_id);


    /**
     * Get all authentication entities for a user
     *
     * @param int|string $userId
     */
    public function getByUserId($userId);


    /**
     * Get a count of all authentication entities for a given user
     *
     * @param int|string $userId
     * @return int
     */
    public function getCountForUserId($userId);


    /**
     * Get all authentication entities for a user and specific provider
     *
     * @param int|string $userId
     * @param string $provider
     */
    public function getByUserIdAndProvider($userId, $provider);

}