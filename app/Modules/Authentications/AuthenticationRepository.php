<?php namespace CommonCore\Authentications;

use \Tbcdigital\Tbcauth\Repositories\TbcauthAuthRepositoryInterface;
use CommonCore\Core\Repositories\EloquentRepository;

/**
 * Our authentication repository implementation
 *
 * @author Lee Sherwood <lee.sherwood@tbc-digital.com>
 * @package CommonCore
 * @version 1.0.0
 */

class AuthenticationRepository extends EloquentRepository implements AuthenticationRepositoryInterface, TbcauthAuthRepositoryInterface
{

    /**
     * Construct an instance of the authentication repository
     *
     * @param Authentication $model
     * @param AuthenticationValidator $validator
     */
    public function __construct(Authentication $model, AuthenticationValidator $validator)
    {
        parent::__construct($model, $validator);
    }


    /**
     * {@inheritdoc}
     */
    public function createForUserId($userId, $attributes)
    {
        $this->perform('createForUserId', $this->getNew(), array_merge($attributes, ['user_id' => $userId]), true);
    }


    /**
     * Performs the "Create for user id" action
     *
     * @see self::createForUserId()
     * @param Authentication $model
     * @param array $attributes
     * @return Authentication|false
     */
    protected function performCreateForUserId($model, array $attributes)
    {
        $model->fill($attributes);
        $model->{$model->user()->getForeignKey()} = $attributes['user_id'];
        return $this->perform('save', $model, $attributes, false);
    }


    /**
     * {@inheritdoc}
     */
    public function findByProvider($provider, $provider_id)
    {
        return $this->findByAttributes(['provider' => $provider, 'provider_id' => $provider_id]);
    }


    /**
     * {@inheritdoc}
     */
    public function findByUserIdAndProvider($userId, $provider, $provider_id)
    {
        return $this->findByAttributes(['user_id' => $userId, 'provider' => $provider, 'provider_id' => $provider_id]);
    }


    /**
     * {@inheritdoc}
     */
    public function getByUserId($userId)
    {
        return $this->getByAttributes(['user_id' => $userId]);
    }


    /**
     * {@inheritdoc}
     */
    public function getByUserIdAndProvider($userId, $provider)
    {
        return $this->getByAttributes(['user_id' => $userId, 'provider' => $provider]);
    }


    /**
     * {@inheritdoc}
     */
    public function getCountForUserId($userId)
    {
        $q = $this->buildQueryWithAttributes($this->newQuery(), ['user_id' => $userId]);
        return $q->count();
    }

}