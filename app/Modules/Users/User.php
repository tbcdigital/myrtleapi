<?php namespace CommonCore\Users;

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

use Illuminate\Support\Facades\DB;
/**
 * The user model
 *
 * @author Lee Sherwood <lee.sherwood@tbc-digital.com>
 * @package CommonCore
 * @version 1.0.0
 *
 * @property \CommonCore\Authentications\Authentication[] $authentications The associated authentication entites
 */

class User extends \CommonCore\Core\Models\Model implements UserInterface, RemindableInterface
{

    use UserTrait, RemindableTrait;

    /**
     * Attributes which should be hidden when serializing the model
     *
     * @var array
     */
    protected $hidden = ['password', 'email'];

    
    /**
     *	Retrofit.
     *	Sets a USER to be public on Create. 
     *  Sets the DESCRIPTION to be ' ' on Create.
     */
    protected $attributes = [
    'privacy' => 'public',
    'description' => ' '
    ];

    /**
     * Attributes which can be mass-assigned
     *
     * @var array
     */
    protected $fillable = ['email', 'username', 'password', 'first_name', 'last_name', 'dob', 'profile_image', 'cover_image', 'gender', 'privacy', 'description'];


    /**
     * Date attributes are listed here so they are converted to Carbon objects
     *
     * @var array
     */
    protected $dates = ['dob'];

    /**
     *
     *
     * @var unknown
     */
    public $imageBaseUrl = "http://myrtle.s3.amazonaws.com/originals/";
    
    
    /**
     * Hash the password automatically when setting it.
     */
    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = \Hash::make($value);
    }


    /**
     * Users can have multiple authentications
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function authentications()
    {
        return $this->hasMany('\CommonCore\Authentications\Authentication');
    }
   
    
    public function getProfileImageAttribute( $existingURLstring ){
    
    	$existingURLstring =  ltrim($existingURLstring , "/");
    
    	//check if $existingURLstring starts with 'http'.  If so, this is
    	//already a fully-formed URL (somehow) and can be returned as-is.
    
    	if( substr($existingURLstring, 0, 4) == 'http' ){
    		return $existingURLstring;
    	}
    	//if not, it is only the image name, and we have to mutate it to add
    	//all domain information.
    	$userID = $this->getKey();
    	$basicURL = $this->imageBaseUrl . $userID . "/";
    
    	$newURLstring =  $basicURL . $existingURLstring;
    
    	//dd($newURLstring);
    
    	return $newURLstring;
    }
    
    public function getCoverImageAttribute( $existingURLstring ){
    
    	$existingURLstring =  ltrim($existingURLstring , "/");
    
    	//check if $existingURLstring starts with 'http'.  If so, this is
    	//already a fully-formed URL (somehow) and can be returned as-is.
    
    	if( substr($existingURLstring, 0, 4) == 'http' ){
    		return $existingURLstring;
    	}
    	//if not, it is only the image name, and we have to mutate it to add
    	//all domain information.
    	$userID = $this->getKey();
    	$basicURL = $this->imageBaseUrl . $userID . "/";
    
    	$newURLstring =  $basicURL . $existingURLstring;
    
    	//dd($newURLstring);
    
    	return $newURLstring;
    }
    
    
    /**
     * Add a push token for a user
     * @param String $token The push token
     * @param String $type The push token type (i.e. ios)
     */
    public function addPushToken($device, $token, $type, $development=false) {
    	$this->getModelQueryBuilder()->push('notification_tokens', array(
    			'deviceType' => $type,
    			'deviceId' => $device,
    			'token' => $token,
    			'development' =>$development,
    	), true);
    }
    
    public function clearPushTokens($device=null) {
    	$this->getModelQueryBuilder()->pull('notification_tokens', array('deviceId' => $device));
    }   

    /**
     * Gets a new query builder with the model's unique lookup key auto applied to the where filter
     *
     * @return Illuminate\Database\Query\Builder
     */
    public function getModelQueryBuilder() {
    	 
    	return $this->newQuery()->where($this->getKeyName(), $this->getKey());
    
    }

}