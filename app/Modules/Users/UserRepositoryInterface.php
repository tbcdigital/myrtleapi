<?php namespace CommonCore\Users;

/**
 * The interface class defining what methods must exist on our user repository
 *
 * @author Lee Sherwood <lee.sherwood@tbc-digital.com>
 * @package CommonCore
 * @version 1.0.0
 */

interface UserRepositoryInterface extends \CommonCore\Core\Repositories\FullRepositoryInterface
{

    /**
     * Use this method to resolve a user account from some sort of identifier
     *
     * This method uses fallbacks in order to find the correct user account. This method should
     * always return the correct account (or nothing at all) and should only ever use unique attributes
     * during the discovery process
     *
     * @param mixed $id
     * @return User|null
     */
    public function discover($id);


    /**
     * Lookup a user by providing their username
     *
     * This would normally be a passthru method to findByAttribute, but by using this
     * we can define a different human-readable (and unique) key in replace of a username
     * where a username is not required for the project
     *
     * @param string $username
     */
    public function findByUsername($username);


    /**
     * Create a user via a provider rather than form registration (changes validator)
     *
     * @param unknown $attributes
     * @return User|false
     */
    public function createFromProvider($attributes);

}