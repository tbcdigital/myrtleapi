<?php namespace CommonCore\Users;

/**
 * The user controller
 *
 * This controller manages:
 * - Fetching a user profile
 * - Updating a user profile
 * - Creating/Registering a user
 *
 * @author Lee Sherwood <lee.sherwood@tbc-digital.com>
 * @package CommonCore
 * @version 1.0.0
 */

class UserController extends \CommonCore\Core\Controllers\BaseController
{

    /**
     * The User repository
     *
     * @var UserRepositoryInterface
     */
    protected $userRepo;


    /**
     * Constructor
     *
     * @param UserRepositoryInterface $userRepo
     */
    public function __construct(UserRepositoryInterface $userRepo) {
        $this->userRepo = $userRepo;
    }


    /**
     * Show user profile
     *
     * @param string $id User id to lookup
     */
    public function getUserProfile($id)
    {

        if(null === ($user = $this->userRepo->discover($id))) {
            return $this->respondWithNotFound();
        }

        return $this->respondWithSuccess(['user' => $this->serialiseUser($user)]);

    }


    /**
     * Update a users profile
     *
     * @param string $id Use rid to update
     */
    public function updateUserProfile($id)
    {

        if(null === ($user = $this->userRepo->discover($id))) {
            return $this->respondWithNotFound();
        }

        if($this->getAuthenticatedUser()->getKey() != $user->getKey()) {
            return $this->respondWithForbidden();
        }

        if(false === $this->userRepo->update($user, \Input::all())) {

            if($this->userRepo->hasErrors()) {

               return $this->respondWithFormErrors($this->userRepo->getErrors()->toArray());

            } else {

                return $this->respondWithInternalError();

            }

        }

        return $this->respondWithSuccess(['user'=>$this->serialiseUser($user)]);

    }


    /**
     * Register a new user account
     */
    public function createUser()
    {

        $user = $this->userRepo->create(\Input::all());

        if(false === $user) {

            if($this->userRepo->getErrors()->count() > 0) {

                return $this->respondWithFormErrors($this->userRepo->getErrors()->toArray());

            } else {

                return $this->respondWithInternalError();

            }

        } else {

            \Auth::login($user);
            return $this->respondWithSuccess(['user' => $this->serialiseUser($user)]);

        }

        // Should never get here
        throw new \Exception('Opps');

    }


    /**
     * Used internally within the controller to format a user for output as user controller does a few extra bits too normal
     *
     * @param User $user
     * @return array The user serialised for output
     */
    private function serialiseUser(User $user)
    {

        if(false !== \Auth::check() && \Auth::user()->getKey() === $user->getKey()) {

            $user->setHidden(array_diff($user->getHidden(), ['email']));

            return $user->load('authentications')->toArray();

        }

        // Else just return as normal
        return $user->toArray();

    }

}