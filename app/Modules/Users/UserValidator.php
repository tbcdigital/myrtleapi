<?php namespace CommonCore\Users;

use \CommonCore\Core\Validators\AbstractValidator;

/**
 * Validation service for the User entity
 *
 * @author Lee Sherwood <lee.sherwood@tbc-digital.com>
 * @package CommonCore
 * @version 1.0.0
 */

class UserValidator extends AbstractValidator {

    protected $rules = [
	   'common' => [
	       'email' => ['unique:<table>,email,<key>'],
	       'username' => ['unique:<table>,username,<key>'],
	       'password' => ['confirmed', 'min:6'],
	       'gender'   => ['in:male,female'],
	       'privacy'  => ['in:public,private']
        ],
       'create' => [
           'email'    => ['required'],
	       'username' => ['required'],
	       'password' => ['required']
        ],
        'createFromProvider' => [
	       'username' => ['required']
        ]
    ];

}