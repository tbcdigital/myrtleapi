<?php namespace CommonCore\Users;

use CommonCore\Core\Repositories\EloquentRepository;
use Tbcdigital\Tbcauth\Repositories\TbcauthUserRepositoryInterface;
use \Auth;

/**
 * Our user repository implementation
 *
 * @author Lee Sherwood <lee.sherwood@tbc-digital.com>
 * @package CommonCore
 * @version 1.0.0
 */

class UserRepository extends EloquentRepository implements UserRepositoryInterface, TbcauthUserRepositoryInterface
{

    /**
     * Construct an instance of the user repository
     *
     * @param User $model
     * @param UserValidator $validator
     */
    public function __construct(User $model, UserValidator $validator)
    {
    	parent::__construct($model, $validator);
    }


    /**
     * {@inheritdoc}
     */
    public function createFromProvider($attributes)
    {

        // This method may never fail due to username clashes
        $newmodel = $this->getNew();
        $attempt = $this->perform('createFromProvider', $newmodel, $attributes, true);

        if($attempt !== false || $this->getErrors()->has('username') === false) {
            return $attempt;
        }

        // If username clashes, first thing we try is just iterating
        $originalUsername = $attributes['username'];

        // Use a LIKE lookup (regex would be better, but LIKE is more standard so easier to work with other ORMS)
        $use_count = $this->buildQueryWithAttributes($this->newQuery(), ['username' => $originalUsername.'.%'], 'LIKE')->count();
        $use_count++;

        $protector = 0;
        do {

            $attributes['username'] = $originalUsername.'.'.($use_count+$protector);

            $attempt = $this->perform('createFromProvider', $newmodel, $attributes, true);

            $protector++;

        } while ($attempt === false && $protector < 20);


        if($attempt !== false) {
            return $attempt;
        }


        // If the nicer iterator fails, just try random!
        $protector = 0;
        do {

            $rand = mt_rand(0, mt_getrandmax());

            $attributes['username'] = $originalUsername.'.'.$rand;

            $attempt = $this->perform('createFromProvider', $newmodel, $attributes, true);

            $protector++;

        } while ($attempt === false && $protector < 20);


        return $attempt;

    }


    /**
     * Perform the create action for provider registration
     *
     * @param  \CommonCore\Core\Models\Model  $model
     * @param  array   $attributes
     *
     * @return \CommonCore\Core\Models\Model|false
     */
    protected function performCreateFromProvider($model, array $attributes)
    {

        $model->fill($attributes);

        return $this->perform('save', $model, $attributes, false);
    }


    /**
     * {@inheritdoc}
     */
    public function findByEmail($email)
    {
        $query = $this->buildQueryWithAttributes($this->newQuery(), ['email' => $email], 'LIKE');

        return $this->fetchSingle($query);
    }


    /**
     * {@inheritdoc}
     */
    public function findByUsername($username)
    {
        $query = $this->buildQueryWithAttributes($this->newQuery(), ['username' => $username], 'LIKE');

        return $this->fetchSingle($query);
    }


    /**
     * {@inheritdoc}
     * @TODO: This uses the Auth facade which it shouldn't for proper abstraction
     */
    public function discover($key)
    {

        if(strtolower($key) == 'me') {
            return Auth::user();
        }

        if(null !== ($user = $this->findByKey($key))) {
            return $user;
        }

        if(null !== ($user = $this->findByUsername($key))) {
            return $user;
        }

        return null;

    }

}