<?php

/**
 * Routes file for authentication
 *
 * @author Lee Sherwood <lee.sherwood@tbc-digital.com>
 * @package CommonCore
 * @version 1.0.0
 */

Route::group(['prefix' => 'v1'], function(){

    // The controller prefix
    $namespace = 'CommonCore\\Users\\';

    // Authenticated users only routes
    Route::group(['before' => 'auth'], function() use ($namespace) {

        // Update a user profile (use "me" as id for updating auth'd user)
        Route::post('users/{id}', $namespace.'UserController@updateUserProfile');

    });

    // Get a user profile (use "me" as id to fetch auth'd user profile)
    Route::get('users/{id}', $namespace.'UserController@getUserProfile');

    // Create a new user account (registration)
    Route::post('users', $namespace.'UserController@createUser');

});