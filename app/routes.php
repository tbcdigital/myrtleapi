<?php

/**
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
Route::group(['prefix' => 'v1'], function(){
	
	/** Reset the Password  */
	Route::post('password/reset', 'RemindersController@resetPassword');
	
	/** Get all users*/
	Route::get('users' , 'UsersFeedController@getAllUsers' );
	Route::get('usersearch' , 'UsersFeedController@getAllUsersByUsername' );
	Route::get('usersearchall' , 'UsersFeedController@getAllUsersByRealname' );
	
	Route::post('invite', 'InvitesController@generalInvite' );
	Route::post('events/{eventID}/invite', 'InvitesController@generalInvite' );

	Route::post('events', 'StorysController@newStory' );
	Route::post('events/{storyID}', 'StorysController@updateStory' );
	//Route::get('events', 'StorysController@getStory' ); superceded by StorysFeedController@getAllEvents
	Route::get('events/{storyID}', 'StorysController@getStory' );
	Route::get('events/user/{creatorID}', 'StorysController@getStoryByCreator' );
	Route::delete ('events/{storyID}', 'StorysController@deleteStory' );
	
	Route::get('events/{storyID}/accesscount', 'StorysController@getStoryAccessCount' );	
	Route::get('events/{storyID}/accessdata', 'StorysController@getStoryAccessData' );
	
	
	
	Route::post ('events/{storyID}/comments', 'CommentsController@newComment' );
	Route::post ('events/comments/reply/{reply_to}', 'CommentsController@newReplyComment' );
	
	Route::post ('events/comments/{commentID}', 'CommentsController@updateComment' );
	Route::get  ('events/comments/{commentID}', 'CommentsController@getComment' );
	Route::get  ('events/{storyID}/comments', 'CommentsController@getComments' );
	Route::get  ('events/{storyID}/comments/{userID}', 'CommentsController@getCommentsUIDandSID' );
	Route::get  ('storypicture/{storyPictureID}/comments', 'CommentsController@getCommentsByStoryPicture' );
	Route::delete  ('events/comments/{commentID}', 'CommentsController@deleteComment' );
	
	Route::post ('/batch/{storyID}/new' , 'StoryPicturesController@makeNewBatch');
	Route::post ('/batch/{batchID}/finish' , 'StoryPicturesController@finishBatch');
	
	/** Access Permissions*/
	/** Permissions Block 1.  Supply {story ID} and an array of userID[] or one userID*/
	Route::post('events/{storyID}/storyaccess', 'StorysUsersPermissionsController@setByStory' );
	Route::delete('events/{storyID}/storyaccess', 'StorysUsersPermissionsController@delByStory' );
	Route::get('events/{storyID}/storyaccess', 'StorysUsersPermissionsController@getByStory' );

	/** Permissions Block 2.  Supply {user ID} and an array of storyID[] or one storyID*/
	Route::post('events/{userID}/useraccess', 'StorysUsersPermissionsController@setByUser' );
	Route::delete('events/{userID}/useraccess', 'StorysUsersPermissionsController@delByUser' );
	Route::get('events/{userID}/useraccess', 'StorysUsersPermissionsController@getByUser' );
	//Route::get('users/{userID}/events/someroute', 'StorysUsersPermissionsController@getByUser' ); // MAYBE redundant.
	
	/** Story Feeds*/
	//Route::get('users/{userID}/events', 'StorysFeedController@getEventsByUser' ); // include access + created UNLESS onlycreated=1
	//Route::get('events', 'StorysFeedController@getEvents' ); //include public + access + created UNLESS onlypublic=1
	Route::get('users/{userID}/events', 'StorysFeedController@getAllEvents' ); // include access + created UNLESS onlycreated=1
	Route::get('events', 'StorysFeedController@getAllEvents' ); //include public + access + created UNLESS onlypublic=1
	

	/** Likes Block 1.  Supply {story ID} and an array of userID[] or one userID*/
	Route::post('events/{storyID}/storylike', 'StorysUsersLikeController@setByStory' );
	Route::delete('events/{storyID}/storylike', 'StorysUsersLikeController@delByStory' );
	Route::get('events/{storyID}/storylike', 'StorysUsersLikeController@getByStory' );
	/** Duplicate with Story as the URL root.*/
	Route::post('story/{storyID}/storylike', 'StorysUsersLikeController@setByStory' );
	Route::delete('story/{storyID}/storylike', 'StorysUsersLikeController@delByStory' );
	Route::get('story/{storyID}/storylike', 'StorysUsersLikeController@getByStory' );
	
	
	
	/** Likes Block 2.  Supply {user ID} and an array of storyID[] or one storyID*/
	Route::post('events/{userID}/userlike', 'StorysUsersLikeController@setByUser' );
	Route::delete('events/{userID}/userlike', 'StorysUsersLikeController@delByUser' );
	Route::get('events/{userID}/userlike', 'StorysUsersLikeController@getByUser' );
	/** Duplicate with Story as the URL root.*/
	Route::post('story/{userID}/userlike', 'StorysUsersLikeController@setByUser' );
	Route::delete('story/{userID}/userlike', 'StorysUsersLikeController@delByUser' );
	Route::get('story/{userID}/userlike', 'StorysUsersLikeController@getByUser' );
	
	
	/** Likes Block 3: Storypictures, 10/29.  Supply {storypictureID} and an array of userID[] or one userID */
	Route::post('storypictures/{storypictureID}/storypicturelike', 'StoryPicturesUsersLikeController@setByStorypicture' );
	Route::delete('storypictures/{storypictureID}/storypicturelike', 'StoryPicturesUsersLikeController@delByStorypicture' );
	Route::get('storypictures/{storypictureID}/storypicturelike', 'StoryPicturesUsersLikeController@getByStorypicture' );	
	
	/** Likes Block 4: Storypictures, 10/29.  Supply {userID} and an array of storypictureID[] or one storypictureID */
	Route::post('storypictures/{userID}/userlike', 'StoryPicturesUsersLikeController@setByUser' );
	Route::delete('storypictures/{userID}/userlike', 'StoryPicturesUsersLikeController@delByUser' );
	Route::get('storypictures/{userID}/userlike', 'StoryPicturesUsersLikeController@getByUser' );	
	
	
	
	
	/** StoryPictures a.k.a Pictures For Events*/
	Route::post('events/{storyID}/picture', 'StoryPicturesController@setEventPictureInformation' );	
	Route::delete('events/picture/{pictureID}', 'StoryPicturesController@delEventPictureInformation' );	
	Route::get('events/{storyID}/picture', 'StoryPicturesController@getEventPictureInformation' );	//}Duplicated to
	Route::get('events/{storyID}/pictures', 'StoryPicturesController@getEventPictureInformation' );	//}account for mistake: picture/pictures
	Route::get('events/picture/{pictureID}', 'StoryPicturesController@getPictureInformation' );
	
	/**Feed for liked StoryPictures*/
	Route::get('storypictures/{storyPictureID}/usersliked', 'StoryPicturesController@likedByStoryPicture' );
	
	/** Follow Operations*/
	Route::post('users/{userID}/follow' , 'FollowsFeedController@follow' );
	Route::delete('users/{userID}/follow' , 'FollowsFeedController@unfollow' );
	Route::get('users/{userID}/following' , 'FollowsFeedController@whoDoIFollow' );
	Route::get('users/{userID}/followers' , 'FollowsFeedController@whoFollowsMe' );

	Route::get('users/{userID}/followingevents' , 'FollowsFeedController@whatEventsDoIfollow' );

	Route::get('users/{userID}/activities' , 'FollowsFeedController@whatEventsDoIfollow' );
	
	
	/** User Profile*/
	Route::get('users/{userID}/basicprofile' , 'UsersProfileExtensionController@userProfile' );
	
	/** Merge operations*/
	Route::post('events/{storyID}/merge' , 'MergeStorysController@mergeStories' );
	Route::post('events/{storyID}/grantmergepermission' , 'MergeStorysController@awardPermissionToMerge' );
	Route::delete('events/{storyID}/grantmergepermission' , 'MergeStorysController@denyPermissionToMerge' );

	/** Activity Feed*/
	Route::get('users/{userID}/activity' , 'ActivityFeedController@activityFeed' );
	
	/** Permissions Feed*/
	Route::get('event/{storyID}/userperms' , 'PermissionsFeedController@getUsersWithPermission' );
	
	/** Remove all permissions for this story */
	Route::delete( 'event/{eventID}/removeallperms' , 'PermissionsFeedController@removePermissionsByStory' );
	
	/** Real-and-Actual Activity Feed*/
	Route::get('users/{userID}/activityactual' , 'ActualActivityFeedController@activityFeed' );
	Route::get('homefeed' , 'ActualActivityFeedController@activityFeed' );
	Route::get('home' , 'ActualActivityFeedController@activityFeed' );
	
	/** Notifications Feed*/
	Route::get('users/{userID}/notifications' , 'NotificationsController@loadFeed' );
	//Route::get('notification/{notificationID}/getandmark' , 'NotificationsController@getAndMarkOneNotification' );
	Route::get('users/{userID}/notifications/{notificationID}' , 'NotificationsController@getNotificationAndMarkAsRead'); //get notification AND MARK AS READ
	Route::delete('users/{userID}/notifications' , 'NotificationsController@deleteNotificationsMarkedAsRead');
	
	/** Permissions Request */
	Route::get("story/{storyID}/permrequest" , 'RequestPermissionController@requestPermission');
	
	/** Cover Picture */
	Route::get( "storypicture/{storypictureID}/setascover" , "CoverPictureController@setCoverPicture" );
	
	/** Users who liked this Event */
	Route::get( "events/{storyID}/usersliked" , "StorysFeedController@usersWhoLikedThisStory" );
	
	//Route::post ('events/like/{eventID}', 'StorysController@like' );
	//Route::delete ('events/like/{eventID}', 'StorysController@unlike' );
	
	//Routes::post ('events/{storyID}/picture', 'CommentsController@newPicture' ); // 1 or Many Without or With Array
	//Routes::get ('events/{storyID}/picture', 'CommentsController@showPicture' );
	//Routes::delete ('events/{storyID}/picture', 'CommentsController@deletePicture' );
	
	//Route::get ('users/{userID}/own', 'events I own' );
	//Route::get ('users/{userID}/permission', 'events I have perms' );
	//Route::get ('users/{userID}/both', 'both' );
	
	//Route::get ('users/{userID}/following', '' );
	//Route::get ('users/{userID}/followers', '' );

	//Route::get ('users/{userID}/following', '' );
	//Route::get ('users/{userID}/followers', '' );
	
	Route::get('upload/token', 'STSController@vendNewToken');
	
	/** Handle Push Tokens  */
	Route::post('users/{id}/pushtoken', 'PushTokenController@savePushToken');
	Route::delete('users/{id}/pushtoken', 'PushTokenController@removePushToken');	
	
	
});
	
Route::get('/', function()
{
	return View::make('hello');
});
