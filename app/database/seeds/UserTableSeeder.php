<?php
use Illuminate\Database\Seeder;
use CommonCore\Users\UserRepositoryInterface;
use CommonCore\Users\User;

/**
 * Seeds the users table with some test data
 */

class UserTableSeeder extends Seeder
{

    /**
     * The number of records to create each time it's run
     *
     * @var int
     */
    protected $numCreate = 3;


    /**
     * Whether to start with a fresh database or not
     *
     * @var bool
     */
    protected $clearDb = true;


    /**
     * The user repository
     *
     * @var CommonCore\Users\UserRepositoryInterface
     */
    protected $userRepo;


    /**
     * The table/collection name
     *
     * @var string
     */
    protected $table;

    /**
     * The constructor
     */
    public function __construct(UserRepositoryInterface $userRepo)
    {

        $this->userRepo = $userRepo;
        $this->table = $userRepo->getModel()->getTable();

    }


    /**
     * The entry method
     *
     * @see \Illuminate\Database\Seeder::run()
     */
    public function run()
    {

        if($this->clearDb) {
            DB::table($this->table)->delete();
        }

        $userdata = $this->getUserProfiles($this->numCreate);

        for($i=1; $i<= $this->numCreate; $i++) {

            if(!isset($userdata[($i-1)])) {
                echo "Failed to create user number ".$i." as no data";
                continue;
            }

            $udata = $userdata[($i-1)];

            $user = $this->userRepo->create([
    	       'username' => $udata['username'],
    	       'email' => $udata['email'],
               'password' => 'password',
               'password_confirmation' => 'password',
    	       'first_name' => $udata['name']['first'],
    	       'last_name' => $udata['name']['last'],
    	       'gender' => $udata['gender'],
    	       'dob' => $udata['dob'],
    	       'profile_image' => "http://www.gama-ksa.com/wp-content/uploads/2014/06/funny-default-facebook-pictures.jpg", //$udata['picture'],
    	       'cover_image' => "http://www.ketchum.com/sites/default/files/styles/media_640w/public/event_marketing.jpg"   //$udata['picture']
            ]);

            $user->created_at = Carbon\Carbon::now()->subDays(mt_rand(0,(4*365)))->subHours(mt_rand(0,23))->subMinutes(mt_rand(0,59))->subSeconds(mt_rand(0,59));
            $user->updated_at = Carbon\Carbon::now()->subDays(mt_rand(0,(1*365)))->subHours(mt_rand(0,23))->subMinutes(mt_rand(0,59))->subSeconds(mt_rand(0,59));
            $user->save();

            if($i%5 == 0) {
                echo "Seeded ".$i." users so far\n";
            }

        }



    }


    /**
     * Get the profiles from the generator service
     *
     * @param int $num Number of profiles to fetch
     * @return array profiles array
     */
    protected function getUserProfiles($num)
    {

        $users = [];

        $client = new GuzzleHttp\Client;
        $requests = [];

        $num_of_reqs = ceil($num/20);

        for($i = 1; $i <= $num_of_reqs; $i++) {

            $requests[] = $client->createRequest('GET', 'http://api.randomuser.me?results=20');

        }

        $results = GuzzleHttp\batch($client, $requests);

        foreach($results as $request) {

            $result = $results[$request];

            if( $result instanceof GuzzleHttp\Message\ResponseInterface ) {

                /* @var $result ResponseInterface */
                $data = $result->json();

                foreach($data['results'] as $arr) {
                    $users[] = $arr['user'];
                }

            } else {

                throw new Exception($result->getMessage());

            }

        }

        return $users;

    }


}