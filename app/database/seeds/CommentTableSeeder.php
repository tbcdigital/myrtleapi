<?php

use CommonCore\Users\User;

class CommentTableSeeder extends Seeder {

    /**
     * How many comments do we want to create?
     * @var int
     */
    protected $create_count = 27;

    protected $storage_driver = null;




    /**
     * Access method which is called by Artisan
     */
    public function run(){
        echo("Running Comment Seeder\n");
        
        $this->delall();
        
        $allEventID=array();
        $i=0;
        Story::all()->each(function($inVar) use (&$allEventID, &$i){array_push($allEventID, $inVar->_id);$i++;});
        $i--;
        $faker = Faker\Factory::create('en_GB');

        $created = 0;
        
        $allUsers=User::all()->toArray();
        $allStorys=Story::all()->toArray();
        $allStoryPictures=StoryPicture::all()->toArray();
        
        $count = sizeof($allUsers);
        $count--;
        $scount = sizeof($allStorys);
        $scount--;
        $pcount = sizeof($allStoryPictures);
        $pcount--;
        

        
        while ($created <= $this->create_count) {

        	$pp = rand(0, 100);
        	if($pp>40){
        		$pub_prive='story';
        	}else{
        		$pub_prive='picture';
        	}
        	

        	$user = $allUsers[rand(0, $count)];
        	$userID = $user['_id'];
        	$story = $allStorys[rand(0, $scount)];
        	$storyID = $story['_id'];
        	$storypicture = $allStoryPictures[rand(0, $pcount)];
        	$storypictureID = $storypicture['_id'];
        	 
            $storyArrRef=rand(0,$i);
                        


            
            $commentContents = array(
	               'comment' => $faker->optional()->paragraph(1) . $faker->optional()->paragraph(2) . $faker->optional()->paragraph(3) . $faker->optional()->paragraph(4)
               );
            $tmp_comment = new Comment($commentContents);
            $tmp_comment->story_id=$allEventID[$storyArrRef];
            $tmp_comment->user_id = $userID;
            $tmp_comment->story_id = $storyID;

            $pp = rand(0, 100);
            if($pp>40){
            	$tmp_comment->type='story';
            }else{
            	$tmp_comment->type='picture';
            	$tmp_comment->storypicture_id = $storypictureID;
            }  
            $tmp_comment->marker_madeBySeeder=1;

            if($tmp_comment->save() !== false)  {
                $created++;
            }

        } 
        echo("Comment Seeder Completed: ". $created ." records.\n");
        return($created);


    }
    
    
    public function del(){
        $affectedRows = Comment::where('marker_madeBySeeder', '=', 1)->delete();
        var_dump($affectedRows);
    }

    public function delall(){
			$tablename = with(new Comment)->getTable();
			DB::table($tablename)->delete();
    }

  

}