<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		ini_set('display_errors',1);
		error_reporting(E_ALL);
	    echo("Running Database Seeder\n");
		Eloquent::unguard();

		//$this->call('UserTableSeeder');
		$this->call('StoryTableSeeder');
		$this->call('StoryPictureTableSeeder');
		$this->call('CommentTableSeeder');
		
		echo("Finished Running Database Seeder\n");

	}

}
