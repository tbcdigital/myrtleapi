<?php

use CommonCore\Users\User;

class StoryTableSeeder extends Seeder {

    /**
     * How many Storys do we want to create?
     * @var int
     */
    protected $create_count = 9;

    protected $storage_driver = null;




    /**
     * Access method which is called by Artisan
     */
    public function run(){
        echo("Running Story Seeder\n");
        
        $this->delall();
        
        $faker = Faker\Factory::create('en_GB');

        $created = 0;

        $allUsers=User::all()->toArray();
        $count = sizeof($allUsers);
        $count--;
        
        while ($created <= $this->create_count) {

        	$pp = rand(0, 100);
        	if($pp>40){
        		$pub_prive='public';
        	}else{
        		$pub_prive='private';
        	}        	
        	
          	$user = $allUsers[rand(0, $count)];
        	$userID = $user['_id'];

 
            $storyContents = array(
	               'title' => "Event00". (String)($created+1) . " - Inserted",
	               'description' =>  "Description: " . $faker->optional()->paragraph(1) . $faker->optional()->paragraph(2) . $faker->optional()->paragraph(3) . $faker->optional()->paragraph(4),
	               'location' => array(
	                   'latitude' => "52",
	                   'longitude' => "-2",
	               		'name'=>"my house"
	               ),
	               'privacy' => $pub_prive
               );
            $tmp_story = new Story($storyContents);
            $tmp_story->user_id = $userID; 
            //$tmp_story->profile_pic = "";
            //$tmp_story->cover_image = "";
            
            
            $tmp_story->marker_madeBySeeder = 1;
            

            if($tmp_story->save() !== false)  {
                $created++;
            }

        } 
        echo("Story Seeder Completed: ". $created ." records.\n");
        return($created);


    }
    
    
    public function del(){
        $affectedRows = Story::where('marker_madeBySeeder', '=', 1)->delete();
        var_dump($affectedRows);
    }

    public function delall(){
			$tablename = with(new Story)->getTable();
			DB::table($tablename)->delete();
    }



}