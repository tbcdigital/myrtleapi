<?php

use CommonCore\Users\User;

class StoryPictureTableSeeder extends Seeder {

    /**
     * How many StoryPictures do we want to create?
     * @var int
     */
    protected $create_count = 18;

    protected $storage_driver = null;




    /**
     * Access method which is called by Artisan
     */
    public function run(){
        echo("Running StoryPicture Seeder\n");
        
        $this->delall();
        
        $allEventID=array();
        $i=0;
        	
        $i--;
        $faker = Faker\Factory::create('en_GB');

        $created = 0;
        
        $allUsers=User::all()->toArray();
        $allStorys=Story::all()->toArray();
        
        $count = sizeof($allUsers);
        $count--;
        $scount = sizeof($allStorys);
        $scount--;
        
        $pub_prive = '';
        
        while ($created <= $this->create_count) {

           	$user = $allUsers[rand(0, $count)];
        	$userID = $user['_id'];
        	$story = $allStorys[rand(0, $scount)];
        	$storyID = $story['_id'];



            
            $commentContents = array(
	               'comment' => $faker->optional()->paragraph(1) . $faker->optional()->paragraph(2) . $faker->optional()->paragraph(3) . $faker->optional()->paragraph(4)
               );
            $tmp_comment = new StoryPicture();
            $tmp_comment->story_id=$storyID;
            $tmp_comment->user_id = $userID;
            $tmp_comment->story_id = $storyID;
            //$tmp_comment->url = "http://imagestore/" . $userID . "/" . $storyID . "/imagenumber" . rand(1000, 9999) . ".jpg"  ;
            
            $tmp_comment->url = "https://myrtletest.s3.amazonaws.com/Landscape-wallpapers-2.jpg";
            
            $pp = rand(0, 100);
            if($pp>40){
            	$tmp_comment->type='story';
            }else{
            	$tmp_comment->type='picture';
            }          

            $tmp_comment->location = array('latitude' => "52", 'longitude' => "-2", 'name'=>"my house");
            
            $tmp_comment->marker_madeBySeeder=1;

            if($tmp_comment->save() !== false)  {
                $created++;
            }
            //$created++;
        } 
        echo("Storypicture Seeder Completed: ". $created ." records.\n");
        return($created);


    }
    
    
    public function del(){
        $affectedRows = StoryPicture::where('marker_madeBySeeder', '=', 1)->delete();
        var_dump($affectedRows);
    }

    public function delall(){
			$tablename = with(new StoryPicture)->getTable();
			DB::table($tablename)->delete();
    }



}