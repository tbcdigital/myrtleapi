<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAuthenticationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{

	    Schema::create('authentications', function($table){

	        $table->bigIncrements('id')->unsigned();
	        $table->bigInteger('user_id')->unsigned();
	        $table->string('provider', 20);
	        $table->string('provider_id', 50);
	        $table->text('credentials');
	        $table->timestamps();

	        $table->unique(['provider', 'provider_id']);

	    });

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::dropIfExists('authentications');
	}

}
