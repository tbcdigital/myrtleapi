<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{

	    Schema::create('users', function($table){

	        $table->bigIncrements('id')->unsigned();
	        $table->string('username')->unique();
	        $table->string('email')->unique()->nullable();
	        $table->string('password')->nullable();
	        $table->string('first_name')->nullable();
	        $table->string('last_name')->nullable();
            $table->string('profile_image')->nullable();
            $table->string('cover_image')->nullable();
            $table->date('dob')->nullable();
            $table->enum('gender',['male', 'female', 'unknown'])->default('unknown');
	        $table->timestamps();

	    });

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::dropIfExists('users');
	}

}
