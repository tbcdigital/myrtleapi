<?php
/**
 * Handle Setting / Unsetting of something per story/user.
 * This is a base class which should not be directly invoked.
 * It functions as a base class for any operation which requires the CRUD of Redis
 * records to match up story / user AND user / story data.
 * 
 * A derived class of this type would typically override 
 * $Storykey
 * $typekey
 * $userkey
 * 
 * If asynchronous operations are required, the subclass should override void registerOperations(void)
 * to fire a Custom event.
*/
use CommonCore\Users\User;
use Carbon\Carbon;

class BaseStorysUsersController extends \BaseController{

	/**
	 * User repository
	 *
	 * @var CommonCore\Users\UserRepositoryInterface
	 */
	protected $userRepo;
	
	/**
	 * 	Suffix for the Redis Key User type
	 */
	protected $userkey = "";
	
	/**
	 * 	Suffix for the Redis Key Story type
	 */
	protected $storykey = "";
	
	/**
	 * 	Suffix for the Redis type Key (likes or perm, or something else for your custom subclass) 
	 */
	protected $typekey = "";
	
	/**
	 * The ID of the authenticated user.
	 *
	 * @var string
	 */
	protected $userID = '';

	/**
	 * An array to hold all the User ID's (where one story / multiple users are set).
	 * 
	 * @var string[]
	 */
	protected $allUserID=array();	
	
	/**
	 * An array to hold all the Story ID's (where one user / multiple storys are set).
	 * 
	 * @var string[]
	 */
	protected $allStoryID=array();	
	
	/**
	 * Passed in or discovered user
	 * 
	 * @var User obj
	 */
	protected $user=null;	
	
	/**
	 * Passed-in Story
	 * 
	 * @var Story obj
	 */
	protected $story=null;	
	
	/**
	 * Validate that some user is logged in.
	 * Store that user ID to a class-level var.
	 * Get the array of User ID's
	 * Store
	 *
	 *  @param:  null
	 *  @return: string completed_status_message
	 */	


	public function __construct(CommonCore\Users\UserRepositoryInterface $userRepo)
	{
		$this->userRepo = $userRepo;
	}	
	

	function fill_allStoryID(){
		
		$storyID_data = Input::get("storyID");
		if (is_array($storyID_data)){
			foreach($storyID_data as $story){
				$st = Story::find($story)->user_id;
				array_push($this->allStoryID, $story);
			}
		}else{
			array_push($this->allStoryID, $storyID_data);
		}
	}
	
	function fill_allUserID(){
			$userID_data = Input::get("userID");
			
			if (is_array($userID_data)){
				foreach($userID_data as $user){
					array_push($this->allUserID, $user);
				}
			}else{
				if(null == $userID_data){
				/**
				 * Handle the special case of NO USER ID being supplied as an argument.
				 * This will hapen when and only when:
				 * i. The URL is shaped like events/{eventID}/storylike
				 * ii. NO USER ID is supplied OR the USER ID is defined as ME
				 *  */
					$userID_data = "me";
					
				 /**
				  * In both cases, the user ID will be set to 'me', which can be understood by
				  * $this->userRepo->discover
				  */
				}
					
				$this->user = $this->userRepo->discover($userID_data);
				if(null==$this->user){
					return Response::json(array('message' => '(setByUser) Bad User'),    403);
				}
				
				$userID_data = $this->user->getKey();				
				
				array_push($this->allUserID, $userID_data);
				
				
			}
	}
	
	function validateAndLoad_User($userID){
		$this->fill_allStoryID();		
	}
	
	function setByStory($storyID){

		if(null==Auth::User()){
			return Response::json(array('message' => 'Not Logged In'),    403);
		}
		
		$this->story = Story::find($storyID);
		
		if($this->story==null){
			return Response::json(array('message' => 'Not Valid Story ID'),    403);
		}
		
		if($this->storykey != "storylike"){
			if( Story::find($storyID)->user_id !=  Auth::User()->getKey() ){
				return Response::json(array('message' => 'You are not the Story Creator'),    403);
			}
		}
		
		$this->fill_allUserID();	
		
		
		if(  !empty($this->allUserID)  ){
			if( $this->allUserID[0] === null ){
				array_push(  $this->allUserID, Auth::User()->getKey()  );
			}
		}
		
		//dd($this->allUserID);		
		
		$failbag = array();
		$successbag = array();
		
		foreach($this->allUserID as $userID){
			
			//if we are LIKEing, and now awarding permissions, we have to chaeck that the LIKE
			// has not already been set:
			$redis_key = "user:" .  $userID . ":" . $this->storykey;
			
			$data = RedisL4::connection()->ZSCORE($redis_key,$storyID);
			
			if(null != $data &&  $this->storykey == "storylike" ){
				$failbag[] = $userID . " Is already set on " . $storyID;
				break;
			}
			
			
				
				
			$redis_key = "user:" .  $userID . ":" . $this->storykey;
			//var_dump($redis_key);
			$data = RedisL4::connection()->ZADD($redis_key,Carbon::now()->getTimestamp(), $storyID);
			
			$redis_key = "story:" .  $storyID . ":" . $this->userkey;
			$data = RedisL4::connection()->ZADD($redis_key,Carbon::now()->getTimestamp(), $userID);

			$redis_key = "story:" . $this->typekey;
			$data = RedisL4::connection()->ZINCRBY($redis_key, 1, $storyID);  
			
			$dbo = Story::find($storyID);
			$dbo->increment("likes"); 
			$successbag[] = ($userID . " Added on " . $storyID);
			
			$this->registerOperations($userID);
		}
		
		return Response::json(['message' => 'Complete', 'success'=>$successbag, 'fails'=>$failbag], 200);
	}	
	
	function delByStory($storyID){
		//dd("DEL-BY-STORY");
		if(Story::find($storyID)==null){
			return Response::json(array('message' => 'Not Valid Story ID'),    403);
		}
		
		if(null==Auth::User()){
			return Response::json(array('message' => 'Not Logged In'),    403);
		}
		/*
		if( Story::find($storyID)->user_id !=  Auth::User()->getKey() ){
			return Response::json(array('message' => 'You are not the Story Creator'),    403);
		}
		*/
		$this->fill_allUserID();	

		$failbag = array();
		$successbag = array();		
		
		foreach($this->allUserID as $userID){

			$redis_key = "user:" .  $userID . ":" . $this->storykey;
			$data = RedisL4::connection()->ZSCORE($redis_key,$storyID);
			if(null == $data &&  $this->storykey == "storylike" ){
				//echo("Breakin\n");
				$failbag[] = $userID . " never set a " . $this->storykey . " on " . $storyID;
				break;
			}

			$redis_key = "user:" .  $userID . ":" . $this->storykey;
			$data = RedisL4::connection()->ZREM($redis_key, $storyID);
			
			$redis_key = "story:" .  $storyID . ":" . $this->userkey;
			$data = RedisL4::connection()->ZREM($redis_key, $userID);
			
			$redis_key = "story:" . $this->typekey;
			$data = RedisL4::connection()->ZINCRBY($redis_key, -1, $storyID); 

			$dbo = Story::find($storyID);
			$dbo->decrement("likes");
			$successbag[] = ($userID . " removed from " . $storyID);
		}
		
		return Response::json(['message' => 'Complete', 'success'=>$successbag, 'fails'=>$failbag], 200);
	}	
	
	function getByStory($storyID){
		
		if(Story::find($storyID)==null){
			return Response::json(array('message' => 'Not Valid Story ID'),    403);
		}
		
		$this->fill_allUserID();
		
		$returnresult=array();
		$allreturnresult = array();
		
		$allreturnresult['event_id'] = $storyID;
		
		foreach($this->allUserID as $userID){
			$redis_key = "story:" .  $storyID . ":" . $this->userkey;
			$data = RedisL4::connection()->ZSCORE($redis_key, $userID);
			if($data != 0){
				$returnresult[] = array($userID);
			}
		}	
		$allreturnresult['data']=$returnresult;
		return Response::json($allreturnresult, 200);
	}	

	
	function setByUser($userID='me'){

		if(null==Auth::User()){
			return Response::json(array('message' => 'Not Logged In'),    403);
		}

		$this->user = $this->userRepo->discover($userID);
		if(null==$this->user){
			return Response::json(array('message' => '(setByUser) Bad User'),    403);
		}		
		
		$userID = $this->user->_id;

		$this->fill_allStoryID();		
	
		$failbag = array();
		$successbag = array();
		
		foreach($this->allStoryID as $storyID){
			
			//if we are LIKEing, and now awarding permissions, we have to chaeck that the LIKE
			// has not already been set:
			$redis_key = "user:" .  $userID . ":" . $this->storykey;
			$data = RedisL4::connection()->ZSCORE($redis_key,$storyID);
			if(null != $data &&  $this->storykey == "storylike" ){
				$failbag[] = (Auth::User()->getKey() . " was already set on " . $storyID);
				break;
			}
						
			
			$this->story = Story::find($storyID);
			if($this->story ->user_id ==  Auth::User()->getKey()  || $this->storykey == "storylike"){
				$redis_key = "user:" .  $userID . ":" . $this->storykey;
				$data = RedisL4::connection()->ZADD($redis_key, Carbon::now()->getTimestamp(), $storyID);
				
				$redis_key = "story:" .  $storyID . ":" . $this->userkey;
				$data = RedisL4::connection()->ZADD($redis_key, Carbon::now()->getTimestamp(), $userID);
				
				$redis_key = "story:" . $this->typekey;
				$data = RedisL4::connection()->ZINCRBY($redis_key, 1, $storyID);  

				$dbo = Story::find($storyID);
				$dbo->increment("likes");
				
				$successbag[] = (Auth::User()->getKey() . " Added on " . $storyID);
				$this->registerOperations($userID);
			}else{
				$failbag[] = (Auth::User()->getKey() . " Is Not story creator on " . $storyID);
			}
		}
		
		return Response::json(['message' => 'Complete', 'success'=>$successbag, 'fails'=>$failbag], 200);
		
	
	}
	
	function delByUser($userID='me'){
		//dd("DEL-BY-USER");
		if(null==Auth::User()){
			return Response::json(array('message' => 'Not Logged In'),    403);
		}
		
		$this->user = $this->userRepo->discover($userID);
		if(null==$this->user){
			return Response::json(array('message' => '(setByUser) Bad User'),    403);
		}		
		
		$userID = $this->user->_id;				
		
		$this->fill_allStoryID();		
		
		$failbag = array();
		$successbag = array();
		
		foreach($this->allStoryID as $storyID){
			
			$redis_key = "user:" .  $userID . ":" . $this->storykey;
			$data = RedisL4::connection()->ZSCORE($redis_key,$storyID);
			if(null == $data &&  $this->storykey == "storylike" ){
				$failbag[] = $userID . " never set a " . $this->storykey . " on " . $storyID;
				break;
			}			
			
				//echo("storyID: " .  $storyID . "\n");
				$redis_key = "user:" .  $userID . ":" . $this->storykey;
				$data = RedisL4::connection()->ZREM($redis_key, $storyID);
					
				$redis_key = "story:" .  $storyID . ":" . $this->userkey;
				$data = RedisL4::connection()->ZREM($redis_key, $userID);
				
				$redis_key = "story:" . $this->typekey;
				$data = RedisL4::connection()->ZINCRBY($redis_key, -1, $storyID);  

				$dbo = Story::find($storyID);
				if(null != $dbo){
					$dbo->decrement("likes");				
				}
				$successbag[] = (Auth::User()->getKey() . " Deleted from " . $storyID);
		}
		
		//dd("finished loop");
		return Response::json(['message' => 'Complete', 'success'=>$successbag, 'fails'=>$failbag], 200);
	}
	
	function getByUser($userID){
		
		if(null==Input::get("storyID")){
			
			return $this->getAllStorysByUserID($userID);
		}else{
			
			$this->fill_allStoryID();
			
			
			$returnresult=array();
			$allreturnresult = array();
			
			$allreturnresult['user_id'] = User::find($userID)->toArray();
			
			foreach($this->allStoryID as $storyID){
				$redis_key = "user:" .  $userID . ":" . $this->storykey;
				$data = RedisL4::connection()->ZSCORE($redis_key, $storyID);
				if($data>0){
					$returnresult[] = array(Story::find($storyID), $data);
				}else{
					$returnresult[] = array($storyID, $data);
				}
			}
			$allreturnresult['data']=$returnresult;
			return Response::json($allreturnresult, 200);		
		}
	}
	
	function getAllStorysByUserID($userID){
		//get a list of stories which this user is specifically allowed to see
		$s = Story::all();
		$returnresult=array();
		
		$allreturnresult['user_id'] = User::find($userID)->toArray();
		$redis_key = "user:" .  $userID . ":" . $this->storykey;
		$data = RedisL4::connection()->ZRANGEBYSCORE($redis_key, -INF , +INF);
		
		$returndata=array();
		
		foreach($data as $dataitem){
			$returndata[] = Story::find($dataitem)->toArray();
		}
		$allreturnresult['data']=$returndata;
		return Response::json($allreturnresult, 200);	
	}
	


	function registerOperations($userID){
		//override this function in all subclasses.
	}	
	
}