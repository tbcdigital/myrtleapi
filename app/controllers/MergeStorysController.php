<?php
use CommonCore\Users\User;

/**
 *
 */
class MergeStorysController extends \BaseController{


	/**
	 * User repository
	 *
	 * @var CommonCore\Users\UserRepositoryInterface
	 */
	protected $userRepo;
	
	/**
	 * 	The user logged in by common core
	 */
	protected $user = null;
	/**
	 * 	The 'master' story in a merging operation
	 */
	protected $storyID = null;
	/**
	 * 	an array of all STORYs to be merged into 1 (different) story
	 *  NOTE. This data is always stored as an array, even if only
	 *  one storyID is supplied
	 */
	protected $mergingStories = array();
	
	/**
	 * 	an array of all USERS to be processed in bulk.
	 *  This is always an array, even if only 1 user ID is supplied.
	 */
	protected $userID_data = array();
	
	/**
	 * 	Root Class Var Comment
	 */
	protected $varvar = null;
	
	
	public function __construct(CommonCore\Users\UserRepositoryInterface $userRepo)
	{
		$this->userRepo = $userRepo;
	}	
	
	
	function fill_allStoryID(){
	
		$storyID_data = Input::get("storyID");
	
		if (is_array($storyID_data)){
			foreach($storyID_data as $story){
				array_push($this->mergingStories, $story);
			}
		}else{
			array_push($this->mergingStories, $storyID_data);
		}
	
	}

	function fill_allUserID(){
	
		$userID_data = Input::get("userID");
	
		if (is_array($userID_data)){
			foreach($userID_data as $user){
				array_push($this->userID_data, $user);
			}
		}else{
			array_push($this->userID_data, $userID_data);
		}
	
	}

	public function mergeStories($storyID, $userID='me'){
		
		if(null==Auth::User()){
			return Response::json(array('message' => 'Not Logged In'),    403);
		}
		
		$this->user = $this->userRepo->discover($userID);
		if(null==$this->user){
			return Response::json(array('message' => 'Bad User'),    403);
		}
		
		//$storyID is the 'master' story which all the others will be merged into.
		//Only the creator of this story can do this.
		$ss = Story::find($storyID);
		if(null != $ss){
			if( $ss->user_id !=  $this->user->_id ){
				return Response::json(array('message' => 'You are not the Story Creator of ' . $storyID),    403);
			}	
		}else{
			return Response::json(array('message' => 'Bad Master-Story ID: ' . $storyID),    403);
		}
		$this->storyID = $storyID;
		$this->fill_allStoryID();
		$successbag=array();
		$failbag   =array();		

		//the stories whose ID's are in $this->mergingStories can be merged into the
		//Master story by their own creator, or by someone given explicit permissions by
		//the creator.
		foreach($this->mergingStories as $oneStory){
			$mergePermission = $this->checkMergePermission($oneStory, $this->user->_id);
			if($mergePermission==0){
				$failbag[] = ($this->user->_id . " cannot merge " . $oneStory . " into " . $storyID . ". Story does not exist, or Permissions are not set.");
			}

			$userID = $this->user->_id;
			$allUsers = User::all(array('_id'));
			
			if($mergePermission==1){
				//dd($mergePermission);
				//try to merge.
				//get all the Comments associated with this story.  Reassign to the master story. Reassign the comments' UserID to this->userID
				$comments = Comment::where("story_id", "=", $oneStory)->get();
				$comments->each(function(&$c){
					$c->story_id = $this->storyID;
					$c->user_id = $this->user->_id;
					$c->save();
				});
				$successbag[] =  "get all the Comments associated with this story (".$oneStory.").  Reassigned to the master story (". $storyID .") and User (". $this->user->_id .") \n";

				//get all the StoryPictures associated with this story.  Reassign to the master story.
				$storypictures = StoryPicture::where("story_id", "=", $oneStory)->get();
				$storypictures->each(function(&$c){
					$c->story_id = $this->storyID;
					$c->user_id = $this->user->_id;
					$c->save();
				});
				$successbag[] =  "get all the StoryPictures associated with this story (".$oneStory.").  Reassigned to the master story (". $storyID .") and User (". $this->user->_id .") \n";
				
				//get all the Likes associated with this story.  Reassign to the master story.
				$allUsers->each(function($u) use($storyID, $oneStory){
						$redis_key = "story:" . $oneStory . ":" . "userlike";
						$data = RedisL4::connection()->SISMEMBER($redis_key, $u->_id);
						if($data==true){
							$redis_key = "story:" . $oneStory . ":" . "userlike";
							RedisL4::connection()->SREM($redis_key, $u->_id);
							$redis_key = "story:" . $storyID . ":" . "userlike";
							RedisL4::connection()->SADD($redis_key, $u->_id);
						}	

						$redis_key = "user:" . $u->_id . ":" . "storylike";
						$data = RedisL4::connection()->SISMEMBER($redis_key, $oneStory);
						if($data==true){
							$redis_key = "user:" . $u->_id . ":" . "storylike";
							RedisL4::connection()->SREM($redis_key, $oneStory);
							$redis_key = "user:" . $u->_id . ":" . "storylike";
							RedisL4::connection()->SADD($redis_key, $storyID);
						}
				});
				$successbag[] =  "get all the Likes associated with this story (".$oneStory.").  Reassign to the master story (". $storyID .") .  \n";

				//get all the story:story-ID:useraccess associated with this story.  Reassign to the master story.
				$allUsers->each(function($u) use($storyID, $oneStory){
					$redis_key = "story:" . $oneStory . ":" . "useraccess";
					$data = RedisL4::connection()->SISMEMBER($redis_key, $u->_id);
					if($data==true){
						$redis_key = "story:" . $oneStory . ":" . "useraccess";
						RedisL4::connection()->SREM($redis_key, $u->_id);
						$redis_key = "story:" . $storyID . ":" . "useraccess";
						RedisL4::connection()->SADD($redis_key, $u->_id);
					}
				
					$redis_key = "user:" . $u->_id . ":" . "storyaccess";
					$data = RedisL4::connection()->SISMEMBER($redis_key, $oneStory);
					if($data==true){
						$redis_key = "user:" . $u->_id . ":" . "storyaccess";
						RedisL4::connection()->SREM($redis_key, $oneStory);
						$redis_key = "user:" . $u->_id . ":" . ":storyaccess";
						RedisL4::connection()->SADD($redis_key, $storyID);
					}
				});
				$successbag[] =  "get all the story:story-ID:useraccess associated with this story (".$oneStory.").  Reassigned to the master story (". $storyID .") .  \n";
				
				//get all the story:story-ID:userviews associated with this story.  Reassign to the master story.
				$allUsers->each(function($u) use($storyID, $oneStory){
					$redis_key = "story:" . $oneStory . ":" . ":userviews";
					$data = RedisL4::connection()->ZSCORE($redis_key, $oneStory);
					if($data > 0){
						$redis_key = "story:" . $oneStory . ":" . "userviews";
						$data = RedisL4::connection()->ZREM($redis_key, $oneStory);
						$redis_key = "story:" . $storyID . ":" . "userviews";
						$data = RedisL4::connection()->ZADD($redis_key, $storyID);
						
					}					
				});			
				$successbag[] =  "get all the story:story-ID:userviews associated with this story (".$oneStory.").  Reassigned to the master story (". $storyID .") .  \n";

				//get all the story:likes story:perms story:views associated with this story.  Reassign to the master story.
					$redis_key = "story:likes";
					$data_l = RedisL4::connection()->ZSCORE($redis_key, $oneStory);
					RedisL4::connection()->ZREM($redis_key, $oneStory);
					if($data_l>0){
						RedisL4::connection()->ZINCRBY($redis_key, $data_l, $storyID);
					}
					
					$redis_key = "story:perms";
					$data_p = RedisL4::connection()->ZSCORE($redis_key, $oneStory);
					RedisL4::connection()->ZREM($redis_key, $oneStory);
					if($data_p>0){
						RedisL4::connection()->ZINCRBY($redis_key, $data_p, $storyID);
					}
					
					$redis_key = "story:views";
					$data_v = RedisL4::connection()->ZSCORE($redis_key, $oneStory);
					RedisL4::connection()->ZREM($redis_key, $oneStory);
					if ($data_v > 0){
						RedisL4::connection()->ZINCRBY($redis_key, $data_v, $storyID);
					}
					$successbag[] =  "get all the story:likes(". $data_l ."), :perms(". $data_p ."), :views(". $data_v .") associated with this story (".$oneStory.").  Reassigned to the master story (". $storyID .") .  \n";

					//delete this story.  Do this last, because the delete event will cascade-delete
					//interesting things like comments, storypictures and Redis keys.				
					$s=Story::find($oneStory);
					$s->delete();
					$t=Story::find($oneStory);
					if($t != null){
						$failbag[] =  "Delete Failed on " . $oneStory . "\n";	
					}else{
						$successbag[] =  "Delete Succeeded on " . $oneStory . "\n";	
					}

				
				
				$successbag[] = ( $oneStory . " SUCCESSFULLY MERGED INTO " . $storyID . "");
			}
		}

		
		//return Response::json(['message' => 'Complete'], 200);
		return Response::json(['message' => 'Complete', 'success'=>$successbag, 'fail'=>$failbag], 200);
	}

	/**
	 * Very thin wrapper for awardPermissionToMerge, to be called from REST API.
	 * Does nothing but set $deny=true to force the removal of the Redis record.
	 * @param unknown $storyID
	 * @param string $deny
	 * @param string $userID
	 */	
	public function denyPermissionToMerge($storyID, $deny=false, $userID='me'){
		return $this->awardPermissionToMerge($storyID, true, $userID);
	}
	
	/**
	 * Make a Redis record of shape user:USERID:mergepermission -> val=many-user-ID
	 * This can be executed by a user who is the creator of ($storyID)
	 * If the logged
	 *  @param:  $storyID
	 *  @param: boolean (OPT)$deny If != false, removes the Redis record
	 *  @param: (OPT)$userID
	 *  @return: string completed_status_message
	 */
	public function awardPermissionToMerge($storyID, $deny=false, $userID='me'){
		if(null==Auth::User()){
			return Response::json(array('message' => 'Not Logged In'),    403);
		}
		
		$this->user = $this->userRepo->discover($userID);
		if(null==$this->user){
			return Response::json(array('message' => 'Bad User'),    403);
		}

		$ss = Story::find($storyID);
		if(null != $ss){
			if( $ss->user_id !=  $this->user->_id ){
				return Response::json(array('message' => 'You are not the Story Creator'),    403);
			}else{
				
				//OK for now.
			}
		}else{
			return Response::json(array('message' => 'Bad StoryID'),    403);
		}
		
		$this->fill_allUserID();
		$successbag=array();
		$failbag   =array();
		//var_dump("Passed login-check, bad-user check and Story Creator check");
		//we got this far.  Since $userID or Me is the owner of this story, we can assign right-to-merge
		//to any user we please.
		foreach($this->userID_data as $oneUser){

			$oneUserDetails = User::find($oneUser);
			if(null != $oneUserDetails){
				$redis_key = "story:" . $storyID . ":" . "mergepermission";
				if(false != $deny){
					$data = RedisL4::connection()->SREM($redis_key, $oneUser);
					$successbag[] = ($oneUser . " denied from " . $storyID);	
				}else{
					$data = RedisL4::connection()->SADD($redis_key, $oneUser);
					$successbag[] = ("User: " . $oneUser . " awarded access to story: " . $storyID);
				}
			}else{
				$failbag[] = ($oneUser . " is not a valid user ");
			}
		}
		
		
		return Response::json(['message' => 'Complete', 'success'=>$successbag, 'fail'=>$failbag], 200);
		
	}
	
	/**
	 * Convenience function to check if a certain user has permission to merge a certain story.
	 * This permission is given if the user is the story's creator OR if the user has been
	 * explicitly awarded permission.
	 *  @param:  $storyID
	 *  @param:  $userID
	 *  @return: int (1 or 0) 
	 */	
	private function checkMergePermission($storyID, $userID){
		$su = Story::find($storyID);
		if($su !=null){
			$su_id = $su->user_id;	
			if($su_id==$userID){
				return 1;
			}
		}else{
			return 0;
		}
		
		$redis_key = "story:" . $storyID . ":" . "mergepermission";
		if((RedisL4::connection()->SISMEMBER($redis_key, $userID))==true){
			return 1;
		}else{
			return 0;
		};
		
	}
}
