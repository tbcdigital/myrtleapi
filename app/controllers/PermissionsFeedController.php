<?php
/**
*	All feeds that have to do with User Permissions.
*
*/
use CommonCore\Users\User;
use Carbon\Carbon;

class PermissionsFeedController extends \BaseController{

	/**
	 * User repository
	 *
	 * @var CommonCore\Users\UserRepositoryInterface
	 */
	protected $userRepo;
	
	/**
	 * 	Suffix for the Redis Key User type
	 */
	protected $userkey = "useraccess";
	
	/**
	 * The ID of the authenticated user.
	 *
	 * @var string
	 */
	protected $userID = '';

	/**
	 * Passed in or discovered user
	 * 
	 * @var User obj
	 */
	protected $user=null;	
	
	/**
	 * 	A list of things to append to the URL at pagination
	 */
	protected $urlAppend;
	
	/**
	 * 	Default items-per-page at pagination time
	 */
	protected $pageCount=50;
	
	/**
	 * Discovered Story
	 */
	protected $story = null;
	
	/**
	 * Validate that some user is logged in.
	 * Store that user ID to a class-level var.
	 * Get the array of User ID's
	 * Store
	 *
	 *  @param:  null
	 *  @return: string completed_status_message
	 */	


	public function __construct(CommonCore\Users\UserRepositoryInterface $userRepo)
	{
		$this->userRepo = $userRepo;
	}	
	
	
	
	/** 
	 * Accepts a story ID; deletes all permissions from Redis
	 * (typicallly, the keys story:{storyID}:useraccess and user:{All_USERS}:storyaccess FOR storyID)
	 * Efficiency is not high, so should be used sparingly, and never for read-critical operations.
	 * 
	 * Explicitly DOES NOT change any likes, votes, counts etc, which are considered to be legacy data.
	 * Explicitly DOES NOT change any aspect of any User record.
	 * 
	 * @param unknown $storyID
	 * */
	
	/** 
	 * SYSTEM ARCHITECTURE NOTE.
	 * Consider this function as a candidate for inclusion in some future version of the generic
	 * "Votes" Class, since it is functionally a superset of the delByStory() function.  In this
	 * case, it needs a companion function to delete all (things [likes, perms, bells, whistles] )
	 * by User. 
	 * 
	 * But for now, leave it separate and in a different class since it is concerned exclusively 
	 * with Permissions.
	 * */
	function removePermissionsByStory($storyID){

		if(null==Auth::User()){
			return Response::json(array('message' => 'Not Logged In'),    403);
		}
		
		$story = Story::find( $storyID );
		$storyOwner = $story->user_id;
		
		if ( $storyOwner != Auth::User()->getKey() ){
		//	return Response::json(array('message' => 'Not The Story Owner.'),    403);
		}	
			
		$successbag = array();
		$failbag    = array();
		
		$redis_key = "story:" . $storyID . ":useraccess";
		
		//i.  Run the query to get a list of users who have permissions on this story.
		$allUsers =  RedisL4::connection()->SMEMBERS($redis_key);
		$rez = 0;
		
		/** we have a list of users.  Each user should have a counter-key for each entry.*/
		foreach($allUsers as $user){
			$redis_key_u = "user:" . $user . ":storyaccess";
			
			$rez =  RedisL4::connection()->SREM($redis_key_u, $storyID);
			
			if($rez == 0){
				$failbag[] = "Record Set " . $redis_key_u . " entry for Story " . $storyID . " - deletion attempt failed or no entry was present.";
			}else{
				$successbag[] = "Record Set " . $redis_key_u . " entry for Story " . $storyID . " - succeeded.";
				

				
			}
			
		}
		
		/** The deletion of the counter-key should be transactionally dependent on the deletion of the main key
		 *  There is the possibility that some user:{user}:storyaccess -> story entries could be lurking around,
		 *  but without doing a very expensive user:{all-users-on-system} check, we don't have a reliable way to 
		 *  hoover them up at the moment.
		 *  
		 *  Let's go ahead and delete the main key.
		 */
		$rez =  RedisL4::connection()->DEL($redis_key);
		if($rez == 0){
			$failbag[] = "Record Set " . $redis_key . " - deletion attempt failed or key does not exist.";
		}else{
			$successbag[] = "Record Set " . $redis_key . " - delete succeeded.";
		}		

		/** The KVP Storage appears to have worked.  Let's carefully decrement the Database record,
		 * taking care that this figure never goes below 0. */
		
		$dbr = Story::find($storyID);
		$dbr->permissions = 0;
		$dbr->save();
		
		
		return Response::json(['message' => 'Complete', 'success'=>$successbag, 'fails'=>$failbag], 200);
		
	}
	
	/**
	 * Accepts a story ID; returns a list of users who have permission to view that story.
	 * @param unknown $storyID
	 */
	public function getUsersWithPermission($storyID){

		if(null==Auth::User()){
			return Response::json(array('message' => 'Not Logged In'),    403);
		}
		
		$this->resultsMask = array('_id', 'username', 'profile_image', 'first_name', 'last_name');
		
		$this->story = Story::find($storyID);
		
		if($this->story==null){
			return Response::json(array('message' => 'Not Valid Story ID'),    403);
		}
		
		$creatorID = $this->story->user_id;
		
		/*
		if( Story::find($storyID)->user_id !=  Auth::User()->getKey() ){
			return Response::json(array('message' => 'You are not the Story Creator'),    403);
		}
		*/		
		$redis_key = "story:" .  $storyID . ":" . $this->userkey;
		$data = RedisL4::connection()->SMEMBERS($redis_key);
		//dd($data);
		
		//INCLUDE all of the users pulled from Redis
		$this->query = User::whereIn("_id", $data);
		//EXCLUDE the Creator.  We'll get them later, and we don't want them twice.
		$this->query->where( "_id", "!=", $creatorID );
		
		
		
		$results = $this->makeReturnObject();
		return Response::json($results , 200);		
		
	}
	
	/**
	 * 	does not accept any arguments.
	 * 	uses $this->query, which must be a querysuitable for having a get() run on it.
	 *  uses $this->pageCount to determine how many data objects per page are returned.
	 *  uses get('page',0) to determine which page is to be presented
	 *  
	 *  @returns an array consisting of:
	 *  	-a pagination object
	 *  	-a user object
	 *  	-an object containing many Data objects.  The type of the Data objects is dependent 
	 *  	 on the query set as $this->query.
	 */
	private function makeReturnObject(){

		// Page num?
		$page = intval(Input::get('page',0));
		
		//Per page?
		if($page==0){
			$perpage = intval(Input::get('count',0)) > 0 ? intval(Input::get('count')) : $this->pageCount;
		}else{
			$perpage = intval(Input::get('count',0)) > 0 ? intval(Input::get('count'))-1 : $this->pageCount-1;
		}
		
		$this->query->take($perpage);
		
		$this->query->skip($perpage * $page);
	
		$this->query->orderBy('username');
	
		$results = $this->query->get($this->resultsMask)->toArray();
	
		if ( $page==0 ){
			$creatoruser = User::find($this->story->user_id)->toArray();
			array_unshift($results, $creatoruser);			 
		}
		//dd($results);
		
		$pagination = array(
				'pagination' => [
				'requested' => [
				'page' => $page,
				'count' => $perpage
				],
				'page' => $page,
				'navigation' => [
				'previous' => $page <= 0 ? null : URL::current().'?count='.$perpage.'&page='.($page-1) . $this->urlAppend,
				'next' => count($results) >= $perpage ? URL::current().'?count='.$perpage.'&page='.($page+1). $this->urlAppend : null
				]
				]
		);
	
			
		$returnResults["pagination"] = $pagination;
		$returnResults["users"] = $results;
	
	
	
		return($returnResults);
	}
	
	/**
	 * 
	 * @param array $returnResults
	 * @return array
	 * 
	 * Typically invoked after a paginated get().  This function is to manipulate a small subset
	 * of all of the dat objects returned, which can be further manipulated, for instance to add extra data,
	 * before being packaged up into the return{} object's data{} object.
	 */
	protected function resultsReorder($returnResults){
		//Stub Function for special re-ordering / blocking rules F.I requiring Redis.
		//Note that anything here will only act on whatever was GETted from the DB by $this->query
		return $returnResults;
	}
	
		
	
}	