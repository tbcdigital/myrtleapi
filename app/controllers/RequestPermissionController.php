<?php

use CommonCore\Users\User;
use Carbon\Carbon;

class RequestPermissionController extends \BaseStorysUsersController{
	
	public function requestPermission($storyID){
		
		$story = Story::find($storyID);
		
		$nfc = new NotificationsHandler;
		$msg = "";
		$msg = $msg . Auth::User()->username;
		$msg = $msg . " requested to be added to ";
		$msg = $msg . $story->title;
		
		return $nfc->makeDBentry($story->getKey() , $msg , $story->user_id, $storyID, 'permissionrequest');		
		
	}
	
	
	
}