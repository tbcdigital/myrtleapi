<?php
/**
 * Handle Email Invitations. 
*/
class InvitesController extends \BaseController
{


    /**#@+
     * an array holding useful information about different types of mail we may have
     * to send.
     * 
     * @var array
     */
    private $mailTypes = [
        'comeToMyrtle' => [
            'mailtext' => 'Generic Mail Text about Myrtle',
            'returnAddress' => 'noreply@noreply.com',
            'messageSubject' => 'Come and Join Us!'
        ],
        
        'comeToMyEvent' => [
            'mailtext' => 'Specific Mail Text about Event',
            'returnAddress' => 'confirm@event.com',
            'messageSubject' => 'Come and Join Our Event!'
        ],
        
        'comeToDifferentEvent' => [
            'mailtext' => 'Specific Mail Text about Different Event',
            'returnAddress' => 'confirm@event.com',
            'messageSubject' => 'Come and Join Our Different Event!'
        ]
    ];

    
    /**
     * Execute mail invitations.
     * 
     * @param: array $event_ID .
     * @return: string completed_status_message
     */    
    public function generalInvite($event_ID = null)
    {

    	if(null==Auth::User()){
    		{return Response::json(array('message' => 'Not Logged In'),    200);}
    	}else{
    		$user = Auth::User()->toArray();
    		$user_ID = $user['_id'];
    	}    	
    	
    	$addresses = (array) Input::get('email', []);
        
        if (empty($addresses)) {
            return Response::json(array(
                'message' => 'No Email Address'
            ), 200);
        }
        
        foreach ($addresses as $address) {
            
            if ($event_ID == null) {
                Mail::send('emails.invite.generic', $this->mailTypes['comeToMyrtle'], function ($msg) use($address)
                {
                    $msg->to($address, "User Name")->subject('Myrtle:: Come and Join Us');
                });
            } else {
                $this->mailTypes['comeToMyEvent']['eventID'] = $event_ID;
                Mail::send('emails.invite.event', $this->mailTypes['comeToMyEvent'], function ($msg) use($address)
                {
                    $msg->to($address, "User Name")->subject('Myrtle:: Come and Join Our Event');
                });
            }
        }
        
        return Response::json(array(
            'message' => 'Completed Normally'
        ), 200);
    }
    
}