<?php

use CommonCore\Users\User;
/**
 * 	IN DEV.
 *  To handle some kind of expanded User Profile.  Subject to ad-hoc changes at any time.
 *
 */
class UsersProfileExtensionController extends \BaseController{

	/**
	 * User repository
	 *
	 * @var CommonCore\Users\UserRepositoryInterface
	 */
	protected $userRepo;

	/**
	 * 	The user logged in by common core
	 */
	protected $user = null;

	protected $followingCount = null;
	protected $followerCount = null;
	protected $howManyPrivateItems = null;	

	public function __construct(CommonCore\Users\UserRepositoryInterface $userRepo)
	{
		$this->userRepo = $userRepo;
	}
		
	
	public Function userProfile($userID='me'){
		
		if(null==Auth::User()){
			return Response::json(array('message' => 'Not Logged In'),    403);
		}
		
		$this->user = $this->userRepo->discover($userID);
		if(null==$this->user){
			return Response::json(array('message' => 'Bad User'),    403);
		}
		
		$expandedUser = $this->user->toArray();
		
		$redis_key = "user:" .  $this->user->_id . ":following";
		$data = RedisL4::connection()->SMEMBERS($redis_key);
		$this->followingCount=sizeof($data);
		
		$redis_key = "user:" .  $this->user->_id  . ":followedby";
		$flw = RedisL4::connection()->SMEMBERS($redis_key);
		$this->followerCount=sizeof($flw);		
		
		if(array_key_exists('description', $expandedUser) == false){
			$expandedUser['description'] = "";
		}
		
		$expandedUser['followingCount'] = $this->followingCount;
		$expandedUser['followerCount'] = $this->followerCount;
		$expandedUser['privateArticleCount'] = Story::where("user_id", "=", $this->user->id)->where("privacy", "=", "private")->get()->count();
		$expandedUser['publicArticleCount']  = Story::where("user_id", "=", $this->user->id)->where("privacy", "=", "public")->get()->count();
		
		return $expandedUser;
	}
	
}