<?php
/**
 * Handle Setting / Unsetting of Permissions to view Story per user. 
 * Derived from BaseStorysUsersController.
*/
class StorysUsersPermissionsController extends \BaseStorysUsersController{


	/**
	 * 	Suffix for the Redis Key User type
	 */
	protected $userkey = "useraccess";
	
	/**
	 * 	Suffix for the Redis Key Story type
	 */
	protected $storykey = "storyaccess";
	
	/**
	 * 	Suffix for the Redis type Key (likes or perm)
	 */
	protected $typekey = "perms";

	
	//beginning of new block
	
	function setByStory($storyID){
	
		if(null==Auth::User()){
			return Response::json(array('message' => 'Not Logged In'),    403);
		}
	
		$this->story = Story::find($storyID);
	
		if($this->story==null){
			return Response::json(array('message' => 'Not Valid Story ID'),    403);
		}
		
		if( Story::find($storyID)->user_id !=  Auth::User()->getKey() ){
			//return Response::json(array('message' => 'You are not the Story Creator'),    403);
		}
	
		$this->fill_allUserID();
	
		foreach($this->allUserID as $userID){
			
			
			//if we are ADDING PERMS, we mustn't increment more than one time.
			// has not already been set:
			$redis_key = "user:" .  $userID . ":" . $this->storykey;
			$data = RedisL4::connection()->SISMEMBER($redis_key,$storyID);			
			if(null != $data){
				$failbag[] = (Auth::User()->getKey() . " Already registered on " . $storyID);
				break;
			}			
			
			$redis_key = "user:" .  $userID . ":" . $this->storykey;
			$data = RedisL4::connection()->SADD($redis_key, $storyID);
				
			$redis_key = "story:" .  $storyID . ":" . $this->userkey;
			$data = RedisL4::connection()->SADD($redis_key, $userID);
	
			$redis_key = "story:" . $this->typekey;
			$data = RedisL4::connection()->ZINCRBY($redis_key, 1, $storyID);
				
			$dbo = Story::find($storyID);
			$dbo->increment("permissions");
			
			$this->registerOperations($userID);
		}
		
		return Response::json(['message' => 'Completed'], 200);
	}
	
	function delByStory($storyID){
		
		//dd( Story::find($storyID)->toArray() );
		
		if(Story::find($storyID)==null){
			return Response::json(array('message' => 'Not Valid Story ID'),    403);
		}
	
		if(null==Auth::User()){
			return Response::json(array('message' => 'Not Logged In'),    403);
		}
	
		$this->fill_allUserID();
		
	
		$this->fill_allUserID();
	
		foreach($this->allUserID as $userID){

			$redis_key = "story:" .  $storyID . ":" . $this->userkey;
			$data = RedisL4::connection()->SISMEMBER($redis_key, $userID);
			
			if($data == null){
				if( Story::find($storyID)->user_id !=  Auth::User()->getKey() ){
					continue;
					//return Response::json(array('message' => 'You are not the Story Creator'),    403);
				}
			}
				
			
			$redis_key = "user:" .  $userID . ":" . $this->storykey;
			$data = RedisL4::connection()->SREM($redis_key, $storyID);
				
			$redis_key = "story:" .  $storyID . ":" . $this->userkey;
			$data = RedisL4::connection()->SREM($redis_key, $userID);
				
			$redis_key = "story:" . $this->typekey;
			$data = RedisL4::connection()->ZINCRBY($redis_key, -1, $storyID);
	
			$dbo = Story::find($storyID);
			$dbo->decrement("permissions");
		}
	
		return Response::json(['message' => 'Completed'], 200);
	}
	
	function getByStory($storyID){
	
		if(Story::find($storyID)==null){
			return Response::json(array('message' => 'Not Valid Story ID'),    403);
		}
	
		$this->fill_allUserID();
	
		$returnresult=array();
		$allreturnresult = array();
	
		$allreturnresult['event_id'] = $storyID;
	
		foreach($this->allUserID as $userID){
			$redis_key = "story:" .  $storyID . ":" . $this->userkey;
			$data = RedisL4::connection()->SISMEMBER($redis_key, $userID);
				
			$returnresult[] = array($userID, $data);
		}
		$allreturnresult['data']=$returnresult;
		return Response::json($allreturnresult, 200);
	}
	
	
	function setByUser($userID='me'){
	
		if(null==Auth::User()){
			return Response::json(array('message' => 'Not Logged In'),    403);
		}
	
		$this->user = $this->userRepo->discover($userID);
		if(null==$this->user){
			return Response::json(array('message' => 'Bad User'),    403);
		}
	
		$userID = $this->user->_id;
	
		$this->fill_allStoryID();
	
		$failbag = array();
		$successbag = array();
	
		foreach($this->allStoryID as $storyID){
			$this->story = Story::find($storyID);
			//dd($this->story);
			if($this->story->user_id ==  Auth::User()->getKey() ){

				//if we are ADDING PERMS, we mustn't increment more than one time.
				// has not already been set:
				$redis_key = "user:" .  $userID . ":" . $this->storykey;
				$data = RedisL4::connection()->SISMEMBER($redis_key,$storyID);
				if(null != $data){
					$failbag[] = (Auth::User()->getKey() . " Already registered on " . $storyID);
					break;
				}				
				
				$redis_key = "user:" .  $userID . ":" . $this->storykey;
				$data = RedisL4::connection()->SADD($redis_key, $storyID);
	
				$redis_key = "story:" .  $storyID . ":" . $this->userkey;
				$data = RedisL4::connection()->SADD($redis_key, $userID);
	
				$redis_key = "story:" . $this->typekey;
				$data = RedisL4::connection()->ZINCRBY($redis_key, 1, $storyID);
	
				//$dbo = Story::find($storyID);
				$this->story->increment("permissions");
				
	
				$successbag[] = ($userID . " Added on " . $storyID);
				$this->registerOperations($userID);
			}else{
				$failbag[] = (Auth::User()->getKey() . " Is Not story creator on " . $storyID);
			}
		}
		
		return Response::json(['message' => 'Complete', 'success'=>$successbag, 'fails'=>$failbag], 200);
	
	
	}
	
	function delByUser($userID='me'){
	
		if(null==Auth::User()){
			return Response::json(array('message' => 'Not Logged In'),    403);
		}
	
		$this->user = $this->userRepo->discover($userID);
		if(null==$this->user){
			return Response::json(array('message' => 'Bad User'),    403);
		}
	
		$userID = $this->user->_id;
	
		$this->fill_allStoryID();
	
		$failbag = array();
		$successbag = array();
	
		foreach($this->allStoryID as $storyID){
			
			$redis_key = "story:" .  $storyID . ":" . $this->userkey;
			$data = RedisL4::connection()->SISMEMBER($redis_key, $userID);
			
			if( Story::find($storyID)->user_id ==  Auth::User()->getKey() || $data != false ){
				$redis_key = "user:" .  $userID . ":" . $this->storykey;
				$data = RedisL4::connection()->SREM($redis_key, $storyID);
					
				$redis_key = "story:" .  $storyID . ":" . $this->userkey;
				$data = RedisL4::connection()->SREM($redis_key, $userID);
	
				$redis_key = "story:" . $this->typekey;
				$data = RedisL4::connection()->ZINCRBY($redis_key, -1, $storyID);
	
				$dbo = Story::find($storyID);
				$dbo->decrement("permissions");
	
				$successbag[] = (Auth::User()->getKey() . " Deleted from " . $storyID);
			}else{
				$failbag[] = (Auth::User()->getKey() . " Is Not story creator on, and does not have permissions on " . $storyID);
			}
		}
	
		return Response::json(['message' => 'Complete', 'success'=>$successbag, 'fails'=>$failbag], 200);
	}
	
	function getByUser($userID){
	
		if(null==Input::get("storyID")){
				
			return $this->getAllStorysByUserID($userID);
		}else{
				
			$this->fill_allStoryID();
				
				
			$returnresult=array();
			$allreturnresult = array();
				
			$allreturnresult['user_id'] = User::find($userID)->toArray();
				
			foreach($this->allStoryID as $storyID){
				$redis_key = "user:" .  $userID . ":" . $this->storykey;
				$data = RedisL4::connection()->SISMEMBER($redis_key, $storyID);
				if($data==true){
					$returnresult[] = array(Story::find($storyID), $data);
				}else{
					$returnresult[] = array($storyID, $data);
				}
			}
			$allreturnresult['data']=$returnresult;
			return Response::json($allreturnresult, 200);
		}
	}
	
	function getAllStorysByUserID($userID){
		//get a list of stories which this user is specifically allowed to see
		$s = Story::all();
		$returnresult=array();
	
		$allreturnresult['user_id'] = User::find($userID)->toArray();
		$redis_key = "user:" .  $userID . ":" . $this->storykey;
		$data = RedisL4::connection()->SMEMBERS($redis_key);
	
		$returndata=array();
	
		foreach($data as $dataitem){
			$returndata[] = Story::find($dataitem)->toArray();
		}
		$allreturnresult['data']=$returndata;
		return Response::json($allreturnresult, 200);
	}

	//end of new block
	
	
	
	
	
	function registerOperations($userID){
		Event::fire('story.permissiongranted', array($this->story, $userID) );
	}
	
}