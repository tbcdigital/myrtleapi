<?php
use CommonCore\Core\Controllers\BaseController;
use Carbon\Carbon;

/**
 * PHP implementation of the Amazon Token Vending Machine
 *
 * This controller will create federated access tokens that are unique to the logged in user,
 * to allow upload access to the S3 bucket. The resource access is namespaced for additional
 * security so you may only upload images into your own S3 path. 
 */
class STSController extends BaseController {
	
	/**
	 * The duration (in hours) that a generated token should last before expiring
	 * 
	 * Must be between 15mins (900secs) and 36 hours (129600secs)
	 * 
	 * @var int
	 */
	protected $durationHours = 3;
	
	protected $policy = '{
		"Statement": [
		    {
			"Sid": "AllowUploadAccessv1",
			"Effect": "Allow",
			"Action": [
				"s3:AbortMultipartUpload",
				"s3:DeleteObject",
				"s3:GetObject",
				"s3:PutObject",
				"s3:RestoreObject"
			    ],
			"Resource": [
			    "arn:aws:s3:::myrtle/originals/<userid>/*"
			    ]
			}
		]}';
	
	/**
	 * Entry method for vending the token
	 */
	public function vendNewToken()
	{
		
		if(false === Auth::check()) {
			return Response::json(['message' => 'Not logged in'], 403);
		}
		
		/* @var $client \Aws\Sts\StsClient */
		$client = App::make('aws')->get('sts');
				
		//$userid = '5448c15b39a23c8348316f9c';
		$userid = Auth::user()->getKey();

		
		try {
			
			$response = $client->getFederationToken([
				'Name'=> Auth::user()->getKey(),
				'DurationSeconds' => $this->durationHours * 60 * 60, 
				'Policy' => str_replace(["\r", "\n", "\r\n"], '', str_replace('<userid>', Auth::user()->getKey(), $this->policy))
			]);
					
			$credentials = $client->createCredentials($response);
			
		} catch (Exception $e) {
			return $this->respondWithInternalError($e->getMessage());
		}
		
		$expire = new Carbon($credentials->getExpiration());
		
		return Response::json([
			'key' => $credentials->getAccessKeyId(),
			'secret' => $credentials->getSecretKey(),
			'token' => $credentials->getSecurityToken(),
			'current_time' => Carbon::now()->toDateTimeString(),
			'expires_at' => $expire->toDateTimeString(),
			'duration' => $expire->diffInMinutes(null, true)
		]);
		
	}
		
}