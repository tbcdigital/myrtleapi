<?php
/**
 * Handle Setting / Unsetting of Likes per Story and User. 
 * Derived from BaseStorysUsersController.
*/
class StorysUsersLikeController  extends \BaseStorysUsersController{


	/**
	 * 	Suffix for the Redis Key User type
	 */
	protected $userkey = "userlike";
	
	/**
	 * 	Suffix for the Redis Key Story type
	 */
	protected $storykey = "storylike";
	
	/**
	 * 	Suffix for the Redis type Key (likes or perm)
	 */
	protected $typekey = "likes";	
	
	function registerOperations($userID){
		Event::fire('story.liked', array($this->story, $userID));
	}
	
}