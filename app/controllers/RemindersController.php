<?php
use CommonCore\Users\User;
use \Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
class RemindersController extends Controller {

	/**
	 * We don't use laravels built in password stuff as it relies on web views
	 * Current implementation is to just send a new password via email
	 */
	public function resetPassword() {

		if(Input::has('email')) {
			$username = Input::get('email');
		} else if (Input::has('username')) {
			$username = Input::get('username');
		} else {

			return Response::json(array('message' => 'No valid credentials were found'), 403);

		}


		if(false !== filter_var($username, FILTER_VALIDATE_EMAIL)) {

			$user = User::where('email', '=', $username)->first();

		} else {

			$user = User::where('username', '=', $username)->first();

		}


		if(is_null($user)) {

			return Response::json(array('message' => 'User not found'), 404);

		} else {

			//--- generate new password
			$newpassword = str_random(10);

			//--- set it
			/* @var $user User */
			$user->password = $newpassword;
			$user->save();

			//---send email
			Mail::send('emails.auth.reset', array('password' => $newpassword, 'first_name' => $user->first_name), function($msg) use ($user) {

				$msg->to($user->getReminderEmail(), $user->first_name . " " . $user->last_name)->subject('MYRTLE :: Password Reset');

			});


			//---done
			return Response::json(array('message' => 'Password sent to email account'));

		}



	}

}