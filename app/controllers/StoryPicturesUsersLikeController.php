<?php
/**
 * Handle Setting / Unsetting of Likes per Story and User. 
 * Derived from BaseStorysUsersController.
*/

use CommonCore\Users\User;
use Carbon\Carbon;

class StoryPicturesUsersLikeController extends \BaseStorysUsersController{


	/**
	 * 	Suffix for the Redis Key User type
	 */
	protected $userkey = "userlike";
	
	/**
	 * 	Suffix for the Redis Key Story type
	 */
	protected $storypicturekey = "storypicturelike";
	
	/**
	 * 	Suffix for the Redis type Key (likes or perm)
	 */
	protected $typekey = "likes";	
	

	/**
	 * User repository
	 *
	 * @var CommonCore\Users\UserRepositoryInterface
	 */
	protected $userRepo;
	
	/**
	 * The ID of the authenticated user.
	 *
	 * @var string
	 */
	protected $userID = '';
	
	/**
	 * An array to hold all the User ID's (where one story / multiple users are set).
	 *
	 * @var string[]
	 */
	protected $allUserID=array();
	
	/**
	 * An array to hold all the Story ID's (where one user / multiple storys are set).
	 *
	 * @var string[]
	*/
	protected $allStorypictureID=array();
	
	/**
	 * Passed in or discovered user
	 *
	 * @var User obj
	*/
	protected $user=null;
	
	/**
	 * Passed-in Storypicture.  In this class only, it will hold a Storypicturepicture object.
	 *
	 * @var Storypicture obj
	 */
	protected $storypicture=null;
	

	
	/**
	 * Validate that some user is logged in.
	 * Store that user ID to a class-level var.
	 * Get the array of User ID's
	 * Store
	 *
	 *  @param:  null
	 *  @return: string completed_status_message
	 */
	
	
	public function __construct(CommonCore\Users\UserRepositoryInterface $userRepo)
	{
		$this->userRepo = $userRepo;
	}
	
	
	function fill_allStorypictureID(){
	
		$storypictureID_data = Input::get("storypictureID");
		if (is_array($storypictureID_data)){
			foreach($storypictureID_data as $storypicture){
				$st = StoryPicture::find($storypicture)->user_id;
				array_push($this->allStorypictureID, $storypicture);
			}
		}else{
			array_push($this->allStorypictureID, $storypictureID_data);
		}
	}
	
	function fill_allUserID(){
		$userID_data = Input::get("userID");
			
		if (is_array($userID_data)){
			foreach($userID_data as $user){
				array_push($this->allUserID, $user);
			}
		}else{
			array_push($this->allUserID, $userID_data);
		}
	}
	
	function validateAndLoad_User($userID){
		$this->fill_allStorypictureID();
	}
	
	function setByStorypicture($storypictureID){
		
		//dd("StoryPicturesUsersLikeController :: setByStorypicture");
	
		if(null==Auth::User()){
			return Response::json(array('message' => 'Not Logged In'),    403);
		}
	
		$this->storypicture = StoryPicture::find($storypictureID);
	
		if($this->storypicture==null){
			return Response::json(array('message' => 'Not Valid Storypicture ID'),    403);
		}
	
		if($this->storypicturekey != "storypicturelike"){
			if( StoryPicture::find($storypictureID)->user_id !=  Auth::User()->getKey() ){
				return Response::json(array('message' => 'You are not the Storypicture Creator'),    403);
			}
		}
	
		$this->fill_allUserID();
	
	
		if(  !empty($this->allUserID)  ){
			if( $this->allUserID[0] === null ){
				array_push(  $this->allUserID, Auth::User()->getKey()  );
			}
		}
	
		//dd($this->allUserID);
	
		$failbag = array();
		$successbag = array();
	
		foreach($this->allUserID as $userID){
				
			//if we are LIKEing, and now awarding permissions, we have to chaeck that the LIKE
			// has not already been set:
			$redis_key = "user:" .  $userID . ":" . $this->storypicturekey;
				
			$data = RedisL4::connection()->ZSCORE($redis_key,$storypictureID);
				
			if(null != $data &&  $this->storypicturekey == "storypicturelike" ){
				$failbag[] = $userID . " Is already set on " . $storypictureID;
				break;
			}
				
			
	
	
			$redis_key = "user:" .  $userID . ":" . $this->storypicturekey;
			$data = RedisL4::connection()->ZADD($redis_key,Carbon::now()->getTimestamp(), $storypictureID);
				
			$redis_key = "storypicture:" .  $storypictureID . ":" . $this->userkey;
			$data = RedisL4::connection()->ZADD($redis_key,Carbon::now()->getTimestamp(), $userID);
	
			$redis_key = "storypicture:" . $this->typekey;
			$data = RedisL4::connection()->ZINCRBY($redis_key, 1, $storypictureID);
				
			$dbo = StoryPicture::find($storypictureID);
			$dbo->increment("likes");
			$successbag[] = ($userID . " Added on " . $storypictureID);
				
			$this->registerOperations($userID);
		}
	
		//return Response::json(['message' => 'Completed'], 200);
		return Response::json(['message' => 'Complete', 'success'=>$successbag, 'fails'=>$failbag], 200);
	}
	
	function delByStorypicture($storypictureID){
		
		//dd("StoryPicturesUsersLikeController :: delByStorypicture");
		
		//dd("DEL-BY-STORY");
		if(StoryPicture::find($storypictureID)==null){
			return Response::json(array('message' => 'Not Valid Storypicture ID'),    403);
		}
	
		if(null==Auth::User()){
			return Response::json(array('message' => 'Not Logged In'),    403);
		}
		/*
			if( Storypicture::find($storypictureID)->user_id !=  Auth::User()->getKey() ){
		return Response::json(array('message' => 'You are not the Storypicture Creator'),    403);
		}
		*/
		$this->fill_allUserID();

		$failbag = array();
		$successbag = array();		
		
		foreach($this->allUserID as $userID){
			
			$redis_key = "user:" .  $userID . ":" . $this->storypicturekey;
	
			$data = RedisL4::connection()->ZSCORE($redis_key,$storypictureID);
			
			if(null == $data &&  $this->storypicturekey == "storypicturelike" ){
				$failbag[] = $userID . " was not set on " . $storypictureID;
				break;
			}
			
				
			$redis_key = "user:" .  $userID . ":" . $this->storypicturekey;
			$data = RedisL4::connection()->ZREM($redis_key, $storypictureID);
				
			$redis_key = "storypicture:" .  $storypictureID . ":" . $this->userkey;
			$data = RedisL4::connection()->ZREM($redis_key, $userID);
				
			$redis_key = "storypicture:" . $this->typekey;
			$data = RedisL4::connection()->ZINCRBY($redis_key, -1, $storypictureID);
	
			$dbo = StoryPicture::find($storypictureID);
			$dbo->decrement("likes");
			
			$successbag[] = ($userID . " Removed from " . $storypictureID);
		}
	
		return Response::json(['message' => 'Complete', 'success'=>$successbag, 'fails'=>$failbag], 200);
	}
	
	function getByStorypicture($storypictureID){
		
		//dd("StoryPicturesUsersLikeController :: getByStorypicture");
	
		if(StoryPicture::find($storypictureID)==null){
			return Response::json(array('message' => 'Not Valid Storypicture ID'),    403);
		}
	
		$this->fill_allUserID();
	
		$returnresult=array();
		$allreturnresult = array();
	
		$allreturnresult['event_id'] = $storypictureID;
	
		foreach($this->allUserID as $userID){
			$redis_key = "storypicture:" .  $storypictureID . ":" . $this->userkey;
			$data = RedisL4::connection()->ZSCORE($redis_key, $userID);
			if($data != 0){
				$returnresult[] = array($userID);
			}
		}
		$allreturnresult['data']=$returnresult;
		return Response::json($allreturnresult, 200);
	}
	
	
	function setByUser($userID='me'){
	
		if(null==Auth::User()){
			return Response::json(array('message' => 'Not Logged In'),    403);
		}
	
		$this->user = $this->userRepo->discover($userID);
		if(null==$this->user){
			return Response::json(array('message' => 'Bad User'),    403);
		}
	
		$userID = $this->user->_id;
	
		$this->fill_allStorypictureID();
	
		$failbag = array();
		$successbag = array();
	
		
		foreach($this->allStorypictureID as $storypictureID){
				
			//if we are LIKEing, and now awarding permissions, we have to chaeck that the LIKE
			// has not already been set:
			$redis_key = "user:" .  $userID . ":" . $this->storypicturekey;
			$data = RedisL4::connection()->ZSCORE($redis_key,$storypictureID);
			if(null != $data &&  $this->storypicturekey == "storypicturelike" ){
				$failbag[] = $userID . " Is already set on " . $storypictureID;
				break;
			}
			
			$this->storypicture = StoryPicture::find($storypictureID);
			//if($this->storypicture ->user_id ==  Auth::User()->getKey()  || $this->storypicturekey == "storypicturelike"){
				$redis_key = "user:" .  $userID . ":" . $this->storypicturekey;
				$data = RedisL4::connection()->ZADD($redis_key, Carbon::now()->getTimestamp(), $storypictureID);
	
				$redis_key = "storypicture:" .  $storypictureID . ":" . $this->userkey;
				$data = RedisL4::connection()->ZADD($redis_key, Carbon::now()->getTimestamp(), $userID);
	
				$redis_key = "storypicture:" . $this->typekey;
				$data = RedisL4::connection()->ZINCRBY($redis_key, 1, $storypictureID);
	
				$dbo = StoryPicture::find($storypictureID);
				$dbo->increment("likes");
	
				$successbag[] = (Auth::User()->getKey() . " Added on " . $storypictureID);
				$this->registerOperations($userID);
			//}else{
			//	$failbag[] = (Auth::User()->getKey() . " Is Not storypicture creator on " . $storypictureID);
			//}
		}
	
		return Response::json(['message' => 'Complete', 'success'=>$successbag, 'fails'=>$failbag], 200);
	
	
	}
	
	function delByUser($userID='me'){
		//dd("DEL-BY-USER");
		
		if(null==Auth::User()){
			return Response::json(array('message' => 'Not Logged In'),    403);
		}

		$this->user = $this->userRepo->discover($userID);
		if(null==$this->user){
			return Response::json(array('message' => 'Bad User'),    403);
		}
		
		$userID = $this->user->_id;		
		
		$this->fill_allStorypictureID();
	
		$failbag = array();
		$successbag = array();
	
		//dd($this->allStorypictureID);
		
		foreach($this->allStorypictureID as $storypictureID){
				
			$redis_key = "user:" .  $userID . ":" . $this->storypicturekey;
			$data = RedisL4::connection()->ZSCORE($redis_key,$storypictureID);
			if(null == $data &&  $this->storypicturekey == "storypicturelike" ){
				//echo("Breakin\n");
				$failbag[] = (Auth::User()->getKey() . "  is not a record on " . $storypictureID);
				break;
			}
				
			//echo("storypictureID: " .  $storypictureID . "\n");
			$redis_key = "user:" .  $userID . ":" . $this->storypicturekey;
			$data = RedisL4::connection()->ZREM($redis_key, $storypictureID);
				
			$redis_key = "storypicture:" .  $storypictureID . ":" . $this->userkey;
			$data = RedisL4::connection()->ZREM($redis_key, $userID);
	
			$redis_key = "storypicture:" . $this->typekey;
			$data = RedisL4::connection()->ZINCRBY($redis_key, -1, $storypictureID);
	
			$dbo = StoryPicture::find($storypictureID);
			if(null != $dbo){
				$dbo->decrement("likes");
			}
			$successbag[] = (Auth::User()->getKey() . " Deleted from " . $storypictureID);
		}
	
		//dd("finished loop");
		return Response::json(['message' => 'Complete', 'success'=>$successbag, 'fails'=>$failbag], 200);
	}
	
	function getByUser($userID){
	
		if(null==Input::get("storypictureID")){
				
			return $this->getAllStorypicturesByUserID($userID);
		}else{
				
			$this->fill_allStoryID();
				
				
			$returnresult=array();
			$allreturnresult = array();
				
			$allreturnresult['user_id'] = User::find($userID)->toArray();
				
			foreach($this->allStorypictureID as $storypictureID){
				$redis_key = "user:" .  $userID . ":" . $this->storypicturekey;
				$data = RedisL4::connection()->ZSCORE($redis_key, $storypictureID);
				if($data>0){
					$returnresult[] = array(StoryPicture::find($storypictureID), $data);
				}else{
					$returnresult[] = array($storypictureID, $data);
				}
			}
			$allreturnresult['data']=$returnresult;
			return Response::json($allreturnresult, 200);
		}
	}
	
	function getAllStorypicturesByUserID($userID){
		//get a list of stories which this user is specifically allowed to see
		$s = StoryPicture::all();
		$returnresult=array();
	
		$allreturnresult['user_id'] = User::find($userID)->toArray();
		$redis_key = "user:" .  $userID . ":" . $this->storypicturekey;
		$data = RedisL4::connection()->ZRANGEBYSCORE($redis_key, -INF , +INF);
	
		$returndata=array();
	
		foreach($data as $dataitem){
			$returndata[] = StoryPicture::find($dataitem)->toArray();
		}
		$allreturnresult['data']=$returndata;
		return Response::json($allreturnresult, 200);
	}
	
	
	
	
	
	
	function registerOperations($userID){

		Event::fire('storypicture.liked', array($this->storypicture, $userID));
	}
	
}