<?php
use CommonCore\Users\User;

class UsersFeedController extends \BaseController{

	/**
	 * User repository
	 *
	 * @var CommonCore\Users\UserRepositoryInterface
	 */
	protected $userRepo;
	
	/**
	 * 	The user logged in by common core
	 */
	protected $user = null;
	
	/**
	 * 	A list of things to append to the URL at pagination
	 */
	protected $urlAppend;
	
	/**
	 * 	Default items-per-page at pagination time
	 */
	protected $pageCount=50;

	/**
	 * 	A general query which should be acceptable to ->get() on.
	 */
	protected $query=null;


	/**
	 * 	Array of strings, each representing a DB 'column' which is specifically
	 *  required.  Unpopluated or null returns all items.
	 */
	protected $resultsMask = null;
	
	
	
	public function getAllUsers(){

		$this->resultsMask = array('username', 'profile_image', 'first_name', 'last_name', 'privacy');
		
		if(null==Auth::User()){
			return Response::json(array('message' => 'Not Logged In. You must be logged in to search.'),    403);
		}		
		
		$this->query = User::orderBy('username');
		
		/** 10/22.  Make all users, including pivate ones, available. */
		//$this->query->where('privacy', '!=', 'private');
		
		$results = $this->makeReturnObject();
		
		return Response::json($results , 200);
		
	}
	
	public function getAllUsersByUsername(){
		
		$this->resultsMask = array('_id', 'username', 'profile_image');
		
		if(null != Input::get("username")){
			$username = Input::get("username");
		}else{
			return Response::json(array('message' => 'No User ID supplied'),    403);
		}
		
		if(null==Auth::User()){
			return Response::json(array('message' => 'Not Logged In'),    403);
		}		
		
		//the application can supply some part of a username, not only a whole username.
		$this->query = User::where('username' , 'like' , '%' . $username . '%');
		
		/** 10/22.  Make all users, including pivate ones, available. */
		//$this->query->where('privacy', '!=', 'private');
		
		$results = $this->makeReturnObject();
		
		return Response::json($results , 200);
		
	}
	
	public function getAllUsersByRealname(){
		
		$this->resultsMask = array('_id', 'username', 'profile_image', 'first_name', 'last_name');
		
		if(null != Input::get("username")){
			$username = Input::get("username");
		}else{
			return Response::json(array('message' => 'No User ID supplied'),    403);
		}
		
		if(null==Auth::User()){
			return Response::json(array('message' => 'Not Logged In'),    403);
		}		
		
		//the application can supply some part of a username, not only a whole username.
		$this->query = User::where('first_name' , 'like' , '%' . $username . '%');
		$this->query->orWhere('last_name' , 'like' , '%' . $username . '%');
		$this->query->orWhere('username' , 'like' , '%' . $username . '%');
		
		/** 10/22.  Make all users, including pivate ones, available. */
		//$this->query->where('privacy', '!=', 'private');
		
		/*
		 * This approach may be overkill, in which case remove before production.
		 * Keep around for now in case needed.11
			foreach($arrKeywords as $key=>$val){
			$this->query->where(
					function($qry) use ($val){
						$this->query->orWhere('title', 'like' , "%".$val."%");
					}
			)->orWhere(
					function($qry) use ($val){
						$this->query->orWhere('description', 'like' , "%".$val."%");
					}
			);
		}
		*/
		$results = $this->makeReturnObject();
		return Response::json($results , 200);
		
	}
	
	private function makeReturnObject(){

		$loggedinUserID = Auth::User()->getKey();
		$this->query->where('_id', '!=' , $loggedinUserID );
		
		//Per page?
		$perpage = intval(Input::get('count',0)) > 0 ? intval(Input::get('count')) : $this->pageCount;
		$this->query->take($perpage);
			
		// Page num?
		$page = intval(Input::get('page',0));
		$this->query->skip($perpage * $page);
	
		
		$results = $this->query->get($this->resultsMask);
		
		$preResults = $results->toArray();
		$postResults = $this->resultsReorder($preResults);
		
			$pagination = array(
				'pagination' => [
					'requested' => [
						'page' => $page,
						'count' => $perpage
					],
					'page' => $page,
					'navigation' => [
						'previous' => $page <= 0 ? null : URL::current().'?count='.$perpage.'&page='.($page-1) . $this->urlAppend,
						'next' => $results->count() >= $perpage ? URL::current().'?count='.$perpage.'&page='.($page+1). $this->urlAppend : null
					]
				]
		);

			
		$returnResults["pagination"] = $pagination;
		$returnResults["users"] = $postResults;
		
		
		
		return($returnResults);
	}
	
	protected function resultsReorder($users){

		$returnResults = array();
		
		foreach($users as $user){
			
			$redis_key = "user:" .  Auth::User()->getKey() . ":" . "following";
			$doTheyFollowMe = RedisL4::connection()->SISMEMBER($redis_key, $user["_id"]);
			
			if(null == $doTheyFollowMe){
				$user["following"] = "false";
			}else{
				$user["following"] = "true";
			}
				
			$returnResults[]=$user;
			
		}
		
		return $returnResults;
	}	
	
	
}