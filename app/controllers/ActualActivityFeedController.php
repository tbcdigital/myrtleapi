<?php




/**

Makes and returns the Home Feed a.k.a Activity Feed a.k.a Main Feed.

This consists of a paginated list of the 'most relevant' stories at any time for a certain user, with the twenty
most recent StoryPictures for each Story.
 
The definition of 'most relavent' is quite complicated and is discussed in detail here:

1. Make a redis entry where key is user:MyUserID:activityfeed of type SortedSet
 
This has many entries, each of the type
someEventID _ someUserID			->			the most-recent timestamp of any of the StoryPhotos belonging to this eventID / userID combo.

2. The entries are drawn from the following:

i.	users I follow: their Public events - DEPRC (this is subsumed as a subset of all Public events)

ii. all the users/events in the world, for whom I have explicit event access

HOW TO GET THIS?
ii.i. Get the list of events which I have explicit access to (redis key of type: user:MyUserID:storyaccess)
ii.ii.  From THIS list, get a further list of all the users who can access this event:
foreach (user:MyUserID:storyaccess AS eventID)
	story:eventID:useraccess (gives 1 or Many userID's)
giving a long list of eventID - userID pairs which probably comtains duplicates


iii. Public Events which a Person I follow has done something to.  UNLESS this user is PRIVATE,
	 in which case, the condition of MUTUAL FOLLOWING must be fulfilled to allow access.
	 (note that half of the Mutual Following condition is inferred by the fact that I am Following 
	 the Person)

HOW TO GET THIS?
iii.i.  Get a list of all the StoryPictures where the event_id defines a public event
iii.ii	Restrict this list to StoryPictures where the user_id is someone I follow.
iii.ii  Remove from this list all StoryPictures where the user_id identifies a user whose privacy
		is set to private; UNLESS this user is also following me.  This is the MUTUAL FOLLOWING
		condition.
iii.iv  We now have another list of eventID - userID pairs.

iii.v.  We hope to have at least ($this->count) Storypictures from these 2 scenarios.
		In the event that we do not, we pull enough random storypictures for this event (whose users are 
		not private) to make up he amount.  If there are still not enough, the client should show a blank image.

iii.vi	One storypicture can be nominated to be the Cover image.  If there is none, the highest placed
		image must serve as the Cover image.

iv. append (ii) and (iii). Deduplicate this list.

3. For each userID / eventID combo, get all the StoryPictures (0, 1 or many) from Mongo; pick out the
most recent Timestamp.
 
4. Store this in Redis as a value (eventID_userID -> most-recent Timestamp)

5. Do a RevRangeByScore to get all the now-unique eventID_userID pairs in order of most-recent activity,
most recent first.  Note that a very old event with a very recent picture will come ahead of a newer event with a
less-recent picture.  create_date of event is not a consideration in this list.
 
6. Make this list available for pagination.  Redraw the list any time page=0 is hit.

7. For any given PAGE, look up all the StoryPictures by the userID and eventID list for that page only.  This means that
the Redis store will only be regenerated upon a reasonable condition (hitting the first page (page 0) ), and will only do (COUNT)
Mongo queries for any page.
 
8. Any given userID / eventID combination may refer to many StoryPictures.  How many to add to the Activity list?  For now, 20.
Note that the API will try to find (count) images from three sources.

*/


use CommonCore\Users\User;
use Carbon\Carbon;


class ActualActivityFeedController extends \BaseController{

	/**
	 * User repository
	 *
	 * @var CommonCore\Users\UserRepositoryInterface
	 */
	protected $userRepo;

	/**
	 * 	The user logged in by common core
	 */
	protected $user = null;

	/**
	 * 	A list of things to append to the URL at pagination
	 */
	protected $urlAppend;

	/**
	 * 	Default items-per-page at pagination time
	 */
	protected $pageCount=20	;

	/**
	 * 	Default storypictures-per-activity at pagination time
	 */
	protected $storyPicturesCount=20  ;

	/**
	 * 	A general query which should be acceptable to ->get() on.
	 */
	protected $query=null;

	/**
	 * 	String to hold the name of the assoc. array which contains the main data
	 *  requested by $query
	 */
	protected $dataArrayName='Feed';

	/**
	 * 	Array of strings, each representing a DB 'column' which is specifically
	 *  required.  Unpopluated or null returns all items.
	 */
	protected $resultsMask = array();

	/**
	 * 	count of Who Follows Me
	 */
	 protected $followingCount = null;
	
	/** 
	*  count of Who I am Following
	*/
	 protected $followerCount = null;
	 
	/**
	*  count of How many Private Items
	*/
	protected $howManyPrivateItems = null;

	/**
	 * Array of all users we are interested in,
	 */
	protected $allUsers=array();

	/**
	 * Array of all user ID's we are interested in,
	*/
	protected $allUserIDs=array();

	/**
	 * Array of all events we want to see,
	*/
	protected $allEvents=array();

	protected $reorderOrNot=false;
	
	protected $coverPictureObject = null;
	
	
	public function __construct(CommonCore\Users\UserRepositoryInterface $userRepo)
	{
		$this->userRepo = $userRepo;
	}

	
	
	public function activityFeed($userID='me'){
		
		
		$this->user = $this->userRepo->discover($userID);
		if(null==$this->user){
			return Response::json(array('message' => 'Bad User'),    403);
		}
		
		if(null==Auth::User()){
			return Response::json(array('message' => 'Not Logged In'),    403);
		}

		/**
		5. Do a RevRangeByScore to get all the now-unique eventID_userID pairs in order of most-recent activity,
		most recent first.  Note that a very old event with a very recent picture will come ahead of a newer event with a
		less-recent picture.  create_date of event is not a consideration in this list.
		 
		6. Make this list available for pagination.  Redraw the list any time page=0 is hit.
		(See: Point 1 in activityFeedRegen)
		*/

		$perpage = intval(Input::get('count',0)) > 0 ? intval(Input::get('count')) : $this->pageCount;

		// Page num?
		$page = intval(Input::get('page',0));
		
		if($page==0){
			$this->activityFeedRegen();
		}
			
		/**
		7. For any given PAGE, look up all the StoryPictures by the userID and eventID list for that page only.  This means that
		the Redis store will only be regenerated upon a reasonable condition (hitting the first page), and will only do (COUNT)
		Mongo queries for any page.
		
		This request is now implented at pagination time in makeReturnObject.  Here, we concentrate on making a bulk query.
		*/
	
		
		$redis_key_get = "story_user_mostrecent:user:" . $this->user->getKey();
		
		$countOfAll = count(RedisL4::connection()->zRevRangeByScore($redis_key_get, INF, -INF));
		
		
		
		$selectBasisData =  RedisL4::connection()->zRevRangeByScore($redis_key_get, INF, -INF, [ 'withscores' => false, 'limit' => [($page*$perpage),$perpage]]);
		
		//dd( $selectBasisData );
		
		$allObject = array();
		
		/** Do our best to get 20 pictures from the specific StoryID / UserID combination */
		
		foreach($selectBasisData as $selectBasisDatum){
			
			//var_dump($selectBasisDatum);
			
			$storyAndUser = explode( "_" , $selectBasisDatum );
			$storyID = $storyAndUser[0];
			$userID  = $storyAndUser[1];			
			
			$storypicturesByStoryAndUserAndCoverImage=array();
			
			/**
			 8a. Try to find a cover picture.
			 */			
			$this->query = StoryPicture::where('iscoverpicture', '=', true)->where('story_id', '=', $storyID);
			$coverPictureObject = $this->query->take(1)->get();
			$coverPictureArray = $coverPictureObject->toArray();
			$this->coverPictureObject = $coverPictureObject;
			
			if( !empty($coverPictureArray) ){
				$localcount = $this->storyPicturesCount - 1;
			}else{
				$localcount = $this->storyPicturesCount;
			}

			
			$oneObject = array();
			$this->query = StoryPicture::orderBy('created_at');

			$lstory = $this->enhancedStoryData(Story::find($storyID));
			$luser  = User::find($userID);

			$privacy = $luser->privacy;
			if( $privacy != "private" ){
				//almost certainly allow this storypicture to be shown.  But leave in this
				//condition for future contingencies.
				
			}else{
				
				//this statement can be excused if the (user) in the event_user key and
				//the logged in user follow each other.  We check Redis to find out.
				
				//the ID of the user in this event_user
				// $userID
				
				//the ID of the logged-in user
				$meID = Auth::User()->getKey();
				
				$redisKey1 = "user:" . $userID . ":followedby";
				$redisKey2 = "user:" . $meID . ":following";
				
				$data1 = RedisL4::connection()->SISMEMBER( $redisKey1 , $meID );
				$data2 = RedisL4::connection()->SISMEMBER( $redisKey2 , $userID );
					
				//echo "RS 1: " .  $redisKey1 . "---USER---"  . $meID . "\n";
				//echo "RS 2: " .  $redisKey2 . "---USER---"  . $userID . "\n";
				//echo "\n data1:" . $data1 . "  data2:" . $data2 . "\n\n";
				
				//  BOTH data1 and data2 must return 1 in order for the break condition to be excused.
				//  If either are 0, the rest of the loop cycle is skipped, and the next event_user 
				//  object is considered.
				if ( $data1==0 || $data2==0 ){	
					
					//BUT, if the logged in user IS the (user) in the event_user
					//we may also excuse the continue, because I am always allowed to see my own stuff....
					//see Condition (iv) below, all about how I can view My Own stuff, whether public or private....
					if($userID != $meID){
						continue;
					}
				}
			}
			
			$this->query->orWhere(
        			function($qry) use ($storyID, $userID){
        				/**
        				 8b. Any given userID / eventID combination may refer to many StoryPictures.  How many to add to the Activity list?  For now, 20.
        				*/
        				$qry->where('story_id', '=', $storyID)->where('user_id', '=', $userID)->where('iscoverpicture', '!=', true);
        				
        				
        			}
			);

			$storypicturesByStoryAndUser = $this->query->take($this->storyPicturesCount)->get()->toArray();
			
			/** For convenience, let's make a side array of all the ID's of the items in $storypicturesByStoryAndUser.  
			 *  We will need it to deduplicate the array of storypictures-selected-by-story-only
			 *  */
			$storypicturesByStoryAndUser_id = array();
			
			//dd($storypicturesByStoryAndUser_id);
			
			foreach($storypicturesByStoryAndUser as $storypictureByStoryAndUser){
				$storypicturesByStoryAndUser_id[] = $storypictureByStoryAndUser['_id'];
			}
			
			/** If this result doesn't give us  [$this->storyPicturesCount]'s worth of storypictures, we 
				have to run another, cruder query using only story_id
			 */
			if( count($storypicturesByStoryAndUser) < $this->storyPicturesCount){
				$this->query = StoryPicture::orderBy('created_at', 'DESC');
				$this->query->orWhere(
						function($qry) use ($storyID, $userID){
							/**
							 8b. Consider only Story id.  How many to add to the Activity list?  For now, default - (number collected from (8) ).
							 */
							$qry->where('story_id', '=', $storyID)->where('iscoverpicture', '!=', true);
						}
				);
				$subLimit = $localcount - count($storypicturesByStoryAndUser) ;
				$storypicturesByStory = $this->query->take($subLimit)->get()->toArray();
				
				/** Make another ID array. */
				$storypicturesByStory_id = array();
				foreach($storypicturesByStory as $storypictureByStory){
					$storypicturesByStory_id[] = $storypictureByStory['_id'];
				}
				
				/** array_diff this new ID array against $storypicturesByStoryAndUser_id to get
				 *  a list of ID's of storypictures which are not found in the list of $storypicturesByStoryAndUser_id.
				 */
				$storypicturesByStory_id = array_diff($storypicturesByStory_id , $storypicturesByStoryAndUser_id);
				
				
				/** loop through $storypicturesByStory again, compiling a new list $storypicturesByStory_final
				 *  consisting only of the items whose ID's appear in $storypicturesByStory_id
				 *  */
				$storypicturesByStory_final = array();

				foreach ( $storypicturesByStory as $storypictureByStory ){
					
					if ( true == in_array($storypictureByStory['_id'] , $storypicturesByStory_id ) ){
						if( isset($coverPictureArray[0]) ){
							if( $coverPictureArray[0]['url'] == $storypictureByStory['url']){
								//  THIS storypicture is already set to be the cover image.  
								//  we don't want it showing up in the Feed.
								//  Do not discard this case, because in the future
								//  we may have to do something with the cover picture.
							}else{
								$storypicturesByStory_final[] = $storypictureByStory;
							}
						}else{
							$storypicturesByStory_final[] = $storypictureByStory;
						}
					}
				}
				
				$storypicturesByStoryAndUser = array_merge( $storypicturesByStoryAndUser , $storypicturesByStory_final );
				
			}	
			
			/** If we find a cover picture for this story, make sure it goes to the front of the pile. */
			if( !empty($coverPictureArray) ){
				$storypicturesByStoryAndUser = array_merge($coverPictureArray , $storypicturesByStoryAndUser);
			}			
			
			$oneObject['user']=$luser;
			$oneObject['story']=$lstory;
			$oneObject['storypictures']=$storypicturesByStoryAndUser;
				
			$allObject[]=$oneObject;
						
		}
		
		//dd("ENDED");
		
		$pagination = array(

				'requested' => [
				'page' => $page,
				'count' => $perpage
				],
				'page' => $page,
				'navigation' => [
				'previous' => $page <= 0 ? null : URL::current().'?count='.$perpage.'&page='.($page-1) . $this->urlAppend,
				'next' => $countOfAll-1 >= $perpage*($page+1) ? URL::current().'?count='.$perpage.'&page='.($page+1). $this->urlAppend : null
				]
	
		);
			
		$returnResults = array();
		
		$returnResults['user'] = $this->user;
		
		$returnResults['pagination'] = $pagination;
		
		$returnResults['activities'] = $allObject;
		
		return Response::json($returnResults , 200);
		
	}
	
	
	public function activityFeedRegen(){
		
	/**

	 1. Make a redis entry where key is user:MyUserID:activityfeed of type SortedSet
	 	
	 This has many entries, each of the type
	 someEventID _ someUserID			->			the most-recent timestamp of any of the StoryPhotos belonging to this eventID / userID combo.
	 
	 2. The entries are drawn from the following:
	 
	 i.	users I follow: their Public events - DEPRC.
	 
	 ii. all the users/events in the world, for whom I have explicit event access
	 
	 iii. 
	 
	 HOW TO GET THIS?
	 	ii.i. Get the list of events which I have explicit access to (redis key of type: user:MyUserID:storyaccess)
	 	ii.ii.  From THIS list, get a further list of all the users who can access this event:
	 			foreach (user:MyUserID:storyaccess AS eventID)
	 				story:eventID:useraccess (gives 1 or Many userID's)
	 				giving a long list of eventID - userID pairs which probably comtains duplicates

 	*/		

		/** get a list of the events which I have explicit access to */
		$redis_key_myaccess = "user:" .  $this->user->getKey()  . ":storyaccess";
		$myaccess = RedisL4::connection()->SMEMBERS($redis_key_myaccess);
		
		
		$arrEventIDuserID = array();
		
		foreach($myaccess as $oneStory){
			$redis_key_onestory = "story:" .  $oneStory  . ":useraccess";
			$userlist = RedisL4::connection()->SMEMBERS($redis_key_onestory);
			foreach($userlist as $oneUser){
				$arrEventIDuserID[] = $oneStory . "_" . $oneUser ;
			}
		}

		$arrEventIDuserID = array_unique($arrEventIDuserID);
		
		
	/**
 
	 iii. Public Events which a Person I follow has done something to
	 
	 HOW TO GET THIS?
	 	iii.i.  Get a list of all the StoryPictures where the event_id defines a public event
	 	iii.ii	Restrict this list to StoryPictures where the user_id is someone I follow.
	 	iii.iii We now have another list of eventID - userID pairs. 
	*/		

		/**
		 * 		CHANGE, 11/03 PM.
		 * 		Make it so your own public and private events appear in your home feed
		 *		Make it that if you update someone elses album that update isn't shown on your home feed
		 *
		 *
		 *		WHAT DOES THIS MEAN?
		 *		Update:
		 *		iii.ii	Restrict this list to StoryPictures where the user_id is someone I follow.
		 *		To:
		 *		iii.ii	Restrict this list to StoryPictures where the user_id is ME.
		 *
		 * */
		
		
		/** if the spec of 10/14 is continued, use this definition */
		/** under this rule, it is PEOPLE I FOLLOW.  Uncomment the next 2 lines.*/
		$redis_key_myaccess = "user:" .  $this->user->getKey()  . ":following";
		$arrPeopleIfollow = RedisL4::connection()->SMEMBERS($redis_key_myaccess);		

		/** if the change of 11/03pm is executed, use this def'n of the list */
		/** under this rule, it is JUST ME.  Uncomment the next 1 line.*/
		$arrPeopleIfollow[]=$this->user->getKey();
				
		//dd("about to run");
		
		$query = Story::whereIn("user_id" , $arrPeopleIfollow);
		$query->where("privacy" , "=" , "public");
		$arrPublicEvents = $query->get();
		
		$s_uKey=array();
		$arrPublicEvents->each(function($arrPublicEvent) use(&$arrEventIDuserID) {
			
			$arrEventIDuserID[] = $arrPublicEvent->getKey() . "_" . $arrPublicEvent->user_id;
		});

		
		
		/*
			iv. All of My Events, reagrdless of whether public or private.
			NOTE. Requested 2014/12/28 - ish.  Appears to overturn a previous decision.
			
			We now add another bunch of event_user pairs for all of My events, nomatter whether they are public or private.
		 */
		$query = Story::where("user_id" , $this->user->getKey());
		$myEvents = $query->get();
		
		//dd( $myEvents->toArray() );
		$myEvents->each(function($myEvent) use(&$arrEventIDuserID){
			//var_dump($myEvent->getKey() . "_" . $myEvent->user_id);
			$arrEventIDuserID[] = $myEvent->getKey() . "_" . $myEvent->user_id;
		} );
		//dd($arrEventIDuserID);
		
		
	/**
 	 3. For each userID / eventID combo, get all the StoryPictures (0, 1 or many) from Mongo; pick out the
	 	most recent Timestamp.
	 	
	 4. Store this in Redis as a value (eventID_userID -> most-recent Timestamp)
	 
	 5. Do a RevRangeByScore to get all the now-unique eventID_userID pairs in order of most-recent activity,
	    most recent first.  Note that a very old event with a very recent picture will come ahead of a newer event with a 
	    less-recent picture.  create_date of event is not a consideration in this list.
 	*/		
		
		//destroy any previou versions of this Redis key as we are about to regenerate.
		$redis_key_kill = "story_user_mostrecent:user:" . $this->user->getKey();
		RedisL4::connection()->ZREMRANGEBYSCORE($redis_key_kill, -INF, +INF);
		
		foreach($arrEventIDuserID as $eventIDuserID){

			$lookForKanye = 0;
			
			$storyAndUser = explode("_" , $eventIDuserID);
			$query = StoryPicture::where("story_id", "=", $storyAndUser[0]);
			$query->where("user_id", "=", $storyAndUser[1]);
			
						
			/** I don't want to see things for which I am the user */
			/** BIG BIG NOTE TO SELF.  Mark this as the "Do I See Myself" issue.
			 *  We may have to wrap this line in some conditions.
			 *  Right now, the rule is that 'I' am excluded from 'my' feed.
			 *  BUT.
			 *  This may change if I am the story owner; f.i. I might want to see ALL stuff which is on a feed I own.
			 *		But right now (11/03), seems to be acceptable.
			 *
			 *		11/03 PM.  Reverse change from earlier.  See note on CHANGE, 11/03 PM.
			 *		11/17 PM.  Seems like this next line will remain commented.  Consider deletion before prod.
			 */		
			//$query->where("user_id", "!=", Auth::User()->getKey() );

			
			$query->orderBy('created_at');
			$query->take(1);
			
			$mostRecentAll = $query->get();
			
			
			
			if(count($mostRecentAll)>0){
				$mostRecent = $query->get()->first();
				/*
				if($eventIDuserID == "54b3d9d239a23cb25b3b1c10_54afa58639a23c1d34aeb50e"){
					var_dump("HERE AGAIN!!");
					//$lookForKanye=1;
					var_dump($mostRecent->toArray());
				}	
				*/				
				//$redis_key = "story_user:" . $eventIDuserID . ":storypicture:mostrecent" ;
				$redis_key = "story_user_mostrecent:user:" . $this->user->getKey();
				$timestamp = $mostRecent->created_at->timestamp;
				$storypicture = $mostRecent->getKey();
				$rez = RedisL4::connection()->ZADD($redis_key, $timestamp, $eventIDuserID);
				/*
				if($eventIDuserID == "54b3d9d239a23cb25b3b1c10_54afa58639a23c1d34aeb50e"){
					var_dump( $redis_key  );
				}
				*/
			}
		}
		
		return;
	}
	
	
	protected function resultsReorder($results){
	
		$returnResults = array();
	
		foreach($results as $result){

			
			$returnResults[]=$result;
		}
	
		return $returnResults;
	}	
	
	protected function enhancedStoryData($result){
		
		if(array_key_exists ( "likes" , $result )){
			if($result["likes"] < 0){
				$result["likes"]=0;
			}
		}else{
			//$result["likes"]=0;
		}
		
		$commCount = Comment::where("story_id", "=", $result["_id"])->where("type", "=", 'story')->count();
		
		if(null != $commCount){
			$result["comments"]=$commCount;
		}else{
			$result["comments"]=0;
		}
		
		$redis_key = "story:" . $result["_id"] . ":userviews";
		$accessdata = RedisL4::connection()->zRangeByScore($redis_key, -INF, +INF, [ 'withscores' => true ]);
		if(count($accessdata) > 0){
			if(null != $accessdata[0][1]){
				$result["viewcount"] = intval($accessdata[0][1]);
			}else{
				$result["viewcount"] = 0;
			}
		}else{
			$result["viewcount"] = 0;
		}	
		
		$redis_key = "user:" .  Auth::User()->getKey() . ":" . "storylike";
		$likeIsThere = RedisL4::connection()->ZSCORE($redis_key, $result['_id']);
			
		if(null == $likeIsThere){
			$result["doIlikethis"] = "false";
		}else{
			$result["doIlikethis"] = "true";
		}		
		
		
		return $result;
		
	}
}