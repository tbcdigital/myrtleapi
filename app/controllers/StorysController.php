<?php
use CommonCore\Users\User;
/**
 * Handle creation / viewing / deletion of STORYs.. 
*/
class StorysController extends \BaseController{

	/**
	 * User repository
	 *
	 * @var CommonCore\Users\UserRepositoryInterface
	 */
	protected $userRepo;	

	/**
	 * 	Suffix for the Redis Key User type
	 */
	protected $userkey = "useraccess";
	
	/**
	 * 	Suffix for the Redis Key Story type
	 */
	protected $storykey = "storyaccess";	

	
	/**
	 * 	Array for user data accessed from inside closures.
	 */	
	protected $usersInfo=array();


	public function __construct(CommonCore\Users\UserRepositoryInterface $userRepo)
	{
		$this->userRepo = $userRepo;
	}	
	
    /**
     * Handle creation of one STORY in Mongo.
     * 
     *  @param:  array (title, description, Optional: location [lat, long], privacy) 
     *  @return: string completed_status_message
     */
    function newStory(){
    	
    	if(null==Auth::User()){
    		return Response::json(array('message' => 'Not Logged In'),    403);
    	}   


    	$s = new Story(  array_merge( Input::all() )  );
    	
    	$s->user_id = Auth::User()->getKey() ;
    	
    	$s->save();    	
    	
        if( false === $s->exists ) {
        	return Response::json(['errors' => $s->errors()->toArray(), 'message' => 'Item failed creation'], 400);
        }
        

        
        //return Response::json(array('message' => 'Completed Normally: ID ' . $s->getKey()), 200);        
        return Response::json(array('message' => $s->getKey()), 200);        
    }
    
    /**
     * Get information for one or more EVENTS a.k.a STORYS in Mongo.
     * 
     *  @param: string $storyID
     *  @return: array (zero, one or many STORY objects in JSON)
     */    
    function getStory($storyID=null){

    	
    	if(null==Auth::User()){
    		return Response::json(array('message' => 'Not Logged In'),    403);
    	}
    	    		    	
    	if(empty($storyID)){
            //return basic information for all items which are created by the logged-in user.
            //This is a candidate for deprecation because its function should be subsumed by
            // search -> OnlyCreated, search -> OnlyPublic
            return  Response::json(['data' => Story::where("user_id", "=", Auth::User()->_id)->get()->toArray()], 200 );
            
        }else{
        	/*Only return results which user is 'allowed' to see, or which have Public access*/
        	$story = Story::find($storyID);
        	
        	if(null == $story){
        		return Response::json(array('message' => 'not a valid Story ID'),    403);
        	}
        	
        	if($story->privacy == 'private'){
        		if($story->user_id != Auth::User()->_id){
	        		$redis_key = "story:" .  $storyID . ":" . "useraccess";
	        		$data = RedisL4::connection()->SISMEMBER($redis_key, Auth::User()->_id);
	        		
	        		if($data != true){
	        			return Response::json(array('message' => 'You do not have access to this story'),    403);
	        		}
	
	        	}
        	}
			//  If we got this far, the story is Public, or Private and User has Permission to View. 
			//  We'll log a view for this user.
        	$redis_key = "story:views";
        	$accesscount = RedisL4::connection()->zIncrby($redis_key, 1, $storyID);
        	$redis_key = "story:" . $storyID . ":userviews";
        	$accesscount = RedisL4::connection()->zIncrby($redis_key, 1, Auth::User()->_id);
        	
        	
        	return Response::json(['data' => $story->toArray()], 200 );
        }    	
    }
    
    /**
     * Get information for one or more EVENTS a.k.a STORYS in Mongo, given a user_id.
     * 
     *  @param: string $creatorID
     *  @return: array (zero, one or many STORY objects in JSON)
     */    
    function getStoryByCreator($creatorID=null){

    	if(null==Auth::User()){
    		return Response::json(array('message' => 'Not Logged In'),    403);
    	}    	
    	
    	$str = Story::where("user_id", "=" ,$creatorID)->get();
    	
    	if($str->count()==0){
    		return Response::json(array('message' => 'No Records'),    400);
    	}
    	
       	return Response::json(['data' => $str->toArray()], 200 );
    }
    
    /**
     * update information for one  EVENT a.k.a STORY in Mongo.
     * 
     *  @param: string $storyID
     *  @param: array Optional:title, Optional:description, Optional:location [lat, long], Optional:privacy
     *  @return: string completed_status_message
     */
    function updateStory($storyID){

    	if(null==Auth::User()){
    		return Response::json(array('message' => 'Not Logged In'),    403);
    	}
    	
        $story=Story::find($storyID);
        
        if($story->privacy == 'private'){
        	if($story->user_id != Auth::User()->_id){
        		$redis_key = "story:" .  $storyID . ":" . "useraccess";
        		$data = RedisL4::connection()->SISMEMBER($redis_key, Auth::User()->_id);
        		 
        		if($data != true){
        			return Response::json(array('message' => 'You do not have access to this story'),    403);
        		}
        
        	}
        }    
            
        $story->fill(array_merge( Input::all() ));
        
        $rez = $story->save();
        
        if($rez==false){
        	return Response::json(array('message' => 'Update failed.'), 200);
        }
        
        return Response::json(array('message' => 'Completed Normally'), 200);
    }
    
    /**
     * delete one  EVENT a.k.a STORY in Mongo.
     * 
     *   @param: $storyID
     *   @return: string completed_status_message
     */    
    function deleteStory($storyID=null){

       	if(null==Auth::User()){
    		return Response::json(array('message' => 'Not Logged In'),    403);
    	}        

    	$story = Story::find($storyID);
    	  	
    	if(null===$story){
    		return Response::json(array('message' => '(' . $storyID . ') Does Not Exist'),    400);
    	}
	
    	$st = $story->user_id;
    	
    	if( $st !=  Auth::User()->getKey()){
    		return Response::json(array('message' => 'You are not the Story Creator'),    403);
    	}
    	
    	
        $s = Story::find($storyID);
    	if(null == $s){
    		return Response::json(['message' => 'No Record'], 400);
    	}
    	$s->delete();
    	
    	//$cc = new CommentsController();
    	//$cc->deleteCommentByStoryID($storyID);
    	
    	if( null == Story::find($storyID) ){
            return Response::json(['message' => 'Deletion Succeeded'], 200);
        }else{
            return Response::json(['message' => 'Deletion Failed'], 200);
        }
    }
    
    /**
     *   Return an object all about the access data for a certain object.
     *	 Obviously, data is only meaningful for (private) stories.	
     *
     *   @param: $storyID
     *   @return: object access data
     */    
    function getStoryAccessCount($storyID){

    	if(null==Auth::User()){
    		return Response::json(array('message' => 'Not Logged In'),    403);
    	}    	
    	
    	$story = Story::find($storyID);
    	
    	$type = $story->privacy;
    	if(!null == $type){
	    	if($type=='public'){
	    		return Response::json(array('message' => 'Story is Public.  Access is unlimited.'),    403);
	    	}
    	}
    	
    	$storyAccessData = array();
    	
    	$redis_key = "story:perms";
    	$accesscount = RedisL4::connection()->zScore($redis_key, $storyID);
    	
    	$storyAccessData['story'] = $story;
    	$storyAccessData['access_count'] = $accesscount;
    	
    	return Response::json(['storyAccessData' => $storyAccessData], 200 );
    }
    
    function getStoryAccessData($storyID){

    	//NOTE:
    	//story:storyID:userviews  -- How many times a story has been Accessed by User (Sorted Set)
    	//story:storyID:useraccess -- List of users who have been awarded access to a certain story.
    	
    	if(null==Auth::User()){
    		return Response::json(array('message' => 'Not Logged In'),    403);
    	}    	
    	
    	$story = Story::find($storyID);
    	
    	$storyAccessData = array();
    	
    	
    	$redis_key = "story:" . $storyID . ":userviews";
    	
    	$accessdata = RedisL4::connection()->zRangeByScore($redis_key, -INF, +INF, [ 'withscores' => true ]);
    	
    	//dd($accessdata);
    	
    	array_walk($accessdata, function($onedataitem){
    		//echo 	$onedataitem;
    		$oneUser = User::find($onedataitem[0])->toArray();
    		$oneUser['access_count']=$onedataitem[1];
    		array_push($this->usersInfo, $oneUser);
    	});
    	//dd($this->usersInfo);
    	$storyAccessData['story'] = $story;
    	$storyAccessData['users'] = $this->usersInfo;
    	
    	return Response::json(['storyAccessData' => $storyAccessData], 200 );
    }
    
    function userpop($accessdata){
    	$this->usersInfo[]=User::find($accessdata);
    }
    
}