<?php
use CommonCore\Users\User;
/**
 * Handle creation / viewing / deletion of STORYs.. 
*/
class StorysFeedController extends \BaseController {

	
	/**
	 * User repository
	 * 
	 * @var CommonCore\Users\UserRepositoryInterface
	 */
	protected $userRepo;
	
	/**
	 * 	The user logged in by common core
	 */
	protected $user = null;

	/**
	 * 	A list of things to append to the URL at pagination
	 */	
	protected $urlAppend;
	
	/**
	 * 	Default items-per-page at pagination time
	 */
	protected $pageCount=50;

	/**
	 * 	String to hold the name of the assoc. array which contains the main data
	 *  requested by $query
	 */
	protected $dataArrayName='stories';

	
	/**
	 * 	Array of strings, each representing a DB 'column' which is specifically
	 *  required.  Unpopluated or null returns all items.
	 */
	protected $resultsMask = array();
	
	/**
	 * 	An array of StoryID's somehow assembled accouding to user requests and permissions.
	 */
	protected $allStoryIDs=array();

	/**
	 * 	A general query which should be acceptable to ->get() on.
	 */	
	protected $query=null;
	
	public function __construct(CommonCore\Users\UserRepositoryInterface $userRepo) 
	{
		$this->userRepo = $userRepo;
	}

    /**
     * Get a list of events for user $userID.
     * If no URL params, return Events-Created-By-User + Events Accessible To User + Public events
     * If ?onlycreated=1, return only Events-Created-By-User
     *  @param:  (opt) userid
     *  @param:  (opt) ?onlycreated=1
     *  @param:  (opt) ?onlypublic=1
     *  @param:  (opt) ?keyword
     *  @param:	 (opt)?count [items per page]
     *  @param:  (opt)?page  [current page]
     *  @return: JSON object of all results
     */
  	function getAllEvents($userID='me'){
  		
		if(null==Auth::User()){
			return Response::json(array('message' => 'Not Logged In'),    403);
		}
		
		$this->user = $this->userRepo->discover($userID);
		if(null==$this->user){
			return Response::json(array('message' => 'Bad User'),    403);
		}

		if(null != Input::get("keywords")){
			$this->keywordsearch();
		}else{
			$this->refinedSearch();
		}
		
		$results = $this->makeReturnObject();
		return Response::json($results , 200);
	}
	/**
	 * 		Provides many diffferent results based on switches which can be set as part of the URL:
	 * 		onlycreated				=		
	 * 		onlypublic				=	
	 * 		onlycreatedandpublic	=	
	 * 		onlycreateandperms		=	
	 * 		(no args)				= explicit pemissions + created by Me + public items
	 */
	private function refinedSearch(){
		
		if(null != Input::get("onlycreated")){
			if(Input::get("onlycreated")==1){
				$this->urlAppend = "&onlycreated=1";
				$this->query = Story::where("user_id", "=", $this->user->id);
				$this->query->where("privacy", "=", "public");
			}else{
				//Stub for some other value of onlycreated
			}
		}elseif(null != Input::get("onlypublic")){
			
			if(Input::get("onlypublic")==1){
				$this->urlAppend = "&onlypublic=1";
				$this->query = Story::where("privacy", "=", "public");
			}else{
				//Stub for some other value of onlypublic
			}
				}elseif(null != Input::get("onlycreatedandpublic")){
			if(Input::get("onlycreatedandpublic")==1){
				//dd("onlycreatedandpublic");
				$this->urlAppend = "&onlycreatedandpublic=1";
				$this->query = Story::where("user_id", "=", $this->user->id);
				$this->query->Where("privacy", "=", "public");
			}else{
				//Stub for some other value of onlynonpublic
			}			
			
				}elseif(null != Input::get("onlycreateandperms")){
				
					if(Input::get("onlycreateandperms")==1){
						//dd("onlycreatedandpublic");
						$this->urlAppend = "&onlycreateandperms=1";
						$this->query = Story::whereIn("_id", RedisL4::connection()->SMEMBERS("user:" .  $this->user->id . ":" . "storyaccess"));
						$this->query->orWhere("user_id", "=", $this->user->id);
					}else{
						//Stub for some other value of onlynonpublic
					}
					
					
			
		}else{
			
			$this->query = Story::whereIn("_id", RedisL4::connection()->SMEMBERS("user:" .  $this->user->id . ":" . "storyaccess"));
			$this->query->orWhere("user_id", "=", $this->user->id);
			$this->query->orWhere("privacy", "=", "public");
		}
			
	}
	
	
	/**
	 * search by Keyword.  Keywords should be supplied as an minus-sign-separated string
	 * in the get('keywords') array.
	 * An array of many single-string keyword can also be supplied
	 * 
	 */
	private function keywordSearch(){
				
		if(null != Input::get("onlycreated")){
			if(Input::get("onlycreated")==1){
				$this->urlAppend = "&onlycreated=1";
				$this->query = Story::where("user_id", "=", $this->user->id);
			}else{
				//Stub for some other value of onlycreated
			}
		}elseif(null != Input::get("onlypublic")){
			
			if(Input::get("onlypublic")==1){
				$this->urlAppend = "&onlypublic=1";
				$this->query = Story::where("privacy", "=", "public");
			}else{
				//Stub for some other value of onlypublic
			}
		
		}else{
			$this->query = Story::whereIn("_id", RedisL4::connection()->SMEMBERS("user:" .  $this->user->id . ":" . "storyaccess"));
			$this->query->orWhere("user_id", "=", $this->user->id);
			$this->query->orWhere("privacy", "=", "public");
		}
		//end of common section with refinedSearch.
		
		$arrKeywords=array();
		
		if(!is_array(Input::get("keywords"))){
			//multi-string keywords are broken up with - marks in the application.
			//these are to be treated as wildcards during query compilation
			$arrKeywords[]=str_replace("-", "%", Input::get("keywords"));
		}else{
			//the application may also supply an array of many single-string keywords
			//which must be treated as individual search items 
			$arrKeywords=Input::get("keywords");
		}
		
		$this->query->where('title', 'like' , "%". Input::get("keywords") . "%");
		$this->query->orWhere('description', 'like' , "%" . Input::get("keywords") . "%");
		
		//two colsures inside a foreach, to reproduce a query of shape:
		// SELECT * from STORYS where (--the story is public, OR I have perms to view) AND (  (the title is like suchandsuch) OR (the description is like suchandsuch)  )
		/*
		foreach($arrKeywords as $key=>$val){
			$this->query->where(
					function($qry) use ($val){
						$this->query->where('title', 'like' , "%".$val."%");
					}
			)->orWhere(
					function($qry) use ($val){
						dd($val);
						$this->query->orWhere('description', 'like' , "%".$val."%");
					}
			);
		}
		*/
		
	}

	/**
	 *  Class-specific makeReturnObject method.
	 * 	does not accept any arguments.
	 * 	uses $this->query, which must be a querysuitable for having a get() run on it.
	 *  uses $this->pageCount to determine how many data objects per page are returned.
	 *  uses get('page',0) to determine which page is to be presented
	 *
	 *	uses a url property, order or order[] to set the ordering of the presented results.
	 *	Default order is title -> created_at -> likes(descending)
	 *
	 *  @returns an array consisting of:
	 *  	-a pagination object
	 *  	-a user object
	 *  	-an object containing many Data objects.  The type of the Data objects is dependent
	 *  	 on the query set as $this->query.
	 */	
	
	private function makeReturnObject(){

		//check to see if any ORDER values have been set.
		/*
		if(null != Input::get("order")){
			$arrOrderPrecidence=array();
				
			if(!is_array(Input::get("order"))){
				$arrOrderPrecidence[]=Input::get("order");
			}else{
				$arrOrderPrecidence=Input::get("order");
			}
			
			//be sure to include any orderable DB colums in this section.
			foreach($arrOrderPrecidence as $key=>$val){
				if(in_array($val, array('title'))){
					$this->query->orderBy($val, 'DESC');
				}elseif(in_array($val, array('created_date', 'likes'))){
					$this->query->orderBy($val, 'DESC');
				}
			}
		}else{
			
			$this->query->orderBy('title');
			$this->query->orderBy('created_at');
			$this->query->orderBy('likes', 'DESC');
			
		}
		*/		
		
		$this->query->orderBy('created_at', 'DESC');
		
		//Per page?
		$perpage = intval(Input::get('count',0)) > 0 ? intval(Input::get('count')) : $this->pageCount;
		$this->query->take($perpage);
		 
		// Page num?
		$page = intval(Input::get('page',0));
		$this->query->skip($perpage * $page);
		
		if($this->resultsMask!=null){
			$results = $this->query->get($this->resultsMask);
		}else{
			$results = $this->query->get();
		}
		
		
		$pagination = array(
				'pagination' => [
					'requested' => [
						'page' => $page,
						'count' => $perpage
					],
					'page' => $page,
					'navigation' => [
						'previous' => $page <= 0 ? null : URL::current().'?count='.$perpage.'&page='.($page-1) . $this->urlAppend,
						'next' => $results->count() >= $perpage ? URL::current().'?count='.$perpage.'&page='.($page+1). $this->urlAppend : null
					]
				]
		);
		 
		$returnResults = array();
		$returnResults['user'] = $this->user;
		$returnResults['pagination'] = $pagination;
		$finalResults = $results->toArray();
		
		$processedFinalResults = $this->resultsReorder($finalResults);
		
		$returnResults[$this->dataArrayName] = $processedFinalResults;
		
		return($returnResults);
	}	
	
	/**
	 * Very complete version of this method adding the following data to each paginated results-chunk:
	 * 	> permissions count
	 *  > likes count
	 *  > comments count
	 * @param unknown $results
	 * @return multitype:number
	 */
	protected function resultsReorder($results){
		
		$returnResults = array();
		
		foreach($results as $result){
			if(array_key_exists ( "permissions" , $result )){
				if($result["permissions"] < 0){
					$result["permissions"]=0;
				}
			}else{
				$result["permissions"]=0;
			}

			if(array_key_exists ( "likes" , $result )){
				if($result["likes"] < 0){
					$result["likes"]=0;
				}				
			}else{
				$result["likes"]=0;
			}
			//dd($result);
			$commCount = Comment::where("story_id", "=", $result["_id"])->count();
			if(null != $commCount){
				$result["comments"]=$commCount;
			}else{
				$result["comments"]=0;
			}
			
			$returnResults[]=$result;
		
		}
		
		return $returnResults;
	}
	
	
	public function usersWhoLikedThisStory($storyID){

		if(null==Auth::User()){
			return Response::json(array('message' => 'Not Logged In'),    403);
		}
		
		
		$this->dataArrayName = "users";
		
		$redisKey = "story:" . $storyID . ":userlike";
		
		$data = RedisL4::connection()->ZRANGEBYSCORE($redisKey , -INF, +INF) ;
		
		$this->query = User::whereIn( "_id" , $data );
		
		$results = $this->makeReturnObject();
		return Response::json($results , 200);
		
	}

    

}