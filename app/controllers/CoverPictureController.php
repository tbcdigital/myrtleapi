<?php



use CommonCore\Users\User;
use Carbon\Carbon;

class CoverPictureController extends \BaseStorysUsersController{

	

	/**
	 * User repository
	 *
	 * @var CommonCore\Users\UserRepositoryInterface
	 */
	protected $userRepo;
	
	
	public function __construct(CommonCore\Users\UserRepositoryInterface $userRepo)
	{
		$this->userRepo = $userRepo;
	}	
	
	
	/**
	 * Sets some storypicture to be the CoverPicture of the story to which it belongs.
	 * @param storypicture ID
	 * @return string of Cover Picture URL
	 */
	public function setCoverPicture($storyPictureID){

		if(null==Auth::User()){
			return Response::json(array('message' => 'Not Logged In'),    403);
		}
		
		
		$storypicture = StoryPicture::find($storyPictureID);
		
		$storypicture->iscoverpicture = true;
		
		$storypicture->save();
		
		$storyID = $storypicture->story->getKey();

		$localStory = Story::find($storyID);
		
		if( Auth::User()->getKey() !=  $localStory->user_id ){
			//return Response::json(array('message' => 'You are not the storycreator'),    403);
		}
		

		$localStory->coverpicture = $storypicture->url;
		$localStory->save();
				
		return Response::json(array('complete' => $storypicture->url ),    200);

	}



}