<?php
use CommonCore\Users\User;
/**
 * Handle Activity Feed, which nominally looks like this:
 *			 Activity {
 *				Activity 1 {
 *					User: (ID)
 *					Activity: (Commented, Liked, Created New Album)
 *					ActivityID: (Commented, Liked, Created New Album)
 *					}
 *				Activity 2 {
 *					User: (ID)
 *					Activity: (Commented, Liked, Created New Album)
 *					ActivityID: (Commented, Liked, Created New Album)
 *					}
 *			}
 *
 *	The basis is the People-I-Follow feed.
 *
 *
 */
class ActivityFeedController extends \BaseController{

	/**
	 * User repository
	 *
	 * @var CommonCore\Users\UserRepositoryInterface
	 */
	protected $userRepo;

	/**
	 * 	The user logged in by common core
	 */
	protected $user = null;

	/**
	 * 	A list of things to append to the URL at pagination
	 */
	protected $urlAppend;

	/**
	 * 	Default items-per-page at pagination time
	 */
	protected $pageCount=50;

	/**
	 * 	A general query which should be acceptable to ->get() on.
	 */
	protected $query=null;

	/**
	 * 	String to hold the name of the assoc. array which contains the main data
	 *  requested by $query
	 */
	protected $dataArrayName='';

	/**
	 * 	Array of strings, each representing a DB 'column' which is specifically
	 *  required.  Unpopluated or null returns all items.
	 */
	protected $resultsMask = array();

	/**
	 * 	count of Who Follows Me
	 *  count of Who I am Following
	*/
	protected $followingCount = null;
	protected $followerCount = null;
	protected $howManyPrivateItems = null;

	/**
	 * Array of all users we are interested in,
	 */
	protected $allUsers=array();

	/**
	 * Array of all user ID's we are interested in,
	*/
	protected $allUserIDs=array();

	/**
	 * Array of all events we want to see,
	*/
	protected $allEvents=array();


	public function __construct(CommonCore\Users\UserRepositoryInterface $userRepo)
	{
		$this->userRepo = $userRepo;
	}

	public function activityFeed($userID='me'){

		
		$this->user = $this->userRepo->discover($userID);
		if(null==$this->user){
			return Response::json(array('message' => 'Bad User'),    403);
		}
	
		if(null==Auth::User()){
			return Response::json(array('message' => 'Not Logged In'),    403);
		}

		$redis_key = "user:" .  $this->user->_id  . ":following";
		$flw = RedisL4::connection()->SMEMBERS($redis_key);
		
		$allFlw=array();
		$allFlwTimestamps=array();
		$i=0;
		foreach($flw as $thisUserID){
			
			$oneFlwData = array();
			
			//1. Check for CREATED stories
			$allMyStories = Story::where("user_id" , "=" , $thisUserID)->get()->each(
				function($s) use (&$allFlw, &$allFlwTimestamps, $i){
					$oneFlwData = array();
					$oneFlwData['activity']='Created New Album';
					$oneFlwData['activityID']=$s->_id;
					$oneFlwData['timestamp']=$s->created_at->timestamp;
					$oneFlwData['object']=null;
					$oneFlwData['user']=null;
					$allFlw[]=$oneFlwData;
					
					$allFlwTimestamps[]=$s->created_at->timestamp;
					
				}
			);
			
			//2. Check for COMMENTS
			$allMyStories = Comment::where("user_id" , "=" , $thisUserID)->get()->each(
				function($s) use (&$allFlw, &$allFlwTimestamps, $i){
					$oneFlwData = array();
					$oneFlwData['activity']='Comment';
					$oneFlwData['activityID']=$s->_id;
					$oneFlwData['timestamp']=$s->created_at->timestamp;	
					$oneFlwData['object']=null;
					$oneFlwData['user']=null;
					$allFlw[]=$oneFlwData;
					
					$allFlwTimestamps[]=$s->created_at->timestamp;
				
				}
			);
			
			//3. Check for STORY-LIKES
			$redis_key = "user:" .  $thisUserID  . ":storylike";
			$likedstories = RedisL4::connection()->zRangeByScore($redis_key, -INF, +INF , [ 'withscores' => true] );
			foreach($likedstories as $likedstory){
				$oneFlwData = array();
				$oneFlwData['activity']='Liked';
				$oneFlwData['activityID']=$likedstory[0];
				$oneFlwData['timestamp']=intval($likedstory[1]);
				$oneFlwData['object']=null;
				$oneFlwData['user']=null;
				$allFlw[]=$oneFlwData;
				
				$allFlwTimestamps[]=intval($likedstory[1]);
			}
			
			$i++;
		}
		
		array_multisort($allFlwTimestamps, SORT_DESC, $allFlw);
		
		
		return Response::json($allFlw , 200);
	}
	
}	
	