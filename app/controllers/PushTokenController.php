<?php
//  UNSUITABLE FOR REVIEW 10/01
//	NOTE:: COPIED AS IS FROM STICKERZAPP.
//	LIST MODIFICATION TO CUSTOMISE FOR MYRTLE HERE:

use CommonCore\Users\User;

class PushTokenController extends \BaseController {
	
	/**
	 * Most methods of this controller operate on a single user object, so we can store it at class level for ease
	 *
	 * @var User The controller user object
	 */
	private $user = null;
	
	public function savePushToken($id) {
		
		//--- If $id is "me" then translate it to the curuser id, before continuing
		if($id == 'me') {
				
			if(Auth::check() === false)
				return Response::json(array('message' => 'You must be logged in'), 403);
				
			$this->user = Auth::user();
			$id = $this->user->getKey();
				
		} else {
				
			$this->user = User::find($id);
			if(is_null($this->user))
				return Response::json(array('message' =>'User not found'), 404);
				
		}
		
		//--- For now, you must be auth'd and can only edit your own account
		if(Auth::check() === false || $this->user->getKey() !== Auth::user()->getKey()) 
			return Response::json(array('message'=>'Forbidden'), 400);
		
		if(($token = Input::get('token')) === null) {
			return Response::json(array('message'=>'Missing token'), 400);
		}
		
		if(($deviceId = Input::get('udid')) === null) {
			return Response::json(array('message'=>'Missing device id'), 400);
		}
		
		if(($deviceType = Input::get('devicetype')) === null) {
			return Response::json(array('message'=>'Missing device type (ios?)'), 400);
		}
		
		if((Input::get('development')) === null) {
			$development = false;
		}else{
			$development = Input::get('development');
		}
		
		$this->user->addPushToken($deviceId, $token, $deviceType, $development);	
		
		return Response::json(array('messages' => 'Push token saved'));	
		
	}
	
	
	public function removePushToken($id) {
	
		//--- If $id is "me" then translate it to the curuser id, before continuing
		if($id == 'me') {
	
			if(Auth::check() === false)
				return Response::json(array('message' => 'You must be logged in'), 403);
	
			$this->user = Auth::user();
			$id = $this->user->getKey();
	
		} else {
	
			$this->user = User::find($id);
			if(is_null($this->user))
				return Response::json(array('message' =>'User not found'), 404);
	
		}
	
		//--- For now, you must be auth'd and can only edit your own account
		if(Auth::check() === false || $this->user->getKey() !== Auth::user()->getKey())
			return Response::json(array('message'=>'Forbidden'), 403);
	
	
		if(($deviceId = Input::get('udid')) === null) {
			return Response::json(array('message'=>'Missing device id'), 400);
		}
		
	
		$this->user->clearPushTokens($deviceId);
	
		return Response::json(array('messages' => 'Push token removed'));
	
	}
	
	

	
}