<?php
//  UNSUITABLE FOR REVIEW 10/01
//	NOTE:: COPIED AS IS FROM STICKERZAPP.
//	LIST MODIFICATION TO CUSTOMISE FOR MYRTLE HERE:

use CommonCore\Users\User;
use \Carbon\Carbon;

class NotificationsController extends \BaseController {
	
	/**
	 * User repository
	 *
	 * @var CommonCore\Users\UserRepositoryInterface
	 */
	protected $userRepo;
	
	/**
	 * 	The user logged in by common core
	 */
	protected $user = null;
	
	/**
	 * 	A list of things to append to the URL at pagination
	 */
	protected $urlAppend;
	
	/**
	 * 	Default items-per-page at pagination time
	 */
	protected $pageCount=20;
	
	/**
	 * 	String to hold the name of the assoc. array which contains the main data
	 *  requested by $query
	 */
	protected $dataArrayName='notifications';
	
	
	/**
	 * 	Array of strings, each representing a DB 'column' which is specifically
	 *  required.  Unpopluated or null returns all items.
	 */
	protected $resultsMask = array();
	
	
	/**
	 * 	A general query which should be acceptable to ->get() on.
	 */
	protected $query=null;		

	
	public function __construct(CommonCore\Users\UserRepositoryInterface $userRepo)
	{
		$this->userRepo = $userRepo;
	}
	
	public function getAndMarkOneNotification($notificationID){
		
		$notification = Notification::find($notificationID);
		
		$notification->read = true;
		
		return Response::json($notification->toArray() , 200);
		
	}
	
	
	/**
	 * 	return a paginated Notifications feed
	 */
	public function loadFeed($userID='me') {

		// Must be authd
		if(Auth::check() === false){
			return Response::json(array('message' => 'Forbidden'), 403);
		}
		
		$this->user = $this->userRepo->discover($userID);
		if(null==$this->user){
			return Response::json(array('message' => 'Bad User'),    403);
		}		
		$this->query = Notification::where("recipient_id", "=", $this->user->getKey());
		$this->query->where("user_id", "!=", $this->user->getKey());
		//$this->query->orWhere("type", "=", "permissionrequest");
		$this->query->where("read", "!=" , true);
		
		$results = $this->makeReturnObject();
		return Response::json($results , 200);		
		

	}

	
	/**
	 */
	function getNotificationAndMarkAsRead($userID, $notificationID){
	
	//$this->query = Question::orderBy('created_by');
	//dd($notificationID);
	
	if(null==Auth::User()){
		return Response::json(array('message' => 'Not Logged In'),    403);
	}
	
	$this->user = $this->userRepo->discover($userID);
	
	if(null==$this->user){
		return Response::json(array('message' => 'Bad User'),    403);
	}
	
		$userID = $this->user->getKey();

		$localQuery = Notification::find($notificationID);

		$localQuery->read = "true";

		$localQuery->save();

		if($localQuery->errors()  ==  null ){

		}

		$results = $localQuery->toArray();

		return Response::json($results , 200);
	
	}	
	
	
	/**
	 */
	function deleteNotificationsMarkedAsRead($userID){
	
	
	if(null==Auth::User()){
	return Response::json(array('message' => 'Not Logged In'),    403);
	}
	
	$this->user = $this->userRepo->discover($userID);
	if(null==$this->user){
	return Response::json(array('message' => 'Bad User'),    403);
		}
	
			$userID = $this->user->getKey();
	
			$n = Notification::where( "recipient_id", "=", $userID );
	
					$n->where( "read", "=", "true" );
	
		$n->get();
	
		if( null == $n ){
			return Response::json(['message' => 'No records to delete'], 200);
		}
		$n->delete();
	
			//dd($n->array());
	
			$n=null;
	
			$n = Notification::where( "recipient_id", "=", $userID );
		$n->where( "read", "=", true );
			$n->get();
	
					if( null == $n ){
					return Response::json(['message' => 'Deletion Failed'], 400);
			}else{
			return Response::json(['message' => 'Record Deleted'], 200);
			}
	
	
		return Response::json($results , 200);
	
	}
	
		
	
	
	
	/**
	 * 	does not accept any arguments.
	 * 	uses $this->query, which must be a querysuitable for having a get() run on it.
	 *  uses $this->pageCount to determine how many data objects per page are returned.
	 *  uses get('page',0) to determine which page is to be presented
	 *  
	 *  This version in this class does not require a field mask or ordering properties.
	 *  
	 *  @returns an array consisting of:
	 *  	-a pagination object
	 *  	-a user object
	 *  	-an object containing many Data objects.  The type of the Data objects is dependent 
	 *  	 on the query set as $this->query.
	 */
	private function makeReturnObject(){
	
		
		$this->query->orderBy('created_at', 'DESC');
		
		//Per page?
		$perpage = intval(Input::get('count',0)) > 0 ? intval(Input::get('count')) : $this->pageCount;
		$this->query->take($perpage);
			
		// Page num?
		$page = intval(Input::get('page',0));
		$this->query->skip($perpage * $page);
	
		$results = $this->query->get();

		
		/*
		if($this->resultsMask!=null){
			$results = $this->query->get($this->resultsMask);
		}else{
			$results = $this->query->get();
		}
		*/
	
	
		$pagination = array(
				'pagination' => [
				'requested' => [
				'page' => $page,
				'count' => $perpage
				],
				'page' => $page,
				'navigation' => [
				'previous' => $page <= 0 ? null : URL::current().'?count='.$perpage.'&page='.($page-1) . $this->urlAppend,
				'next' => $results->count() >= $perpage ? URL::current().'?count='.$perpage.'&page='.($page+1). $this->urlAppend : null
				]
				]
		);
		
		$results->each(function($r){

			if( $r->type === 'comment' ){
				$commentID = $r->entity_id;
				$comment = Comment::find( $commentID );
					
				if(isset($comment->storypicture_id)){
					$storypicture_id = $comment->storypicture_id;
					$r->storypicture_id = $storypicture_id;
				}else{
					$r->storypicture_id = null;
				}
			}			

			$r->user = User::find($r->user_id);
							
			
		});

		/*
			$aresults = $results->toArray();
			
			$newaresults = array();
			
			foreach($aresults as $aresult){
					
				if( $aresult['type'] === 'comment' ){
					$commentID = $aresult['entity_id'];
					$comment = Comment::find( $commentID );
			
					if(isset($comment->storypicture_id)){
						$storypicture_id = $comment->storypicture_id;
						$aresult['storypicture_id'] = $storypicture_id;
					}
				}
				$newaresults[] = $aresult;
			}		
		*/
		$returnResults = array();
		$returnResults['user'] = $this->user;
		$returnResults['pagination'] = $pagination;
		$returnResults[$this->dataArrayName] = $results->toArray();
		
		//$finalReturnResults = $this->processReturnResults($returnResults);
		
		return($returnResults);
	}	

	
	/**
	 * From here, arguably deprecated. Unless some convincing need arises, be sure to delete before production release.
	 * 
	 * 
	 * @param unknown $returnResults
	 * @return multitype:Ambigous <\Illuminate\Support\Collection, \Illuminate\Database\Eloquent\static, \Illuminate\Database\Eloquent\Collection, \Illuminate\Database\Eloquent\Model, NULL, multitype:\Illuminate\Database\Eloquent\static >
	 */
	
	protected function processReturnResults($returnResults){
		
		dd($returnResults);
		
		$finalReturnResults = array();
		
		foreach($returnResults as $returnResult){
			
			$user = User::find($returnResult['user_id']);
			$returnResult['user'] = $user;
			$finalReturnResults[] = $returnResult;
		}
		
		return $finalReturnResults;
	}

	public function make_model($n) {

		$model = array('type' => $n['event']);

		$model['read'] = isset($n['read']) && $n['read'] == 0 ? 0 : 1;
		$model['_id'] = isset($n['_id']) ? $n['_id'] : null;



		if(isset($n['user'])) {
			$user = User::find($n['user']);
			if(is_null($user))
				return;
			$model['user'] = $user->toArray();
		}

		switch($n['event']) {

			case 'ownsticker.liked':

				if(!array_key_exists('sticker', $n) || ($sticker = Sticker::find($n['sticker'])) === null)
					return false;

				$model['object_type'] = 'sticker';
				$model['object'] = $sticker->toArray();

			break;

			case 'ownsticker.comment':

				if(!array_key_exists('comment', $n) || ($comment = StickerComment::find($n['comment'])) === null || is_null($comment->sticker))
					return false;

				$model['object_type'] = 'sticker';
				$model['object'] = $comment->sticker->toArray();
				$model['comment'] = $comment->message;

			break;

			case 'ownsticker.purchased':

				if(!array_key_exists('sticker', $n) || ($sticker = Sticker::find($n['sticker'])) === null)
					return false;

				$model['object_type'] = 'sticker';
				$model['object'] = $sticker->toArray();

			break;

			case 'ownphoto.liked':

				if(!array_key_exists('photo', $n) || ($photo = Photo::find($n['photo'])) === null)
					return false;

				$model['object_type'] = 'photo';
				$model['object'] = $photo->toArray();

			break;

			case 'ownphoto.comment':

				if(!array_key_exists('comment', $n) || ($comment = PhotoComment::find($n['comment'])) === null || is_null($comment->photo))
					return false;

				$model['object_type'] = 'photo';
				$model['object'] = $comment->photo->toArray();
				$model['comment'] = $comment->message;

			break;


		}

		if(isset($n['coins']))
			$model['coins'] = $n['coins'];

		return $model;



	}

	
	



	
	
	
	/**
	 * Really should put notifications into mongo, or work a way of having an ID in redis that we can easily select just one from
	 * but this is what you get for spec changing halfway through, we should look at cleaning up the notifications during a second phase of dev once the system has stopped being changed
	 * @param string $nid
	 */

	public function markAsRead($nid) {

	    // Must be authd
	    if(Auth::check() === false)
	        return Response::json(array('message' => 'Forbidden'), 403);

	    $notifications = array();

	    $redis = RedisL4::connection();

	    $notifications_enc = $redis->zRevRangeByScore('user:'.Auth::user()->getKey().':notifications', '+inf', '-inf', 'WITHSCORES');

	    //-- expand em!
	    if(!empty($notifications_enc)) {

	        foreach($notifications_enc as $ndata) {

	            $notif_data = json_decode($ndata[0], true);
	            $timestamp = $ndata[1];

	            if($nid === 'all' || ( isset($notif_data['_id']) && $notif_data['_id'] == $nid )) {

	                $notif_data['read'] = 1;

	                $redis->zRem('user:'.Auth::user()->getKey().':notifications', $ndata[0]);

	                $redis->zAdd('user:'.Auth::user()->getKey().':notifications', $timestamp, json_encode($notif_data));

	            }



	        }



	    }

        return Response::json(['message' => 'OK']);

	}

}
