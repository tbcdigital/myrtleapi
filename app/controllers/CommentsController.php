<?php
use CommonCore\Users\User;
/**
 * Handle creation / viewing / deletion of COMMENTs. 
*/
class CommentsController extends \BaseController{
	
	
	/**
	 * 	A general query which should be acceptable to ->get() on.
	 */
	protected $query=null;

	/**
	 * 	A list of things to append to the URL at pagination
	 */
	protected $urlAppend;
	
	/**
	 * 	Default items-per-page at pagination time
	 */
	protected $pageCount=50;
	
	protected $allresults;
	
    /**
     * Handle creation of one COMMENT in Mongo.
     * 
     *  @param:  (opt if reply_to is set)string $story_ID
     *  @param:  (opt) string $reply_to: the ID of a comment being replied to 
     *  @param:  (req) string $comment
     *  @param:  (req) string $type picture | story 
     *  @param:  (opt) string $storypicture_id : the ID of a storypicture which this comment attaches to
     *  @return: string completed_status_message
     */
    function newComment($storyID=null, $reply_to=null){
    	
		
    	if(null==Auth::User()){
    		return Response::json(array('message' => 'Not Logged In'),    403);
    	}
    	
    	$access_key = "user:" .Auth::User()->getKey()  . ":storyaccess";
    	
    	$doesExist = RedisL4::connection()->sIsMember( $access_key, $storyID );
    	
    	$story=Story::find($storyID);
    	if( $story->user_id !=  Auth::User()->getKey() && $story->privacy != 'public'  && $doesExist==false ){
    		return Response::json(array('message' => 'The story is not Public, You are not the Story Creator, and You do not have permissions to Comment on this story'),    403);
    	}
    	    	 	
    	$comment    		= Input::get('comment');
    	$type				= Input::get('type');	
    	$storypicture_id	= Input::get('storypicture');

    	if(null!=$type){}
    	
		if(!null==$reply_to && null==$storyID){
	    	$storyID = Comment::find($reply_to)->story_id;
	    }
       	
        if(!null==$storypicture_id && null==StoryPicture::find($storypicture_id)  && $type=='picture'){
    		return Response::json(['message' => 'Comments of type PICTURE must supply a valid StoryPicture ID.'], 400);
    	}
    	
    	$s = new Comment( array( 'user_id'=>Auth::User()->getKey(), 'reply_to'=>$reply_to ) );
    	$s->story_id=$storyID;
    	$s->comment=$comment;
    	$s->user_id=Auth::User()->getKey();
    	
   		$s->type=$type;
    	
    	if(null!=$storypicture_id){
	    	$s->storypicture_id=$storypicture_id;
    	}
    		
    	$s->save();
    	    	
    	if( false === $s->exists ) {
    		return Response::json(['errors' => $s->errors()->toArray(), 'message' => 'Comment failed creation'], 400);
    	}
    	
    	return Response::json(array('message' => 'Completed Normally: ID ' . $s->_id), 200);    
    }
    
    /**
     * Handle edit of one COMMENT in Mongo.
     * 
     *  @param: array Input::get() 
     *  @return: string completed_status_message
     */
    function updateComment($commentID=null){
    	
    	if(null==Auth::User()){
    		return Response::json(array('message' => 'Not Logged In'),    403);
    	}    	
    	
        $st=Comment::find($commentID);
        
        $st->update(Input::all()) ;
        
        $rez = $st->save();
        
        if($rez==false){
        	return Response::json(array('message' => 'Update failed.'), 400);
        }
        
        return Response::json(array('message' => 'Completed Normally'), 200);        
    }

    /**
     * Get a list of comments, by Story (event) or User.
     * ACCEPTS a 'criteria ID', which can be an event ID or a User ID.  Mongo ID's are unique, so no ambiguation risk.
     * 
     *  @param: string $crit_ID
     *  @param: (opt) string type (picture | story )
     *  @return: array (zero, one or many COMMENT objects in JSON)
     */
    function getComments($crit_ID=null){
    	//dd('here');
       	if(null==Auth::User()){
    		return Response::json(array('message' => 'Not Logged In'),    403);
    	}    	
    	
        if (empty($crit_ID)){
        	return Response::json(array('message' => 'No story/user ID'), 400);
        }
        
        //$this->query = Comment::where('story_id', '=', $crit_ID)->orWhere('user_id', '=', $crit_ID)->orWhere('storypicture_id', '=', $crit_ID);
        //$results = $this->makeReturnObject();
        //return Response::json($results , 200);        
        
        $type		= Input::get('type');
        //dd($type);
        $storyTry = Story::find($crit_ID);
        $userTry  = User::find($crit_ID);
        $storyPictureTry  = StoryPicture::find($crit_ID);
        if(null!=$storyTry){
            $critData = $storyTry->toArray();
        }else{
            if(null!=$userTry){
                $critData = $userTry->toArray();
            }else{
            	if(null!=$storyPictureTry){
            		$critData = $storyPictureTry->toArray();
            	}else{
                	return;
            	}
            }
        }
		
        if(null==$type){
	        $this->query = Comment::where('story_id', '=', $crit_ID)->orWhere('user_id', '=', $crit_ID)->orWhere('storypicture_id', '=', $crit_ID);
        }else{
        	
        	if($type=='story'){
        		$this->query = Comment::where('story_id', '=', $crit_ID)->orWhere('storypicture_id', '=', $crit_ID)->where('type', '=', $type);
        	}elseif($type=='picture'){
        		$this->query = Comment::where('story_id', '=', $crit_ID)->orWhere('storypicture_id', '=', $crit_ID)->where('type', '=', $type);
        	}
        	
        	
       /*
        	$rows = Comment::where(
        			function($qry) use ($crit_ID, $type){
        				$qry->where('story_id', '=', $crit_ID)
        				  ->where('type', '=', $type);
        			}
        	)->orWhere(
        			function($qry) use ($crit_ID, $type){
        				$qry->where('user_id', '=' , $crit_ID)
        				  ->where('type', '=', $type);
        			}
        	)->get();
        */
        }	
        
        $results = $this->makeReturnObject();
        return Response::json($results , 200);
        
        //return Response::json(array( ['requestor' => $critData] , ['comments' => $rows->toArray()]), 200 );
    }
    
    
    /**
     * Get a list of COMMENTs, by Story (event) and User.
     * 
     * ACCEPTS BOTH a UserID and an Event ID.
     * 
     *  @param: string $userID
     *  @return: array (zero, one or many COMMENT objects in JSON)
     */    
     function getCommentsUIDandSID($storyID=null, $userID=null){
     	
         if(null==Auth::User()){
    		return Response::json(array('message' => 'Not Logged In'),    403);
    	} 
     	
        $rows = Comment::where('user_id', '=', $userID)->where('story_id', '=', $storyID)->get();
        
        if (empty($rows->toArray())){
        	return Response::json(array('message' => 'no data for story / user ' . $storyID . '/' . $userID), 200);
        }
        
        return Response::json(['data' => $rows->toArray()], 200 );
    }
    
    /**
     * Get a list of COMMENTs by storypicture.
     *
     * ACCEPTS BOTH a UserID and an Event ID.
     *
     *  @param: string $userID
     *  @return: array (zero, one or many COMMENT objects in JSON)
     */
    function getCommentsByStoryPicture($storypictureID){
    
    	if(null==Auth::User()){
    		return Response::json(array('message' => 'Not Logged In'),    403);
    	}
    
    	$rows = Comment::where('storypicture_id', '=', $userID)->where('story_id', '=', $storyID)->get();
    
    	if (empty($rows->toArray())){
    		return Response::json(array('message' => 'no data'), 200);
    	}
    
    	return Response::json(['data' => $rows->toArray()], 200 );
    }
    
    
    /**
     *  Get one COMMENT in detail.
     *  
     *  @param: string $commentID
     *  @return: array (zero or one COMMENT objects in JSON)
     */
    function getComment($commentID=null){

        if(null==Auth::User()){
    		return Response::json(array('message' => 'Not Logged In'),    403);
    	} 
    	
    	    	
        if (empty($commentID)){
        	return Response::json(array('message' => 'No comment ID'), 200);
        }
        
        
        
        return Response::json(['data' => Comment::find($commentID)->toArray()], 200 );
    }
    
    /**
     * Check if the Client can allow this user to delete a certain comment.
     *
     *  @param: string $commentID
     *  @return: TRUE | FALSE
     */
    function deleteCommentRightsCheck($commentID=null){   
    
    	$c = Comment::find($commentID);
    	$s = $c->user_id;
    	if(null == $s){
    		return Response::json(['message' => 'No User ID'], 400);
    	}
    	
    	$story = Story::find( $c->story_id );
    	$story_user = $story->user_id;
    	 
    	$fails=0;
    	$failstring= "";
    	 
    	if( $s !=  Auth::User()->getKey()){
    		$failstring = $failstring . " You are neither the Comment Creator ";
    		$fails++;
    	}
    	
    	if( $story_user !=  Auth::User()->getKey()){
    		$failstring = $failstring . " You are not the Event Creator ";
    		$fails++;
    	}
    	
    	if($fails>1){
    		return false;
    	} else{
    		return true;
    	}  	
    
    }
    	
    	
    /**
     * Handle deletion of one COMMENT in Mongo.
     * 
     *  @param: string $commentID
     *  @return: string completed_status_message
     */
    function deleteComment($commentID=null){

    	if(null==Auth::User()){
    		return Response::json(array('message' => 'Not Logged In'),    403);
    	}
    	
    	$c = Comment::find($commentID);
    	$s = $c->user_id;
    	if(null == $s){
    		return Response::json(['message' => 'No User ID'], 400);
    	}

    	$story = Story::find( $c->story_id );
    	$story_user = $story->user_id;
    	
    	$fails=0;
    	$failstring= "";
    	
    	if( $s !=  Auth::User()->getKey()){
    		
    		$failstring = $failstring . " You are not the Comment Creator. ";
    		
    		$fails++;
    	
    	}    	

    	if( $story_user !=  Auth::User()->getKey()){

    		$failstring = $failstring . " You are not the Event Creator. ";
    		
    		$fails++;
    	
    	}
    	 
    	if($fails>1){
    		
    		return Response::json(array('message' => $failstring),    403);
    	
    	 }
    	
    	$c->delete();
        
        if( false != Story::find($commentID) ){
        	return Response::json(['message' => 'Deletion Failed'], 400);
        }else{
            return Response::json(['message' => 'Record Deleted'], 200);
        }        
    }

    
    /**
     * Handle creation of one COMMENT in Mongo.
     * A thin wrapper around newComment to enable calling with no storyID but with a reply-to comment ID.
     *
     *  @param: (opt) string $reply_to: the ID of the comment being replied to
     *  @param:  string $comment
     *  @return: string completed_status_message
     */
    function newReplyComment($reply_to){
    	return $this->newComment(null, $reply_to);
    }    

    private function makeReturnObject(){
    
    
    
    	//Per page?
    	$perpage = intval(Input::get('count',0)) > 0 ? intval(Input::get('count')) : $this->pageCount;
    	$this->query->take($perpage);
    		
    	// Page num?
    	$page = intval(Input::get('page',0));
    	$this->query->skip($perpage * $page);
    
    
    	$results = $this->query->get();
    	$arrResults = $results->toArray();
    	$finalResults = $this->resultsGetUsers($arrResults);
    	
    	$pagination = array(
    			'pagination' => [
    			'requested' => [
    			'page' => $page,
    			'count' => $perpage
    			],
    			'page' => $page,
    			'navigation' => [
    			'previous' => $page <= 0 ? null : URL::current().'?count='.$perpage.'&page='.($page-1) . $this->urlAppend,
    			'next' => $results->count() >= $perpage ? URL::current().'?count='.$perpage.'&page='.($page+1). $this->urlAppend : null
    			]
    			]
    	);
    
    		
    	$returnResults["pagination"] = $pagination;
    	$returnResults["comments"] = $finalResults;
    
    
    
    	return($returnResults);
    }
    
    protected function resultsGetUsers($results){
		$retval = array();
    	foreach($results as $result){
    		$result["user"] = User::find($result["user_id"]);
    		$retval[]=$result;
    	}
		return $retval;
    }
    
    protected function resultsReorder($returnResults){
    	//Stub Function for special re-ordering / blocking rules F.I requiring Redis.
    	//Note that anything here will only act on whatever was GETted from the DB by $this->query
    	return $returnResults;
    }    
    
    
}