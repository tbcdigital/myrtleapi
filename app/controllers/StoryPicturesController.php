<?php
use CommonCore\Users\User;
/**
 * Handle some information about Pictures For Events. 
*/



class StoryPicturesController extends \BaseController{

	/**
	 * User repository
	 *
	 * @var CommonCore\Users\UserRepositoryInterface
	 */
	protected $userRepo;
	
	/**
	 * 	The user logged in by common core
	 */
	protected $user = null;


	/**
	 * 	String to hold the name of the assoc. array which contains the main data
	 *  requested by $query
	 */
	protected $dataArrayName='users';

	/**
	 * 	A list of things to append to the URL at pagination
	 */
	protected $urlAppend;
	
	/**
	 * 	Default items-per-page at pagination time
	 */
	protected $pageCount=50;

	/**
	 * 	A general query which should be acceptable to ->get() on.
	 */
	protected $query=null;

	
	
	
	public function __construct(CommonCore\Users\UserRepositoryInterface $userRepo)
	{
		$this->userRepo = $userRepo;
	}

	
	/**
	 * Function setEventPictureInformation.
	 *
	 * @param: string $eventID.
	 * @return: string statusMessage
	 */
	function setEventPictureInformation($storyID){
		
		
		if(null==Auth::User()){
			return Response::json(array('message' => 'Not Logged In'),    403);
		}		
		
		$story=Story::find($storyID);
		
		$addCrit = 0;
		
		if( $story->user_id ==  Auth::User()->getKey() || $story->privacy == 'public'){
			$addCrit++;
		}		
		
		$redis_key = "story:" . $storyID . ":useraccess";
		
		$data = RedisL4::connection()->SISMEMBER($redis_key, Auth::User()->getKey());
		
		if($data == true){
			$addCrit++;
		}
		
		if($addCrit > 0){
			//In the future, we may need to handle 'either' perms 'or' creator, 'or' public, but not all 3 here.
			//Right now, any 1 of 3 will do.
		}else{
			return Response::json(array('message' => 'The story is not Public, and You are not the Story Creator'),    403);
		}
		
		//dd("setEventPictureInformation");
		
		$epi = new StoryPicture(Input::all());
		
		/** NEW 11/19.
		 * From now, we will get only the image name from the client.
		 * A Mutator will be used 
		 *  */
		
		$epi->story_id = $storyID;
		$epi->user_id = Auth::User()->getKey();
		$epi->save();
		$id = $epi->_id;
		
		return Response::json(['message' => 'Complete.  ID: ' . $id], 200);
	}
	
	/**
	 * Function deleteEventPictureInformation.
	 *
	 * @param: string $eventID.
	 * @return: string statusMessage
	 */
	function delEventPictureInformation($storyPicID){

		if(null==Auth::User()){
			return Response::json(array('message' => 'Not Logged In'),    403);
		}

		if(null==StoryPicture::find($storyPicID)){
			return Response::json(array('message' => 'No Such Storypic: ' . $storyPicID),    400);
		}
		
		$c = StoryPicture::find($storyPicID);
		$s = $c->story_id;
		$u = $c->user_id;
		$o = Story::find($s)->user_id;
		
		if( $u !=  Auth::User()->getKey()  &&  $o !=  Auth::User()->getKey()   ){
			return Response::json(array('message' => 'You are neither the Story Creator nor the Story-Photo creator'),    403);
		}
		 
		$c->delete();
		
		if( false != StoryPicture::find($storyPicID) ){
			return Response::json(['message' => 'Deletion Failed'], 400);
		}else{
			return Response::json(['message' => 'Record Deleted'], 200);
		}		
		
		
	}
	
	/**
	 * Function getEventPictureInformation.
	 *
	 * @param: string $eventID.
	 * @return: string statusMessage
	 */
	function getEventPictureInformation($storyID){
		
		if(null==Auth::User()){
			return Response::json(array('message' => 'Not Logged In'),    403);
		}
		
		$story = Story::find($storyID);
		
		$strcmt = Comment::where('story_id', "=", $story->getKey() )->get()->toArray();
		
		$story['commentcount'] = count($strcmt);
		
		if(null==$story){
			return Response::json(array('message' => 'No Such Event: ' . $storyID),    400);
		}
		
		$pix = $story->storypictures()->orderBy("created_at", "DESC")->get()->toArray();
		
		$pixOut = array();
		
		foreach($pix as $pic){
			
			if(array_key_exists('likes', $pic) == false){
				$pic['likes'] = 0;
			}
			
			$redis_key = "user:". Auth::User()->getKey() .":storypicturelike";
			
			$data = RedisL4::connection()->ZSCORE($redis_key, $pic['_id'] );
			
			if($data == null){
				$pic['doilikethis'] = false;
			}else{
				$pic['doilikethis'] = true;
			}
			
			$uid = $pic['user_id'];
			$pic['user'] = User::find($uid)->toArray();
			
			$cmt = Comment::where('storypicture_id', "=", $pic['_id'] )->get()->toArray();
			
			$pic['commentcount'] = count($cmt);
			
			$pixOut[] = $pic;
		}
		
		return Response::json(['story'=>$story , 'data' => $pixOut], 200 );
	}
	
	function getPictureInformation($storyPicID){
		
		if(null==Auth::User()){
			return Response::json(array('message' => 'Not Logged In'),    403);
		}
		
		$storyPic = StoryPicture::find($storyPicID);
		
		if($storyPic==null){
			return Response::json(array('message' => 'No Such Storypic: ' . $storyPicID),    400);
		}
		
		return Response::json(['data' => StoryPicture::find($storyPicID)], 200 );
	}
	
	
	function makeNewBatch($eventID="", $userID = "me"){
		
		if(null==Auth::User()){
			return Response::json(array('message' => 'Not Logged In'),    403);
		}
		
		$this->user = $this->userRepo->discover($userID);
		if(null==$this->user){
			return Response::json(array('message' => 'Bad User'),    403);
		}		
		
		//dd($this->user);
		
		$epi = new Batch();
		$epi->story_id = $eventID;
		$epi->user_id = Auth::User()->getKey();
		$epi->save();
		$id = $epi->_id;
		
		return Response::json(['message' => $id], 200);		
		
		
	}
	
	function finishBatch($batchID, $userID = "me"){

		if(null==Auth::User()){
			return Response::json(array('message' => 'Not Logged In'),    403);
		}
		
		$this->user = $this->userRepo->discover($userID);
		if(null==$this->user){
			return Response::json(array('message' => 'Bad User'),    403);
		}
		
		$count = 0;
		$count = Input::get("count");
		
		$batch = Batch::find($batchID);
		
		if( null == $batch){
			return Response::json(['message' => "no such batch"], 200);
		}
		
		if ($batch->user_id != $this->user->getKey() ){
			return Response::json(['message' => "not your batch"], 200);
		}
		
		//if ($batch->status == "open" ){
			$batch->status = "closed";
			$batch->count = $count;
			$batch->save();
			Event::fire('batch.completed', array($batch, $this->user->getKey() ));
			return Response::json(['message' => "batch " . $batchID . " closed."], 200);
		//}else{
		//	return Response::json(['message' => "batch finished. nothing to do."], 200);
		//}
			
	}
	
	/**
	 * Function to return a list of User objects for users who LIKED this storypicture.
	 * @storypicture ID
	 */
	function likedByStoryPicture( $storyPictureID, $userID = "me" ){

		if(null==Auth::User()){
			return Response::json(array('message' => 'Not Logged In'),    403);
		}
		
		$this->user = $this->userRepo->discover($userID);
		if(null==$this->user){
			return Response::json(array('message' => 'Bad User'),    403);
		}		
		
		$redisKey = "storypicture:" . $storyPictureID . ":userlike";
		
		$data = RedisL4::connection()->ZRANGEBYSCORE( $redisKey, -INF, +INF  );
		
		$this->query = User::whereIn("_id", $data);
		
		//	dd($data);
		
		//	dd($this->query->get()->toArray());
		
		$results = $this->makeReturnObject();
		
		return Response::json($results , 200);
	}

	
	private function makeReturnObject(){
	
		//$this->query->orderBy('created_at', 'DESC');
	
		$perpage = intval(Input::get('count',0)) > 0 ? intval(Input::get('count')) : $this->pageCount;
		$this->query->take($perpage);
				
		// Page num?
			$page = intval(Input::get('page',0));
			$this->query->skip($perpage * $page);

			$results = $this->query->get();
	
			$pagination = array(
			'pagination' => [
			'requested' => [
			'page' => $page,
			'count' => $perpage
					],
			'page' => $page,
			'navigation' => [
			'previous' => $page <= 0 ? null : URL::current().'?count='.$perpage.'&page='.($page-1) . $this->urlAppend,
					'next' => $results->count() >= $perpage ? URL::current().'?count='.$perpage.'&page='.($page+1). $this->urlAppend : null
					]
				]
			);
		
			$returnResults = array();
			$returnResults['user'] = $this->user;
			$returnResults['pagination'] = $pagination;
			$finalResults = $results->toArray();

			//$processedFinalResults = $this->resultsReorder($finalResults);

			$returnResults[$this->dataArrayName] = $finalResults;

			return($returnResults);

	}	
	
}
