<?php
use CommonCore\Users\User;
/**
 * Handle user A follows user B...
 * and useful information like "Who do I follow..?", "Who follows me...?"
 * 
*/
class FollowsFeedController extends \BaseController{

	/**
	 * User repository
	 *
	 * @var CommonCore\Users\UserRepositoryInterface
	 */
	protected $userRepo;
	
	/**
	 * 	The user logged in by common core
	 */
	protected $user = null;
	
	/**
	 * 	A list of things to append to the URL at pagination
	 */
	protected $urlAppend;
	
	/**
	 * 	Default items-per-page at pagination time
	 */
	protected $pageCount=50;

	/**
	 * 	A general query which should be acceptable to ->get() on.
	 */
	protected $query=null;

	/**
	 * 	String to hold the name of the assoc. array which contains the main data
	 *  requested by $query
	 */
	protected $dataArrayName='Feed';

	/**
	 * 	Array of strings, each representing a DB 'column' which is specifically
	 *  required.  Unpopluated or null returns all items.
	 */
	protected $resultsMask = array();	
	
	/**
	 * 	count of Who Follows Me
	 *  count of Who I am Following
	 */
	protected $followingCount = null;
	protected $followerCount = null;
	protected $howManyPrivateItems = null;
	
	/**
	 * Array of all users we are interested in,
	 */
	protected $allUsers=array();

	/**
	 * Array of all user ID's we are interested in,
	 */
	protected $allUserIDs=array();

	/**
	 * Array of all events we want to see,
	 */
	protected $allEvents=array();
	
	protected $reorderOrNot = false;
	
	protected $followIndicatorOrNot = false;
	
	protected $followingIndicatorOrNot = false;
	
	public function __construct(CommonCore\Users\UserRepositoryInterface $userRepo)
	{
		$this->userRepo = $userRepo;
	}	
	
	public function follow($userID){

		if(User::find($userID)==null){
			return Response::json(array('message' => 'Not Valid User ID'),    403);
		}
		
		if(null==Auth::User()){
			return Response::json(array('message' => 'Not Logged In'),    403);
		}
		
		$followuser = User::find( $userID );
		//dd($followuser);
		/**
			FOR MONDAY: CHECK USER PRIVACY; SEND NOTIFICATION IF USER IS PRIVATE.
		 */
		if( $followuser->privacy == "private" ){

			$entityText = Auth::User()->username . " has requested to Follow you.  Please note that this user will not be able to read your messages until you follow them.";
			
			$redis_key = "user:" .  Auth::User()->_id . ":followingcandidate";
			$data = RedisL4::connection()->SADD($redis_key, $userID);
			
			$redis_key = "user:" .  Auth::User()->_id . ":following";
			$data = RedisL4::connection()->SADD($redis_key, $userID);	
			
			$redis_key = "user:" .  $userID . ":followedby";
			$data = RedisL4::connection()->SADD($redis_key, Auth::User()->_id);			
			
			$nots = new NotificationsHandler();
			$nots->extMakeDBentry($followuser->getKey(), $entityText, $followuser->getKey(), "", "followrequest");
			
		}else{
			$redis_key = "user:" .  Auth::User()->_id . ":following";
			$data = RedisL4::connection()->SADD($redis_key, $userID);			
			$redis_key = "user:" .  $userID . ":followedby";
			$data = RedisL4::connection()->SADD($redis_key, Auth::User()->_id);
		}

		
		return Response::json(['message' => 'Completed.  User '. Auth::User()->_id .' now following ' . $userID], 200);
	}
	
	public function unfollow($userID){

		if(User::find($userID)==null){
			return Response::json(array('message' => 'Not Valid Story ID'),    403);
		}
		
		if(null==Auth::User()){
			return Response::json(array('message' => 'Not Logged In'),    403);
		}
		
		
		$redis_key = "user:" .  Auth::User()->_id . ":following";
		$data = RedisL4::connection()->SREM($redis_key, $userID);
		
		$redis_key = "user:" .  $userID . ":followedby";
		$data = RedisL4::connection()->SREM($redis_key, Auth::User()->_id);
		
		return Response::json(['message' => 'Completed.  User '. Auth::User()->_id .' not following ' . $userID], 200);
	}
	
	public function whoFollowsMe($userID='me'){

		$this->reorderOrNot=false;
		
		$this->followingIndicatorOrNot=true;
		
		$this->dataArrayName='followers';
		
		if(null==Auth::User()){
			return Response::json(array('message' => 'Not Logged In'),    403);
		}
		
		$this->user = $this->userRepo->discover($userID);
		if(null==$this->user){
			return Response::json(array('message' => 'Bad User'),    403);
		}
		
		$redis_key = "user:" .  $this->user->_id  . ":followedby";
		$data = RedisL4::connection()->SMEMBERS($redis_key);

		$this->followerCount=sizeof($data);

		$redis_key = "user:" .  $this->user->_id  . ":following";
		$flw = RedisL4::connection()->SMEMBERS($redis_key);		
		
		$this->followingCount=sizeof($flw);
		
		$this->query = User::whereIn("_id", $data);
		
		$results = $this->makeReturnObject();
		return Response::json($results , 200);		
		
	}
	
	public function whatEventsDoIfollow($userID='me'){

		$this->reorderOrNot=true;
		
		if(null==Auth::User()){
			return Response::json(array('message' => 'Not Logged In'),    403);
		}
		
		$this->user = $this->userRepo->discover($userID);
		if(null==$this->user){
			return Response::json(array('message' => 'Bad User'),    403);
		}
		
		$redis_key = "user:" .  $this->user->_id . ":following";
		$data = RedisL4::connection()->SMEMBERS($redis_key);
		
		$this->query = Story::whereIn("user_id", $data);
		
		$this->query->orderBy('created_at', 'DESC');
		
		$this->query->orderBy('likes', 'DESC');		
		
		$results = $this->makeReturnObject();
		
		return Response::json($results , 200);		
		
	}
	
	public function whoDoIFollow($userID='me', $returnRawList=false){

		$this->reorderOrNot = false;
		
		$this->followIndicatorOrNot=true;
		//$this->followingIndicatorOrNot=true;
			
		$this->dataArrayName='following';
		
		if(null==Auth::User()){
			return Response::json(array('message' => 'Not Logged In'),    403);
		}
		
		$this->user = $this->userRepo->discover($userID);
		if(null==$this->user){
			return Response::json(array('message' => 'Bad User'),    403);
		}		
		
		$redis_key = "user:" .  $this->user->_id . ":following";
		
		$data = RedisL4::connection()->SMEMBERS($redis_key);	
		
		$this->query = User::whereIn("_id", $data);
		
		$this->query->orderBy('title');
		
		$this->query->orderBy('created_at', 'DESC');
		
		$this->query->orderBy('likes', 'DESC');
		
		$this->followingCount=sizeof($data);
		
		$redis_key = "user:" .  $this->user->_id  . ":followedby";
		
		$flw = RedisL4::connection()->SMEMBERS($redis_key);
		
		$this->followerCount=sizeof($flw);
		
		$results = $this->makeReturnObject();
		
		return Response::json($results , 200);
			
	}
	
	private function makeReturnObject(){
	
		//check to see if any ORDER values have been set.
		if(null != Input::get("order")){
			$arrOrderPrecidence=array();
	
			if(!is_array(Input::get("order"))){
				$arrOrderPrecidence[]=Input::get("order");
			}else{
				$arrOrderPrecidence=Input::get("order");
			}
				
			//be sure to include any orderable DB colums in this section.
			foreach($arrOrderPrecidence as $key=>$val){
				if(in_array($val, array('title'))){
					$this->query->orderBy($val, 'DESC');
				}elseif(in_array($val, array('created_date', 'likes'))){
					$this->query->orderBy($val, 'DESC');
				}
			}
		}
	
		//Per page?
		$perpage = intval(Input::get('count',0)) > 0 ? intval(Input::get('count')) : $this->pageCount;
		$this->query->take($perpage);
			
		// Page num?
		$page = intval(Input::get('page',0));
		$this->query->skip($perpage * $page);
	
		if($this->resultsMask!=null){
			$results = $this->query->get($this->resultsMask);
		}else{
			$results = $this->query->get();
		}
	
		$redis_key = "user:" .  $this->user->_id . ":following";
		$data = RedisL4::connection()->SMEMBERS($redis_key);
		$this->followingCount=sizeof($data);
		
		$redis_key = "user:" .  $this->user->_id  . ":followedby";
		$flw = RedisL4::connection()->SMEMBERS($redis_key);
		$this->followerCount=sizeof($flw);		
		
		//var_dump( $this->user["_id"] );
		//var_dump( $data );
		//var_dump( $flw );
		//dd();
		
		//if ( in_array( $this->user["_id"] , $flw) ){
		if ( in_array( $this->user["_id"] , $data) ){
			$this->user["following"] = true;
		}else{
			$this->user["following"] = false;
		}
		
		
		$pagination = array(
				'pagination' => [
					'requested' => [
						'page' => $page,
						'count' => $perpage
					],
					'page' => $page,
					'navigation' => [
						'previous' => $page <= 0 ? null : URL::current().'?count='.$perpage.'&page='.($page-1) . $this->urlAppend,
						'next' => $results->count() >= $perpage ? URL::current().'?count='.$perpage.'&page='.($page+1). $this->urlAppend : null
					]
				]
		);

		$this->user['followingCount'] = $this->followingCount;
		$this->user['followerCount']  = $this->followerCount;
		$this->user['privateArticleCount'] = Story::where("user_id", "=", $this->user->id)->where("privacy", "=", "private")->get()->count();
		$this->user['publicArticleCount']  = Story::where("user_id", "=", $this->user->id)->where("privacy", "=", "public")->get()->count();
				
		$returnResults = array();
		$returnResults['user'] = $this->user;
		$returnResults['pagination'] = $pagination;
		
		
		
		if($this->reorderOrNot==true){
			$modifiedResults = $this->resultsReorder($results->toArray());
		}elseif($this->followIndicatorOrNot==true){
			$modifiedResults = $this->resultsAddFollowIndicator($results->toArray());
		}elseif($this->followingIndicatorOrNot==true){
			$modifiedResults = $this->resultsAddFollowingIndicator($results->toArray());
		}else{
			$modifiedResults = $results->toArray();
		}	
		$returnResults[$this->dataArrayName] = $modifiedResults;
	
		
	
		return($returnResults);
	}
	
	protected function resultsAddFollowIndicator($unmodifiedResults){
		
		$finalResults = array();

		foreach($unmodifiedResults as $modifiedResult){
			/*
			$redis_key = "user:" . $modifiedResult["_id"]  . ":" . "followedby";
			$doTheyFollowMe = RedisL4::connection()->SISMEMBER($redis_key, Auth::User()->getKey());
			if(null == $doTheyFollowMe){
				$modifiedResult["following1"] = "false";
			}else{
				$modifiedResult["following1"] = "true";
			}

			
			$redis_key = "user:" . $modifiedResult["_id"]  . ":" . "following";
			$doTheyFollowMe = RedisL4::connection()->SISMEMBER($redis_key, Auth::User()->getKey());
			if(null == $doTheyFollowMe){
				$modifiedResult["following2"] = "false";
			}else{
				$modifiedResult["following2"] = "true";
			}
			
			$redis_key = "user:" . Auth::User()->getKey() . ":" . "followedby";
			$doTheyFollowMe = RedisL4::connection()->SISMEMBER($redis_key,  $modifiedResult["_id"] );
			if(null == $doTheyFollowMe){
				$modifiedResult["following3"] = "false";
			}else{
				$modifiedResult["following3"] = "true";
			}
			
			$redis_key = "user:" . Auth::User()->getKey()  . ":" . "following";
			$doTheyFollowMe = RedisL4::connection()->SISMEMBER($redis_key, $modifiedResult["_id"] );
			if(null == $doTheyFollowMe){
				$modifiedResult["following4"] = "false";
			}else{
				$modifiedResult["following4"] = "true";
			}
			*/	
			$redis_key = "user:" .  Auth::User()->getKey() . ":" . "following";
			$doTheyFollowMe = RedisL4::connection()->SISMEMBER($redis_key, $modifiedResult["_id"]);
			if(null == $doTheyFollowMe){
				$modifiedResult["following"] = "false";
			}else{
				$modifiedResult["following"] = "true";
			}
						
			$finalResults[]=$modifiedResult;
				
		}		
		
		
		return $finalResults;
	}
	
	protected function resultsAddFollowingIndicator($unmodifiedResults){
		
		$finalResults = array();
		
		foreach($unmodifiedResults as $modifiedResult){

			$redis_key = "user:" .  Auth::User()->getKey() . ":" . "following";
			$doTheyFollowMe = RedisL4::connection()->SISMEMBER($redis_key, $modifiedResult["_id"]);
				
			if(null == $doTheyFollowMe){
				$modifiedResult["following"] = "false";
			}else{
				$modifiedResult["following"] = "true";
			}
			
			$finalResults[]=$modifiedResult;
			
		}
		
		return $finalResults;
	}
	
	protected function resultsReorder($unmodifiedResults){

		$this->dataArrayName = "followedevents";
		
		$modifiedResults=array();
		$finalResults=array();
		
		
		foreach($unmodifiedResults as $unmodifiedResult){
			
			if(isset($unmodifiedResult['privacy'])){
				if($unmodifiedResult['privacy']=='public'){
					$modifiedResults[]=$unmodifiedResult;
				}else{
					$redis_key = "user:" . $this->user->_id . ":" . "storyaccess";
					if((RedisL4::connection()->SISMEMBER($redis_key, $unmodifiedResult['_id'] ))==true){
						$modifiedResults[]=$unmodifiedResult;
					}
				}
			}
		}			
		
		foreach($modifiedResults as $modifiedResult){
		
			$modifiedResult['user']=User::find($modifiedResult['user_id'])->toArray();


			if(array_key_exists ( "permissions" , $modifiedResult )){
				if($modifiedResult["permissions"] < 0){
					$modifiedResult["permissions"]=0;
				}
			}else{
				$modifiedResult["permissions"]=0;
			}
			
			if(array_key_exists ( "likes" , $modifiedResult )){
				if($modifiedResult["likes"] < 0){
					$modifiedResult["likes"]=0;
				}
			}else{
				$modifiedResult["likes"]=0;
			}

			$commCount = Comment::where("story_id", "=", $modifiedResult["_id"])->count();
			
			if(null != $commCount){
				$modifiedResult["comments"]=$commCount;
			}else{
				$modifiedResult["comments"]=0;
			}
			
			
			
			
			$redis_key = "user:" .  Auth::User()->getKey() . ":" . "storylike";
			$likeIsThere = RedisL4::connection()->ZSCORE($redis_key, $modifiedResult['_id']);
			
			if(null == $likeIsThere){
				$modifiedResult["doIlikethis"] = "false";
			}else{
				$modifiedResult["doIlikethis"] = "true";
			}
			

			
			$finalResults[]=$modifiedResult;
		}
		
		return $finalResults;
	}
	
	
}