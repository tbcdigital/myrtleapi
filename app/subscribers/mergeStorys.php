<?php
use CommonCore\Users\User;
/**
*
*
*/
class StoryMergeSubscriber {

	public function subscribe(Illuminate\Events\Dispatcher $events)
	{
		//$events->listen('eloquent.deleted: Story', 'StoryDeleteSubscriber@onMergeStory');
	}

	/**
	 * Handle merging of one Story into another Mongo.
	 *  @param: object of type Story $story (master story)
	 *  @param: object of type Story $substory (story being merged in)
	 *  @return: null
	 */	
	public function onMergeStory(Story $story, Story $substory){

		return;
	}
}