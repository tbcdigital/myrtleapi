<?php
use CommonCore\Users\User;
/**
*
*
*/
class UserDeleteSubscriber {

	public function subscribe(Illuminate\Events\Dispatcher $events)
	{
		$events->listen('eloquent.deleted: User', 'UserDeleteSubscriber@onDeleteUser');
	}

	/**
	 * Handle deletion of User in Mongo.
	 *  @param: Object of type User $user
	 *  @return: null
	 */	
	public function onDeleteUser(User $user){

		$redis_key = "user:" .  $user->_id . ":following";
		$data = RedisL4::connection()->SREM($redis_key, $userID);
		

		
		$u = User::all();
		$u->each(
				function($oneofmanyusers) use ($user){
					$redis_key = "user:" .  $oneofmanyusers->_id . ":followedby";
					$data = RedisL4::connection()->DEL($redis_key);					
				}
		);	
		
		return;
	}
}