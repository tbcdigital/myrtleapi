<?php
//  UNSUITABLE FOR REVIEW 10/01
use Carbon\Carbon;
use CommonCore\Users\User;


class NotificationsHandler {

	
	protected $target = null;
	
	/**
	 * Register listeners for this subscriber
	 * @param Illuminate\Events\Dispatcher $events
	 * @return array
	 */
	public function subscribe(Illuminate\Events\Dispatcher $events) {
		
		$events->listen('eloquent.created: Story', 'NotificationsHandler@onStoryCreated');
		$events->listen('eloquent.created: Comment', 'NotificationsHandler@onCommentCreated');
		$events->listen('eloquent.created: StoryPicture', 'NotificationsHandler@onStoryPictureCreated');
		
		$events->listen('story.liked', 'NotificationsHandler@onStoryLike');
		$events->listen('storypicture.liked', 'NotificationsHandler@onStoryPictureLike');
		$events->listen('story.permissiongranted', 'NotificationsHandler@onStoryPermissionGranted');

		$events->listen('batch.completed', 'NotificationsHandler@onBatchCompleted');
	}
	
	public function extMakeDBentry($entityID, $entityText, $recipientID, $entityStoryID, $type, $doNotSendPushNotification=null){
		
		$this->makeDBentry($entityID, $entityText, $recipientID, $entityStoryID, $type, 1);
		
	}
	
	function makeDBentry($entityID, $entityText, $recipientID, $entityStoryID, $type, $doNotSendPushNotification=null){
		//dd($entityText);
		//get the logged-in user
		
		if(null==Auth::User()){
			$loggedInUser = "000000000000";
		}else{
			$loggedInUser = Auth::User()->getKey();
		}
		
			$n = new Notification(	
					array(
							'entity_id'=>$entityID,
							'text'=>$entityText,
							'user_id'=>$loggedInUser,
							'story_id'=>$entityStoryID ,
							'recipient_id'=>$recipientID,
							'type' => $type,
							'read' => false
					)
			);
			
			/** 
			 * these debug statement are used very often (because subscribers do not return anything useful)
			 * so can probably be left in.  Especially, it is often useful to be able to see the error from
			 * the model if something isn't saving properly.
			 */
			//	var_dump( $n->toArray() );
			//	dd( $n->toArray() );
					
				$n->save();
				
				$newKey = $n->getKey();
				
				  //var_dump($n->save());
				  //var_dump($n->errors()->toArray());
				
				$recip = User::find( $recipientID );
				//dd($recip->toArray());
				if( $doNotSendPushNotification == null ){
					$this->add_notification($recip, $entityText);
				}
				
			return $newKey;
	}
	
	public function onStoryCreated(Story $story){
		//	echo("ON STORY CREATION");
		//	var_dump($story->toArray());
		//	dd($story->toArray());
		return;
	}

	public function onCommentCreated(Comment $comment){

		$this->target = 'myrtle://comment/';
		
		$arrComment = $comment->toArray();
		$msg = "";
		$msg = $msg . $comment->user->username;
		$msg = $msg . " commented on ";
		
		if($comment->type == 'story'){		
			$msg = $msg . $comment->story->title;
		}else{
			$msg = $msg . $comment->story->title;
		}
		$this->makeDBentry($comment->getKey() , $msg , $comment->story->user_id, $comment->story_id, 'comment');
		
		return;
	}

	public function onStoryPictureCreated(StoryPicture $storyPicture){
		
		$this->target = 'myrtle://storypicture/';
			return;
		
			$msg = "";
			$msg = $msg . $storyPicture->user->username;
			$msg = $msg . " uploaded a picture to ";
			
			$msg = $msg . $storyPicture->story->title;
			$this->makeDBentry($storyPicture->getKey() , $msg , $storyPicture->story->user_id, $storyPicture->story_id, 'storypicture');

		return;		
	}
	
	
	public function onBatchCompleted(Batch $batch, $userID){
		
		//dd($batch->story);
		
		$this->target = 'myrtle://storypicture/';
		
		$storyPictures = StoryPicture::where("batchID", "=", $batch->getKey() );
		
		//dd($storyPictures->count() );
		
		$msg = "";
		$msg = $msg . Auth::User()->username;
		//$msg = $msg . " uploaded " . $storyPictures->count() . " out of ";
		//$msg = $msg . " " . $batch->count . " pictures to the event '";
		$msg = $msg . " uploaded " .  $batch->count . " pictures to the event " . $batch->story->title ;
		
		//dd($msg);
		
		$this->makeDBentry($batch->getKey() , $msg , $batch->story->user_id, $batch->story->getKey(), 'batch-completed');
		
		return;		
	}
	
	public function onStoryLike(Story $story, $userID){
		//	echo("ON STORY LIKE");
		//	var_dump($story->toArray());
		
		$this->target = 'myrtle://storylike/';
		
		$msg = "";
		$msg = $msg . Auth::User()->username;
		$msg = $msg . " liked ";
		$msg = $msg . $story->title;
		$this->makeDBentry($story->getKey() , $msg , $userID, $story->getKey(), 'like');
		
		
		return;		
	}
	
	public function onStoryPictureLike(Storypicture $storypicture, $userID){
		//	echo("ON STORY LIKE");
		//	var_dump($story->toArray());
		
		$this->target = 'myrtle://storypicturelike/';
		
		$msg = "";
		$msg = $msg . Auth::User()->username;
		$msg = $msg . " liked a picture on ";
		$msg = $msg . $storypicture->story->title;
		$this->makeDBentry($storypicture->getKey() , $msg , $userID, $storypicture->getKey(), 'storypicture-like');
		
		
		return;		
	}
	
	public function onStoryPermissionGranted(Story $story, $userID){
		
		$this->target = 'myrtle://permission/';
		
		//Story $story, $userID
		//echo("UserID");
		//dd($incm);
		//$story = $incm[0];
		//$userID = $incm[1];
		//echo("\nUserID\n");
		//dd($userID);
		//TODO.  Single Push Notif. "You have been granted permission to do xxx on yyy
				
		$msg = "";
		$msg = $msg . $story->user->username;
		$msg = $msg . " granted you access to ";
		$msg = $msg . $story->title;
		$this->makeDBentry($story->getKey() , $msg , $userID, $story->getKey(), 'grantpermission');

		/*
		 * We have to inform EVERYONE who has permissions on this story.  We use the redis key
		* story:{storyID}:useraccess to get a list of RECIPIENT ID's
		*/		
		$redis_key = "story:" . $story->getKey()  . ":" . "useraccess";
		$recipients = RedisL4::connection()->SMEMBERS($redis_key);
		
		foreach($recipients as $recipient){
			//$this->makeDBentry($story->getKey() , $msg , $recipient, $story->getKey(), 'grantpermission');
		}
		
		//	echo("ON STORY PERM. GRANTED");
		//	var_dump($story->toArray());
		//	dd($story->toArray());
		return;		
	}
	

	private function add_notification($user, $msg) {
	
		$notif['read'] = 0;

		$notif['_id'] = (string) new MongoId;
	
		$redis = RedisL4::connection();
	
		$redis->zAdd('user:'.$user->getKey().':notifications', Carbon::now()->timestamp, json_encode($notif));
	
		$this->crop_notifications_for_user($user);

        if(null === $user->notification_tokens || !is_array($user->notification_tokens)) {
            return;
        }
	
			foreach($user->notification_tokens as $td) {
					
					//var_dump($td);
					//var_dump( strtr($td['token'], array('>' => '', '<' => '', ' '=>'')) );
					
					//dd();
					$is_dev_token = isset($td['development']) &&  $td['development'] === true  ? true : false;
					$storagePath = storage_path('certificates/myrtle'.($is_dev_token ? 'Dev' : 'Prod') . 'APNS.pem');
					
					//var_dump( $storagePath );
	
					$stream_context = stream_context_create();
					stream_context_set_option($stream_context, 'ssl', 'local_cert', storage_path('certificates/myrtle'.($is_dev_token ? 'Dev' : 'Prod') . 'APNS.pem'));
					stream_context_set_option($stream_context, 'ssl', 'passphrase', 'mYrt13pwr');
					$stream_client = @stream_socket_client("ssl://gateway".($is_dev_token ? '.sandbox' : '').".push.apple.com:2195", $err,	$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_ASYNC_CONNECT|STREAM_CLIENT_PERSISTENT, $stream_context);
					
					$body = array('aps' => $payload = array(
							"sound" => "default",
							"alert" => $msg,
							"target" => $this->target
					));
					
					$payload = json_encode($body);
					//6e0727cef01ebaa28286733f54adf3baf49a8c27823964a138a2410dc2518b14
					//var_dump($td['token']);
					$message = chr(0) . pack('n', 32) . pack('H*', strtr($td['token'], array('>' => '', '<' => '', ' '=>''))) . pack('n', strlen($payload)) . $payload;
					//var_dump( pack('H*', strtr($td['token'], array('>' => '', '<' => '', ' '=>''))) );
					//												6e0727cef01ebaa28286733f54adf3baf49a8c27823964a138a2410dc2518b14
					//												  6e0727cef01ebaa28286733f54adf3baf49a8c27823964a138a2410dc2518b14
					//$message = chr(0) . pack('n', 32) . pack('H*', "6e0727cef01ebaa28286733f54adf3baf49a8c27823964a138a2410dc2518b14") . pack('n', strlen($payload)) . $payload;
						
					//dd($message);
					//var_dump( $stream_client );
					
					// Send it to the server
					if ($stream_client != false ){
						$result = fwrite($stream_client, $message, strlen($message));
						//var_dump($result);
						// Close the connection to the server
						fclose($stream_client);
					}
					
	
	
			}
	
		}
	

	
	private function crop_notifications_for_user($user) {
	
		$maxcount = Config::get('notifications.internalfeed.maxcount');
		$maxcount += 1;
		$maxcount = $maxcount * -1;
		$redis = RedisL4::connection();
		$redis->zRemRangeByRank('user:'.$user->getKey().':notifications', 0, $maxcount);
	
	}
	
		
}