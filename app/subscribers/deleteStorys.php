<?php
use CommonCore\Users\User;
/**
*
*
*/
class StoryDeleteSubscriber {

	public function subscribe(Illuminate\Events\Dispatcher $events)
	{
		$events->listen('eloquent.deleted: Story', 'StoryDeleteSubscriber@onDeleteStory');
	}

	/**
	 * Handle deletion of one Story in Mongo.
	 *  @param: object of type Story $story
	 *  @return: null
	 */	
	public function onDeleteStory(Story $story){
		
			//when a Story is deleted, the Comments, StoryPictures and Redis Keys
			// (for like, perms, views, storyaccess and useraccess) have to be deleted too.
			
			//Mop-up anything in Redis that has some kind of story:thisStoryID key,
			//or any kind of value of $story->_id
		
			$s = Comment::where("story_id" , "=" , $story->_id);
			$s->delete();
			
			$p = StoryPicture::where("story_id" , "=" , $story->_id);
			$p->delete();

			$redis_key = "story:likes";
			$data = RedisL4::connection()->ZREM($redis_key, $story->_id);

			$redis_key = "story:perms";
			$data = RedisL4::connection()->ZREM($redis_key, $story->_id);
			
			$redis_key = "story:views";
        	$data = RedisL4::connection()->ZREM($redis_key, $story->_id);
	        	
        	$redis_key = "story:" . $story->_id . ":userviews";
        	$accesscount = RedisL4::connection()->DEL($redis_key);		
			
			$u = User::all();
			$u->each(
				function($user) use ($story){
					$redis_key = "user:" .  $user->_id . ":storyaccess";
					$data = RedisL4::connection()->SREM($redis_key, $story->_id);
					$redis_key = "story:" .  $story->_id . ":useraccess";
					$data = RedisL4::connection()->SREM($redis_key, $user->_id);		
					$redis_key = "user:" .  $user->_id . ":storylike";
					$data = RedisL4::connection()->ZREM($redis_key, $story->_id);
					$redis_key = "story:" .  $story->_id . ":userlike";
					$data = RedisL4::connection()->ZREM($redis_key, $user->_id);		
				}
			);

		return;
	}
}