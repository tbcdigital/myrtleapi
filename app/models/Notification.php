<?php

class Notification extends CommonCore\Core\Models\SelfValidatingModel {


	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'notifications';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array();

	/**
	 * The attributes which can be mass-assigned
	 *
	 * @var array
	 */
	protected $fillable = array('entity_id', 'text', 'user_id', 'story_id', 'recipient_id', 'type', 'read');

	/**
	 * These attributes are NOT mass assignable
	 */
	protected $guarded = array('id', '_id');

	protected $appends = array();

	protected $relations = [];

	protected static $rules = array(

		'save' => array(
				'text'=>'required|min:1', 
				'user_id'=>'required|min:3', 
				'story_id'=>'required|min:3' ,
				'recipient_id'=>'required|min:3', 
				'type' => 'required|in:story,storypicture,comment,grantpermission,like,permissionrequest,batch-completed,followrequest'
		),
		'create' => array(),
		'update' => array()

	);
	
	public function story(){
	    return $this->belongsTo("Story" , "story_id", "_id");
	}
	
		
}