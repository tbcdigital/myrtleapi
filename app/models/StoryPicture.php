<?php

class StoryPicture extends  CommonCore\Core\Models\SelfValidatingModel {
    
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'storypictures';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array();

	/**
	 * The attributes which can be mass-assigned
	 *
	 * @var array
	 */
	protected $fillable = array('location', 'url', 'batchID');
	
	/**
	 * 
	 * 
	 * @var unknown
	 */
	public $imageBaseUrl = "http://myrtle.s3.amazonaws.com/originals/";
	
	/**
	 * These attributes are NOT mass assignable
	 */
	protected $guarded = array('id', '_id');

	protected $appends = array();

	protected $relations = [];

	protected static $rules = array(

		'save' => array('story_id'=>'required|min:3', 'user_id'=>'required|min:3', 'url'=>'required|min:10'),
		'create' => array(),
		'update' => array()

	);

	
	public function getUrlAttribute( $existingURLstring ){
		
		$existingURLstring =  ltrim($existingURLstring , "/");
		
		//check if $existingURLstring starts with 'http'.  If so, this is
		//already a fully-formed URL (somehow) and can be returned as-is.
		
		if( substr($existingURLstring, 0, 4) == 'http' ){
			return $existingURLstring;
		}
		//if not, it is only the image name, and we have to mutate it to add
		//all domain information.
		$userID = $this->user_id;
		$basicURL = $this->imageBaseUrl . $userID . "/";
		
		$newURLstring =  $basicURL . $existingURLstring;
		
		//dd($newURLstring);
		
		return $newURLstring;
	}
	
	public function user(){
		return $this->belongsTo("CommonCore\Users\User" , "user_id", "_id");
	}

	public function story(){
		return $this->belongsTo("Story" , "story_id", "_id");
	}
	
	public function comments(){
	    return $this->hasMany('Comment');
	}	
	
	public function batchs(){
	    return $this->hasMany('Batch');
	}	
	
}