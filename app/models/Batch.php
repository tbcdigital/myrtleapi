<?php

class Batch extends CommonCore\Core\Models\SelfValidatingModel {


	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'batchs';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array();

	/**
	 * The attributes which can be mass-assigned
	 *
	 * @var array
	 */
	protected $fillable = array('story_id', 'user_id', 'type');

	/**
	 * These attributes are NOT mass assignable
	 */
	protected $guarded = array('id', '_id');

	protected $appends = array();

	protected $relations = [];

	protected static $rules = array(

		'save' => array('story_id'=>'required|min:3', 'user_id'=>'required|min:3'),
		'create' => array(),
		'update' => array()

	);

	protected $attributes = [
	'status' => 'open',
	'count' => 0
	];
	
	public function story(){
	    return $this->belongsTo("Story");
	}
	
	public function user(){
		return $this->belongsTo("CommonCore\Users\User" , "user_id", "_id");
	}

	public function storypictures(){
		return $this->hasMany("StoryPicture", "batch_id", "_id");
	}
	
}