<?php

class Story extends  CommonCore\Core\Models\SelfValidatingModel {
    
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'storys';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array();

	/**
	 * The attributes which can be mass-assigned
	 *
	 * @var array
	 */
	protected $fillable = array('title', 'description', 'location', 'privacy', 'coverpicture');
	
	/**
	 * These attributes are NOT mass assignable
	 */
	protected $guarded = array('id', '_id');

	protected $appends = array();

	protected $relations = [];

	protected static $rules = array(

		'save' => array('title'=>'required|min:3', 'description'=>'required|min:8', 'privacy' => 'required|in:public,private', 'user_id'=>'required|min:3'),
		'create' => array(),
		'update' => array()

	);
	
	protected $attributes = [
		'coverpicture' => 'NO-COVER-PICTURE'
	];
	
	public function comments(){
	    return $this->hasMany('Comment');
	}	

	public function user(){
		return $this->belongsTo("CommonCore\Users\User" , "user_id", "_id");
	}
	
	public function storypictures(){
	    return $this->hasMany('StoryPicture');
	}	
	
}