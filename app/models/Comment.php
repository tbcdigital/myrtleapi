<?php

class Comment extends CommonCore\Core\Models\SelfValidatingModel {


	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'comments';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array();

	/**
	 * The attributes which can be mass-assigned
	 *
	 * @var array
	 */
	protected $fillable = array('comment', 'reply_to', 'type');

	/**
	 * These attributes are NOT mass assignable
	 */
	protected $guarded = array('id', '_id');

	protected $appends = array();

	protected $relations = [];

	protected static $rules = array(

		'save' => array('comment'=>'required|min:3', 'story_id'=>'required|min:3', 'type' => 'required|in:story,picture'),
		'create' => array(),
		'update' => array()

	);
	
	public function story(){
	    return $this->belongsTo("Story" , "story_id", "_id");
	}
	
	public function user(){
		return $this->belongsTo("CommonCore\Users\User" , "user_id", "_id");
	}
	
	
}