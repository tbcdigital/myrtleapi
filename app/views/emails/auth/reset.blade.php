<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<div>
			<p>Hey {{$first_name}},<br /><br />We hear that you can't access your account any more? &#9785; Not to worry, we've reset your password for you which you can find below. We'd recommend changing it too something more memorable as soon as you login, or better yet, attach your favourite social network to your account after logging in, and you'll never have to remember the password again!</p>
			<p>&nbsp;</p>
			<p>Your new password is <b>{{{$password}}}</b></p>
		    <br />
			<p>Cheers, from <b>MYRTLE</b></p>
			<p>&nbsp;</p>
			<h5>Didn't expect this email?</h5>
			<small>If this is all a big surprise to you, then someone may have just accidently entered the wrong username into the password reset page. However there's no need to worry, that's why we only send the new password to your registered email address, so only the real account owner can reset the password. Although, unfortunately you will have to use password above, the next time you login, but you're free to change it to whatever you like afterwards!</small>
			<p>&nbsp;</p>

		</div>
	</body>
</html>