##Common Core Laravel Package

### Initial setup configuration

#### Security
If you do not set up this package using `composer create-project` you should run this command in the root of the project in order to generate a unique application key:
````
php artisan key:generate
````

#### Package configuration
Both the Social authentication and TBC authentication packages included in this framework have configuration directives that you will probably need to change. 
It is therefore a good idea to publish the package config files on installation. To do this, enter these commands:
````
php artisan config:publish tbcdigital/socialauth
php artisan config:publish tbcdigital/tbcauth
````

#### Git
Ensure that:

a. You have removed (or commented out) the `composer.lock` file, this is required when pushing to production.
b. If you cloned the core repository, you should remove the remote repository, and add your project specific one.

#### Database

##### MongoDB
The package comes pre-setup for MongoDB, you just need to go to `app/config/database.php` and check the settings for the mongodb driver are correct (i.e. you may want to change the database name).

##### Changing to use MySQL (or another default laravel driver)
If you want to change the framework to use one of the default laravel/eloquent drivers, you will need to:

1. Change the base model class in `app/Modules/Core/Models/Model.php` to use laravels Eloquent by removing this line: `use Jenssegers\Mongodb\Model as Eloquent;`.
2. Within `app/config/database.php` change the default driver to `'mysql'` or whichever driver you wish to use.
3. Ensure you run the database migration files on the database if required (i.e. is a schema DB like MySQL).
4. For the sake of cleaning up, if you will not be using mongo on the project, remove the references to it from `app/config/app.php` (Service provider) and from `composer.json`


